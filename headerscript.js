// Header Stuck
(function ($) {
  var $topBar = $("#top-bar"),
    $topBarBtn = $("#top-bar-bbtn"),
    $unikwanlogo = $("#unikwanlogo"),
    $window = $(window);

  var scroll = $window.scrollTop();
  if (scroll > 20) {
    $topBar.addClass("stuck");
  }

  $window.on("scroll", function () {
    var scroll = $window.scrollTop();
    if (scroll > 20) {
      $topBar.addClass("stuck");
      $topBarBtn.addClass("stuck");
      $unikwanlogo.attr("src", "images/unikwanlogo/colorlogo.png");
    } else {
      $topBar.removeClass("stuck");
      $topBarBtn.removeClass("stuck");
      if (window.innerWidth <= 1024) {
        $unikwanlogo.attr("src", "images/unikwanlogo/colorlogo.png");
      } else {
        $unikwanlogo.attr("src", "images/unikwanlogo/whitelogo.png");
      }
    }
  });
})(jQuery);
