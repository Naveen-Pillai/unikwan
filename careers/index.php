<!DOCTYPE html>
<html lang="en">

<head>
    <title>Careers | Unikwan Innovations | Design Agency In Bangalore</title>
    <meta name="keywords" content="Career">
    <meta name="description"
        content="Do have passion in the Design domain and are ready to accept challenges? Join us today for exciting career opportunities." />

    <?php require '../elements/headerinner.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../elements/navbarinner.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding">
                <div class="">
                    <div class="subpage-header__bg">
                        <div class="container">
                            <div class="subpage-header__block">
                                <h1 class="subpage-header__title">Careers</h1>
                                <div class="subpage-header__line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section section-default-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xl-4">
                            <div class="section-heading">
                                <h4 class="title">
                                    Opportunities<br> With Us


                                </h4>

                            </div>
                        </div>
                        <div class="col-md-8 col-xl-8">
                            <strong class="base-color-02">Create, craft and anticipate future world with us.
                            </strong>
                            <p>
                                We utilise an interdisciplinary approach to building solutions where technical experts,
                                designers, researchers, management professionals and other domain professionals with
                                diverse skillsets come together as one: a union of thought to leverage the varied
                                synergies resulting from this collaboration.

                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section section-default-top section--bg-00">
                <div class="container">
                    <div class="nav-tabs-dafault">
                        <!-- Nav tabs -->
                        <div class='tabs-uk-overflow'>
                            <ul class="nav nav-tabs" style='min-width:440px'>
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab-01">DESIGN</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-02">DEVELOPMENT</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-03">DIGITAL</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-04">MANAGEMENT</a>
                                </li>
                            </ul>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-01">
                                <div class="accordeon js-accordeon">
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            UX Designer
                                        </h5>
                                        <div class="accordeon__content">
                                            <p>
                                                We are looking for a user experience designer with strong interests and
                                                capabilities in the design and development of engaging user experiences.
                                                The ideal candidate will thrive in a work environment that requires
                                                strong problem solving skills and independent self-direction, coupled
                                                with an aptitude for team collaboration and open communication. A
                                                thorough understanding of contemporary user-centered design
                                                methodologies is a must.
                                            </p>
                                            <br>
                                            <h6 class="role-req">ROLE REQUIREMENTS</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    One or more years of user experience design experience for software,
                                                    Web applications which leverage emergent technologies, consumer
                                                    electronics and/or mobile devices.
                                                </li>
                                                <li>
                                                    Strong conceptualization ability, strong visual communication
                                                    ability, drawing skills and sketchbook technique.
                                                </li>
                                                <li>
                                                    Exceptional design skills, production value and attention to detail.
                                                </li>
                                                <li>
                                                    Ability to create wireframes as well as visual design comps.
                                                </li>
                                                <li>
                                                    Strong working knowledge of Photoshop, Illustrator, InDesign,
                                                    Fireworks and associated design tools.
                                                </li>
                                                <li>
                                                    Experience with user interface design patterns and standard UCD
                                                    methodologies.
                                                </li>
                                                <li>
                                                    Strong written and verbal communication skills.
                                                </li>
                                                <li>
                                                    Understanding of common software development practices.
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>
                                            <!-- <div style="margin-top:20px">
										<a href="mailto:hr@unikwan.com" class="btn-border">Apply Now</a>
                  </div> -->

                                        </div>
                                    </div>
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            Information Designer
                                        </h5>
                                        <div class="accordeon__content">
                                            <p> The position demands the ability to balance tasking from multiple teams
                                                and clients. The position also requires visual translation of complex
                                                data–visual outputs will be translated back and forth between a number
                                                of mediums, including Adobe Illustrator and Photoshop. Must be a great
                                                ‘whiteboard’ thinker!
                                            </p>
                                            <br>
                                            <h6 class="role-req">ROLE REQUIREMENTS</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    Attending client and internal meetings to gather requirements.
                                                </li>
                                                <li>
                                                    Designing conceptual graphics & strategy maps based upon
                                                    brainstorming sessions and direction Reviewing concept sketches or
                                                    mock-­‐ups.
                                                </li>
                                                <li>
                                                    Assist in development of mock-ups for conversion to HTML/interactive
                                                    platform.
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            Video and Motion Designer
                                        </h5>
                                        <div class="accordeon__content">
                                            <p>
                                                Highly detail-oriented, organized, well­-versed in the principles of
                                                strong design and should possess an eye for design aesthetic, and excel
                                                in motion design and layout. He should rapidly create and develop visual
                                                responses to complex communications requirements. Prepare design plan,
                                                concept and layout for motion graphic project.
                                            </p>
                                            <br>
                                            <h6 class="role-req">ROLE REQUIREMENTS</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    Design and create enticing motion graphics for video deliverables
                                                    (corporate videos, eLearning, websites, marketing demos, etc.).
                                                </li>
                                                <li>
                                                    Create and deliver motion graphics in various media including web,
                                                    mobile, etc.
                                                </li>
                                                <li>
                                                    Work with art and creative teams to understand project scope and
                                                    objectives.
                                                </li>
                                                <li>
                                                    Assist in selecting audio, video, colors, animation, etc for graphic
                                                    design.
                                                </li>
                                                <li>
                                                    Work with editors, producers and other designers to resolve
                                                    technical and/or design issues.
                                                </li>
                                                <li>
                                                    Edit raw video footage and add effects/elements to enhance motion
                                                    graphics.
                                                </li>
                                                <li>
                                                    Research and analyze best design techniques and solutions to create
                                                    motion graphics.
                                                </li>
                                                <li>
                                                    Assist in designing and creating storyboards.
                                                </li>
                                                <li>
                                                    Participate in brainstorming session to share new design
                                                    perspectives and ideas.
                                                </li>
                                                <li>
                                                    Maintain and follow best practices for versioning control, naming
                                                    convention and organization of graphic files.
                                                </li>
                                                <li>
                                                    Maintain up-to-date knowledge about latest graphic design
                                                    techniques.
                                                </li>
                                                <li>
                                                    Ensure compliance with company guidelines, deadlines and design
                                                    standards.
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            Graphic Designer
                                        </h5>
                                        <div class="accordeon__content">
                                            <p>
                                                A Graphic Designer should have a creative flair, strong visual language
                                                and up-to-date knowledge of the Design trends. You will be involved in a
                                                various of activities such as Websites, Corporate Branding and Identity,
                                                Posters, Brochures, Exhibition Displays etc.
                                            </p>
                                            <br>
                                            <h6 class="role-req">ROLE REQUIREMENTS</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    Excellent communication and collaboration: You understand that great
                                                    communications aren’t designed by designers alone. You are excited
                                                    about working with our business and product teams to gather the
                                                    insights you need to make design decisions.
                                                </li>
                                                <li>
                                                    Standout graphic design: You are passionate about communications
                                                    design and have the portfolio to match. You’ve explored an
                                                    interesting range of design systems that demonstrate structural
                                                    ability, playful typography and impactful image creation.
                                                </li>
                                                <li>
                                                    Production experience: CMYK, Pantone process, material selection and
                                                    printing specifications are just some of the production tricks up
                                                    your sleeve. You have experience in working with suppliers and
                                                    vendors and will take responsibility to ensure our artwork is good
                                                    to go.
                                                </li>
                                                <li>
                                                    An eye for photography: You make compelling, on-brand image
                                                    selections which bring designs and presentations to life. You are
                                                    confident in making photography edits, from simple color adjustments
                                                    to combining multiple images.
                                                </li>
                                                <li>
                                                    Know your tools: You’ve got a good grip on the Adobe software
                                                    packages and know the shortcuts to create first class designs in a
                                                    timely manner. You’ve also dabbled in Keynote and are ready to
                                                    create some fine looking business presentations.
                                                </li>
                                                <li>
                                                    Responsive design: Designing for the web means considering a number
                                                    of different screen sizes, from desktops to smart phones. It will be
                                                    to your benefit that you understand how elements on the page scale
                                                    and stack at different breakpoints.
                                                </li>
                                                <li>
                                                    Professional design experience: If you have spent some time working
                                                    on communications design at a design agency or technology company
                                                    this will be a bonus. We look forward to hearing about your
                                                    experience.
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane " id="tab-02">
                                <div class="accordeon js-accordeon">
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            Web Engineer/App Developer
                                        </h5>
                                        <div class="accordeon__content">
                                            <p>
                                                We are seeking a motivated, hands-on front-end web developer to join the
                                                development team. Developers are responsible for building, testing,
                                                debugging, and troubleshooting clean, web standards compliant code and
                                                work with server-side developers to integrate this into the final
                                                product.
                                            </p><br>

                                            <h6 class="role-req">ROLE REQUIREMENTS</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    1+ years experience in front-end web development.
                                                </li>
                                                <li>
                                                    Expert knowledge of XHTML, HTML5, CSS3.
                                                </li>
                                                <li>
                                                    Proven knowledge of JavaScript frameworks (jQuery, Backbone,
                                                    Angular, Closure, NodeJS, etc. )
                                                </li>
                                                <li>
                                                    Experience translating comps and wireframes into semantically
                                                    correct web templates.
                                                </li>
                                                <li>
                                                    Knowledge of responsive and adaptive design principles and
                                                    translating into fluid and responsive templates.
                                                </li>
                                                <li>
                                                    Strong working knowledge developing cross platform/browser.
                                                    compatibility (IE, Firefox, Safari, Opera, etc.) for dynamic web
                                                    applications.
                                                </li>
                                                <li>
                                                    Familiarity with and adherence to web standards and best practices
                                                    for load time reduction and accessibility.
                                                </li>
                                                <li>
                                                    Knowledge of SEO best practices.
                                                </li>
                                                <li>
                                                    Effective verbal and written communication skills and the ability to
                                                    interact professionally with a diverse group of people.
                                                </li>
                                                <li>
                                                    Familiarity with Django, PHP, Drupal, .NET is a plus.
                                                </li>
                                                <li>
                                                    Excellent verbal/written communication skills and strong time.
                                                    management and problem solving capabilities.
                                                </li>
                                                <li>
                                                    Proven debugging and troubleshooting skills.
                                                </li>
                                                <li>
                                                    Loves logic and problem solving!
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            Front End Developer
                                        </h5>
                                        <div class="accordeon__content">
                                            <p>
                                                We’re looking for a JavaScript, HTML and CSS specialist, who is always
                                                thinking about how we can make people’s lives better with well-written
                                                and efficient code. Someone with experience developing digital products
                                                in different platforms. You’ll be responsible for developing large
                                                products that demand innovative solutions. You’ll work closely with our
                                                design team and collaborate with our clients on projects that affect the
                                                lives of millions of people.
                                            </p><br>

                                            <h6 class="role-req">ROLE REQUIREMENTS</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    1+ years experience with digital products, such as sites and
                                                    applications.
                                                </li>
                                                <li>
                                                    Mastery of JavaScript, HTML and CSS.
                                                </li>
                                                <li>
                                                    Ability to write efficient, scalable and reusable code.
                                                </li>
                                                <li>
                                                    Experience with Javascript libraries, such as jQuery.
                                                </li>
                                                <li>
                                                    Experience with version control, such as Git.
                                                </li>
                                                <li>
                                                    Ability to work comfortably with command-line interpreters
                                                    (Terminal).
                                                </li>
                                                <li>
                                                    Experience solving compatibility issues between less advanced
                                                    browsers.
                                                </li>
                                                <li>
                                                    Familiarity with Mac OSX and Windows computers.
                                                </li>
                                                <li>
                                                    Focus on the end user, through accessible and efficient code.
                                                </li>
                                                <li>
                                                    Knowledge of CSS preprocessor, such as SASS and LESS.
                                                </li>
                                                <li>
                                                    Experience with fluid grids and responsive sites.
                                                </li>
                                                <li>
                                                    Familiarity with PHP and popular CMSs such as Wordpress.
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>

                                        </div>
                                    </div>
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            Technical Architects
                                        </h5>
                                        <div class="accordeon__content">
                                            <p>
                                                Technical Architects are the devoted problem solvers responsible for the
                                                overall execution and organization of the development effort on
                                                large-scale technology engagements. The Technical Architect has the
                                                ultimate responsibility of making technologies work together and, as a
                                                result, is a key role that contributes heavily towards the success of
                                                the project. They transform the requirements into architecture and
                                                design documents used by the rest of the team to actually build the
                                                solution.
                                            </p><br>
                                            <h6 class="role-req">ROLE REQUIREMENTS</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    Experience developing and leading large-scale technology engagement
                                                    in an agency environment.
                                                </li>
                                                <li>
                                                    Advanced knowledge of programming languages (e.g. JS, C#, Java and
                                                    PHP, Perl, SQL, Ruby-on-Rails, etc.) and the ability to propose
                                                    alternative solutions and cost-benefit analysis based on these
                                                    technologies.
                                                </li>
                                                <li>
                                                    Expert-level knowledge in Web 2.0 technologies, including AJAX,
                                                    social networking and blog platforms, widget development, mobile and
                                                    emerging digital interfaces.
                                                </li>
                                                <li>
                                                    Advanced knowledge and hands on experience with various CMS
                                                    platforms, Drupal in particular.
                                                </li>
                                                <li>
                                                    Experience working with an eCommerce platform, Demandware preferred.
                                                </li>
                                                <li>
                                                    Familiarity with each of the various UML forms and expertise in the
                                                    development of use cases, class diagrams, and occasionally state
                                                    diagrams.
                                                </li>
                                                <li>
                                                    An excellent understanding of best practices across technologies
                                                    (client/server-side, QA, deployment, project management etc.).
                                                </li>
                                                <li>
                                                    SAs work with resource managers to define overall recruiting needs,
                                                    in addition to collaborating with Technology Directors to develop
                                                    estimates and overall implementation of solution plans.
                                                </li>
                                                <li>
                                                    Excellent time management, problem solving, teamwork, and
                                                    communication skills.
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane " id="tab-03">
                                <div class="accordeon js-accordeon">
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            Digital Marketing Executive
                                        </h5>
                                        <div class="accordeon__content">
                                            <p>
                                                Here at UniKwan, we are the leading company in our industry in the
                                                Capital City area. We are hiring an experienced Digital Marketing to
                                                help us keep growing. If you're dedicated and ambitious, UniKwan is an
                                                excellent place to grow your career. Don't hesitate to apply.

                                            </p>
                                            <p>Develops strong and innovative digital marketing strategies, using SEO,
                                                PPC, and other techniques to drive traffic to company pages and generate
                                                interest in company products and services. Creates engaging written,
                                                graphic, and video content while staying up-to-date on latest marketing
                                                technologies and social media.
                                            </p><br>
                                            <h6 class="role-req">Role Responsibilities </h6>
                                            <ul class="list-marker">
                                                <li>
                                                    Develop and implement SEO and PPC strategies
                                                </li>
                                                <li>
                                                    Create and manage link building strategies, content marketing
                                                    strategies, and social media presences
                                                </li>
                                                <li>
                                                    Innovate and present new marketing platforms and strategies
                                                </li>
                                                <li>
                                                    Develop engaging online content including clickbait, forums, videos,
                                                    graphics, and blogs; monitor and analyse content success
                                                </li>
                                                <li>
                                                    Forecast marketing campaign growth and ROI for marketing campaigns
                                                </li>
                                                <li>
                                                    Manage email and social media marketing campaigns
                                                </li>
                                                <li>
                                                    Contact, interview, and hire third party graphic designers, web
                                                    designers, and videographers to create unique and engaging content
                                                </li>
                                                <li>
                                                    Use Google Analytics, Google AdWords, and other relevant sites
                                                </li>
                                                <li>
                                                    Drive traffic to company pages
                                                </li>
                                                <li>
                                                    Develop and manage projects and team members, including delegating
                                                    tasks, reviewing team member work, adhering closely to deadlines and
                                                    to budget, developing and revising ideas, and implementing projects
                                                </li>
                                                <li>
                                                    Keep abreast of new social media sites, web technologies, and
                                                    digital marketing trends; implement these new technologies in
                                                    developing campaigns and update current campaigns to include new
                                                    information
                                                </li>
                                            </ul>
                                            <br>
                                            <h6 class="role-req">Skills and Qualifications </h6>
                                            <ul class="list-marker">
                                                <li>
                                                    Bachelor's Degree in Advertising or Marketing
                                                </li>
                                                <li>
                                                    1-4 Years Experience in Marketing
                                                </li>
                                                <li>
                                                    Strong Written and Verbal Communication Skills
                                                </li>
                                                <li>
                                                    Editing, Self-Motivated, Strong Leadership Skills, Team-Oriented,
                                                    Goal-Oriented
                                                </li>
                                                <li>
                                                    Strong Attention to Detail, SEO, PPC, Google AdWords
                                                </li>
                                                <li>
                                                    Content Management Systems
                                                </li>
                                                <li>
                                                    Photoshop, InDesign, Facebook, Twitter, Microsoft Office
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane " id="tab-04">
                                <div class="accordeon js-accordeon">
                                    <div class="accordeon__item">
                                        <h5 class="accordeon__title">
                                            Business Development Executive
                                        </h5>
                                        <div class="accordeon__content">
                                            <p>
                                                UniKwan is looking for a new Business Development Executive to join our
                                                amazing team!
                                            </p>
                                            <p> As Business Development Executive at UniKwan, you are responsible for
                                                leading our commercial team, exploring opportunities with new and
                                                existing clients around the globe, and helping us build and maintain
                                                industry-wide relationships. You are instrumental in shaping and
                                                steering UniKwan business development strategy in collaboration with the
                                                studio and the team, and as part of the Leadership team, you’ll be
                                                reporting to the MD.</p>
                                            <p>You are a natural self-starter with a strong digital background and
                                                experience in selling, negotiating, and have a broad understanding of
                                                digital solutions across all major platforms alongside project owners,
                                                technical and design teams. In collaboration with the business team, as
                                                well as the rest of UniKwan, you’ll be part of moving the business into
                                                new and exciting spaces.
                                            </p><br>
                                            <h6 class="role-req">REQUIREMENTS</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    You have a strong passion for tech, digital design, products, and
                                                    services. Even better: you’re passionate about helping clients
                                                    create not only the products they’re looking for, but products that
                                                    will have a real impact and make a difference to their business and
                                                    to the world. 
                                                </li>
                                                <li>
                                                    Start to finish experience from network through to pitch. You're
                                                    comfortable with negotiating MSAs, establishing new relationships,
                                                    creating opportunities, qualifying leads, leading RFI/RFP responses,
                                                    crafting bespoke proposals and pitches and contract negotiations.
                                                </li>
                                                <li>
                                                    You're a brilliant storyteller, not only with great communication,
                                                    linguistic and writing skills, but you also have the ability to take
                                                    people on a journey and effectively communicate a vision.
                                                </li>
                                                <li>
                                                    You’re a team player who loves and knows how to get people involved
                                                    and excited about projects in a - at times - hectic work
                                                    environment.
                                                </li>
                                                <li>
                                                    Knowledge and understanding of consultancy studio financial models
                                                    and budgeting processes, both at company and project levels. You
                                                    know how to build a business strategy around finance, what it means
                                                    and looks like to stabilize revenue, ideas around ways to ensure
                                                    profit, and how this all interlinks when it comes to the big
                                                    picture.
                                                </li>
                                                <li>
                                                    You have experience leading people and teams and you know how to
                                                    motivate, engage, and get the best out of the people around you.
                                                </li>
                                                <li>
                                                    You know and understand that business development is not always a
                                                    9-5 job and that some traveling is required. 
                                                </li>
                                            </ul>
                                            <br>
                                            <h6 class="role-req">RESPONSIBILITIES</h6>
                                            <ul class="list-marker">
                                                <li>
                                                    Organize and manage our business development efforts, including
                                                    business strategies, account plans, manage pipeline and forecasting
                                                    - to realize our revenue targets together with your team
                                                </li>
                                                <li>
                                                    Personally manage and grow some of our most important client
                                                    accounts
                                                </li>
                                                <li>
                                                    Grow the sales pipeline, thus ensuring a healthy flow of new
                                                    business opportunities for the studio.
                                                </li>
                                                <li>
                                                    Drive RFPs, pitches, and commercial proposals together with our
                                                    creative teams
                                                </li>
                                                <li>
                                                    Understand and take part in shaping the studio, culture and the
                                                    business team.
                                                </li>
                                                <li>
                                                    Open doors and connect with people, as well as networking with our
                                                    UniKwan community.
                                                </li>
                                                <li>
                                                    Engage with the UniKwan to understand what brands, industries, and
                                                    projects we’re passionate about as well as involve people and create
                                                    excitement around the business development efforts.
                                                </li>
                                                <p class="role-req" style='color:black'>Send Resume to
                                                    <strong>hr@unikwan.com</strong>
                                                </p>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <!-- end section -->
            <section class="section section-default-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xl-4">
                            <div class="section-heading">
                                <h4 class="title">
                                    Internship
                                </h4>

                            </div>
                        </div>
                        <div class="col-md-8 col-xl-8">
                            <strong class="base-color-02">We hire Interns who have passion in the Design domain and are
                                ready to accept challenges. A good sense of trends and knowledge is sought in the
                                applicants. They should be hardworking, smart and self-motivated.
                            </strong>
                            <p>
                                We offer internship opportunities to interested individuals. Our internships are awarded
                                after screening the applications diligently.
                            </p>
                            <div style='height:20px'></div>
                            <ul>
                                <li>
                                    The Internship is for:
                                    <ol>
                                        <li> Those who want to pursue a career in the Design field.
                                        </li>
                                        <li> Those who are looking for summer internships UG and PG students who have an
                                            interest in design.
                                        </li>
                                        <li> Those who are interested in design thinking and critical thinking
                                            Industrialists who are interested to learn about design.
                                        </li>
                                        <li> Those who want to pursue a career in video making.
                                        </li>
                                    </ol>
                                    <p class="role-req" style='color:black'>Send Resume to
                                        <strong>hr@unikwan.com</strong>
                                    </p>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <section class="section section-default-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xl-4">
                            <div class="section-heading">
                                <h4 class="title">
                                    Partner with us
                                </h4>
                            </div>
                        </div>
                        <div class="col-md-8 col-xl-8">
                            <strong class="base-color-02">We work with other equivalent agencies and also with
                                freelancers, independent designers, coders and people with diverse skillsets to augment
                                our capacity to provide unique solutions.
                            </strong>
                            <p>
                                For us, Freelancers are independent professionals who are an essential part of the
                                innovation eco-system. Feel free to reach out with your profile portfolio.
                            </p>
                            <p class="role-req" style='color:black'>Please email us the details <strong>
                                    hr@unikwan.com</strong></p>

                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <?php require '../elements/footerinner.php'; ?>
        </div>
    </main>

    <?php require '../elements/svgcodeinner.php'; ?>
</body>

</html>