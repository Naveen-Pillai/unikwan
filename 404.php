<?php require 'header.php'; ?>

<?php require 'css-links.php'; ?>

</head>

<body>


<!--  preloader start -->
<div id="tb-preloader">
    <div class="tb-preloader-wave"></div>
</div>
<!-- preloader end -->

<div class="wrapper">

    <!--header start-->
    <?php require 'nav.php'; ?>
    <!--header end-->

    <!--hero section-->
    <div  class="parallax text-center vertical-align hero-gradient">
        <div class="container-mid" style="relative">
            <div class="container">
              <div class="row">

                  <div class="m-bot-80 inline-block">
                      <!--title-->
                      <div class="border-short-bottom text-center">
                          <img src="img/custom/flat-design/404.png" class="inner-page-img"/>
                          <h3 class="thank-you">Oops !</h3>
                          <h5 class="thank-you-sub">The page you're trying to reach doesn't exist.</h5>
                      </div>
                      <div class="col-md-12 align-center wow animated fadeInUp delay">
                        <a href="index.php" class="btn btn-circle theme-button hvr-sweep-to-right"> <span class="have-questions-button-text">GO HOME</span></a>
                      </div>
                      <!--title-->
                  </div>

              </div>
            </div>
        </div>
    </div>
    <!--hero section-->

    <!--body content start-->

    <!--body content end-->

    <!--footer 1 start -->
    <?php require 'footer.php'; ?>
    <!--footer 1 end-->
</div>

<?php require 'scripts.php'; ?>


</body>
</html>
