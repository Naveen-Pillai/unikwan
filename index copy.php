<!DOCTYPE html>
<html lang="en">

<head>


    <title>Openstream</title>
    <?php require './global-elements/head-content.php'; ?>
</head>

<body>
    <div id="wrapper">
        <div id="content">
            <!-- Start header -->
            <?php require './global-elements/navbar.php'; ?>
            <!-- End header -->
            <!-- Stat main -->
            <main data-spy="scroll" data-target="#navbar-example2" data-offset="0">
                <!-- Start blog_slider -->
                <section class="demo_1 banner_demo6 banner_section">
                    <div class="container">
                        <div class="row align-items-center mobile-flex-ing">
                            <div class="col-lg-5 mb-5 flex-centerlines">
                                <div class="banner_title flex-centerlines">
                                    <h1 class="c-white">
                                        Heralding the Intelligent Enterprise<span class="tm-text-hero">&#8482;</span>
                                    </h1>
                                    <p style="color: #97CBFF;">
                                        AI Virtual Assistant Platform delivering superior
                                        experiences that are multimodal and context aware.
                                    </p>
                                </div>
                                <a href="request-demo" class="btn rounded-8 c-blue btn_md_primary ripple scale bg-white">
                                    Request demo</a>
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-6">
                                <div class="heroarea-img-slide">


                                    <img width="100%" src="assets/img/rewamp/home/evalogo.svg" alt="Heralding the Intelligent Enterprise" />

                                </div>
                            </div>
                        </div>
                </section>

                <section class="contact__workspace padding-py-10 section__stories blog_slider">
                    <div class="container">
                        <!-- <div class="row padding-py-10 justify-content-center">
                            <div class="col-lg-6">
                                <img src="assets/img/rewamp/home/evalogo.svg" alt="evalogo" width="100%" />
                            </div>
                        </div> -->
                        <div class="row margin-b-10">

                            <div class="col-lg-8">
                                <div class="title_sections mb-0">
                                    <h2 class="c-white">
                                        <span class="c-pink font-w-700">Be the Hero</span> you know you are
                                    </h2>
                                    <p class="c-white be-hero-text">You aren’t afraid to cross the chasm or lean in.
                                        You only work four hours a week (ok, maybe not that).<br /><br />

                                        Your role includes AI, Innovation, CX or Transformation.
                                        You bedazzle customers and make employees superstars.
                                        <br /><br />
                                        You’re on a mission to engage customers, grow revenue, increase profit, delight IT and control risk.
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-4 mt-4 mt-lg-0 text-lg-right my-lg-auto">
                                <div class="cover_blog row justify-content-center">
                                    <img src="assets/img/rewamp/home/evalogo.svg" alt="evalogo" width="100%" />
                                </div>
                            </div>
                        </div>
                        <div class="row margin-b-3">
                            <div class="col-lg-3">
                                <div onclick="openFeatureElements(event, 'feature-item-zero')" class="os-features-box active">
                                    <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                    <p>Discovery in Complexity
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div onclick="openFeatureElements(event, 'feature-item-one')" class="os-features-box">
                                    <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                    <p>Discovery in Complexity
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div onclick="openFeatureElements(event, 'feature-item-two')" class="os-features-box">
                                    <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                    <p>Discovery in Complexity
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div onclick="openFeatureElements(event, 'feature-item-three')" class="os-features-box">
                                    <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                    <p>Discovery in Complexity
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="feature-item-zero" style="display: block;" class="features-tabs-content">
                            <div class="features-tabs-inner">
                                <div class="title_sections mb-0">
                                    <h2 class="c-white">
                                        Webinar Webinar on How State on How State
                                    </h2>
                                    <p class="c-white be-hero-text">
                                        Your role includes AI, Innovation, CX or Transformation.
                                        You bedazzle customers and make employees superstars.
                                        <br /><br />You aren’t afraid to cross the chasm or lean in.
                                        You only work four hours a week (ok, maybe not that).<br /><br />


                                        You’re on a mission to engage customers, grow revenue, increase profit, delight IT and control risk.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="feature-item-one" class="features-tabs-content">
                            <div class="features-tabs-inner">
                                <div class="title_sections mb-0">
                                    <h2 class="c-white">
                                        Webinar Webinar on How
                                    </h2>
                                    <p class="c-white be-hero-text">
                                        Your role includes AI, Innovation, CX or Transformation.
                                        You bedazzle customers and make employees superstars.
                                        <br /><br />
                                        You aren’t afraid to cross the chasm or lean in.
                                        You only work four hours a week (ok, maybe not that).<br /><br />

                                        Your role includes AI, Innovation, CX or Transformation.
                                        You bedazzle customers and make employees superstars.
                                        <br /><br />
                                        You’re on a mission to engage customers, grow revenue, increase profit, delight IT and control risk.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="feature-item-two" class="features-tabs-content">
                            <div class="features-tabs-inner">
                                <div class="title_sections mb-0">
                                    <h2 class="c-white">
                                        Webinar on How State State
                                    </h2>
                                    <p class="c-white be-hero-text">You aren’t afraid to cross the chasm or lean in.
                                        You only work four hours a week (ok, maybe not that).<br /><br />
                                        You’re on a mission to engage customers, grow revenue, increase profit, delight IT and control risk.
                                        Your role includes AI, Innovation, CX or Transformation.
                                        You bedazzle customers and make employees superstars.
                                        <br /><br />
                                        You’re on a mission to engage customers, grow revenue, increase profit, delight IT and control risk.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="feature-item-three" class="features-tabs-content">
                            <div class="features-tabs-inner">
                                <div class="title_sections mb-0">
                                    <h2 class="c-white">
                                        Webinar Webinar on How State Webinar on How State
                                    </h2>
                                    <p class="c-white be-hero-text">You aren’t afraid to cross the chasm or lean in.
                                        You only work four hours a week (ok, maybe not that).<br /><br />

                                        Your role includes AI, Innovation, CX or Transf perstars.
                                        <br /><br />
                                        You’re on a mission to engage customers, grow revenue, increase profit, delight IT and control risk.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="os-features-box main-blue">
                                <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                <p class="c-white">Discovery
                                </p>
                            </div>
                        </div>
                        <div class="row margin-t-2">
                            <div class="col-lg-3">
                                <div class="os-features-box">
                                    <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                    <p>Discovery in Complexity
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="os-features-box">
                                    <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                    <p>Discovery in Complexity
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="os-features-box">
                                    <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                    <p>Discovery in Complexity
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="os-features-box">
                                    <img src="assets/img/rewamp/home/bus-scale.svg" width="45px" alt="icon">
                                    <p>Discovery in Complexity
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Start blog_slider -->
                <section class="padding-py-10 section__stories blog_slider">
                    <div class="container">
                        <div class="row margin-b-4">

                            <div class="col-lg-7">
                                <div class="title_sections mb-0">
                                    <h2 class="c-dark">
                                        <span class='bold-text'>Meet Eva™</span> , a Conversational AI platform that enables business leaders to surpass business goals…
                                        <span class="c-blue">guaranteed</span>.
                                    </h2>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-4 mt-4 mt-lg-0 text-lg-right my-lg-auto">
                                <div class="cover_blog row justify-content-center">
                                    <img src="assets/img/rewamp/home/evalogo.svg" alt="evalogo" width="200px" />
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-4 col-lg-4 swiper-slide">
                                <div class="grid_blog_avatar">
                                    <div class="margin-b-1 row justify-content-center">
                                        <img src="assets/img/rewamp/home/cust-sat.svg" width='260px' alt="Gartner" />
                                    </div>
                                    <div class="body_blog">
                                        <a href="gartner" class="link_blog">
                                            <pre class="c-dark"><i>Openstream</i></pre>
                                            <h4 class="title_blog c-dark">Openstream recognized in "2021
                                                Strategic Roadmap for Enterprise AI: Natural Language
                                                Architecture" -Get the Report</h4>
                                            <p class="c-dark">In recognition of his s alogue, and to the theory and
                                                practice of multimodal interaction</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 swiper-slide">
                                <div class="grid_blog_avatar">
                                    <div class="margin-b-1 row justify-content-center">
                                        <img src="assets/img/rewamp/home/emp-emp.svg" width='260px' alt="Gartner" />
                                    </div>
                                    <div class="body_blog">
                                        <a href="gartner" class="link_blog">
                                            <pre class="c-dark"><i>Openstream</i></pre>
                                            <h4 class="title_blog c-dark">Openstream recognized in "2021
                                                Strategic Roadmap for Enterprise AI: Natural Language
                                                Architecture" -Get the Report</h4>
                                            <p class="c-dark">In r nd to the theory and
                                                practice of multimodal interaction</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 swiper-slide">
                                <div class="grid_blog_avatar">
                                    <div class="margin-b-1 row justify-content-center">
                                        <img src="assets/img/rewamp/home/exe-buy.svg" width='260px' alt="Gartner" />
                                    </div>
                                    <div class="body_blog">
                                        <a href="gartner" class="link_blog">
                                            <pre class="c-dark"><i>Openstream</i></pre>
                                            <h4 class="title_blog c-dark">Openstream recognized in "2021
                                                Strategic Roadmap for Enterprise AI: Natural Language
                                                Architecture" -Get the Report</h4>
                                            <p class="c-dark">In recognition of tion</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-md-center margin-t-6">
                            <a href="request-demo" class="btn rounded-8 c-white btn_md_primary ripple scale bg-blue">
                                Learn More
                            </a>

                        </div>
                </section>

                <!-- Start blog_slider -->
                <section class="contact__workspace padding-py-10 section__stories blog_slider">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="swip__stories">
                                    <!-- Swiper -->
                                    <div class="swiper-container blog-slider">
                                        <div class="title_sections_inner">
                                            <h2 class="c-white">News</h2>
                                        </div>
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="grid_blog_avatar">
                                                    <div class="cover_blog">
                                                        <img src="assets/img/blogs/gartner.png" alt="Gartner" />
                                                    </div>
                                                    <div class="body_blog">
                                                        <a href="gartner" class="link_blog">
                                                            <h4 class="title_blog c-white">Openstream recognized in "2021
                                                                Strategic Roadmap for Enterprise AI: Natural Language
                                                                Architecture" -Get the Report</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                <!-- End grid_blog_avatar -->
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="grid_blog_avatar">
                                                    <div class="cover_blog">

                                                        <picture>
                                                            <source srcset="assets/webp/blogs/philnews.webp" type="image/webp">
                                                            <img src="assets/img/blogs/philnews.png" alt="Dr.Phil Cohen named an ACL Fellow" />
                                                        </picture>
                                                    </div>
                                                    <div class="body_blog">
                                                        <a href="Dr-Phil-Cohen-named-an-ACL-Fellow" class="link_blog">
                                                            <h4 class="title_blog c-white">
                                                                Openstream's Chief Scientist Dr.Phil Cohen named an ACL
                                                                Fellow
                                                            </h4>
                                                        </a>
                                                        <p class="c-white">In recognition of his significant contributions to the study
                                                            of
                                                            communicative action and dialogue, and to the theory and
                                                            practice of multimodal interaction</p>
                                                    </div>
                                                </div>
                                                <!-- End grid_blog_avatar -->
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="grid_blog_avatar">
                                                    <div class="cover_blog">
                                                        <img src="assets/img/blogs/osnews.png" alt="Openstream expands the advisory board" />
                                                    </div>
                                                    <div class="body_blog">
                                                        <a href="Openstream-Expands-Its-Advisory-Board" class="link_blog">
                                                            <h4 class="title_blog c-white">
                                                                Openstream expands the advisory board
                                                            </h4>
                                                        </a>
                                                        <p class="c-white">Company proud to include the visionary leaders in the field
                                                            of Conversational AI on its advisory board </p>
                                                    </div>
                                                </div>
                                                <!-- End grid_blog_avatar -->
                                            </div>


                                        </div>

                                        <!-- Add Arrows -->
                                        <div class="swiper-button-next">
                                            <i class="tio chevron_right"></i>
                                        </div>
                                        <div class="swiper-button-prev">
                                            <i class="tio chevron_left"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>



                <!-- Start group_logo_list -->
                <section class="group_logo_list padding-py-10 ">
                    <div class="container">
                        <div class="row justify-content-center text-center">
                            <div class="col-lg-5">
                                <div class="title_sections_inner margin-b-5">
                                    <h2 class="c-dark">Recognition</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="item_tto py-0 border-0">

                                    <img src="assets/img/logos/clogo2.png" alt="Logo" />
                                    <img src="assets/img/logos/clogo3.png" alt="Logo" />
                                    <img src="assets/img/logos/clogo1.png" alt="Logo" />
                                    <img src="assets/img/logos/clogo4.png" alt="Logo" />
                                    <img src="assets/img/logos/clogo5.png" alt="Logo" />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End. group_logo_list -->
                <section class="contact__workspace padding-py-10">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 mt-4 mt-lg-0 text-lg-right my-lg-auto">
                                <div class="grid_blog_avatar">
                                    <div class="cover_blog">
                                        <img src="assets/img/blogs/gartner.png" alt="Gartner" width="100%" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="title_sections mb-0">
                                    <h2 class="c-white">
                                        Webinar on How State of the Art in Intelligent Virtual
                                        Assistants can accelerate your business in 2021
                                    </h2>
                                    <p class="c-white">
                                        Webinar on How State of the Art in Intelligent Virtual
                                        Assistants can accelerate your business in 2021
                                    </p>
                                    <a href="gartner-webinar" class="btn rounded-8 c-blue btn_md_primary ripple scale bg-white">
                                        Learn More</a>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <!-- Start logos -->
                <!-- End logos -->
                <!-- Start Simple Contact -->

                <?php require './global-elements/request-demo.php'; ?>

                <!-- End Simple Contact -->
            </main>
            <!-- end main -->
        </div>
        <!-- [id] content -->
        <?php require './global-elements/footer.php'; ?>



        <!-- Start Section Loader -->
        <section class="loading_overlay">
            <div class="loader_logo">
                <img class="logo" src="assets/img/homepage/oslogo.png" />
                <!-- <video width="500" autoplay muted>
            <source src="assets/img/video/bluelogo.mp4" type="video/mp4" />
          </video> -->
            </div>
        </section>
        <!-- End. Loader -->


    </div>
    <!-- End. wrapper -->


    <?php require './global-elements/script-loader.php'; ?>
    <script src="./assets/js/main-home.js" type="text/javascript"></script>

</body>

</html>