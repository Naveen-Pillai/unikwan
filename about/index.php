<!DOCTYPE html>
<html lang="en">

<head>
    <title>About Us | Unikwan Innovations | Design Agency In Bangalore</title>
    <meta name="keywords" content="UI/UX Design agency ">
    <meta name="description"
        content="We use an interdisciplinary approach to building next-level user experience design, customer experience design, innovation, and strategic consulting." />

    <?php require '../elements/headerinner.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../elements/navbarinner.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <!-- start section -->
        <section class="section--no-padding section">
            <div class="subpage-header__bg">
                <div class="container">
                    <div class="subpage-header__block">
                        <h1 class="subpage-header__title">About Us</h1>
                        <div class="subpage-header__line"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <section class="section section-default-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-xl-4">
                        <div class="section-heading">
                            <h4 class="title title-lg">Who We Are</h4>

                        </div>
                    </div>
                    <div class="col-md-8 col-xl-8">
                        <strong class="base-color-02">
                            Kwan comes from the word “Kwoon”, which means house, school or style. Founded in 2012,
                            Bengaluru, India, we aim to become design leaders, innovators and drive the digital
                            ecosystem in India and globally.
                        </strong>
                        <p>
                            We use an interdisciplinary approach to building client solutions where technical experts,
                            artists, domain professionals and others with diverse skillsets come together as one: a
                            union of thought aimed at leveraging the varied synergies resulting from this collaboration.
                        </p>
                    </div>
                </div>
                <div class="box-counter-list">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="box-counter">
                                <div class="box-counter__title">Successfully executed</div>
                                <div class="box-counter__value">
                                    <span data-num="200" class="value animate-number">180</span><span class="value">+
                                    </span>
                                    Projects
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="box-counter">
                                <div class="box-counter__title">Funded/Acquired</div>
                                <div class="box-counter__value">
                                    <span data-num="20" class="value animate-number">0</span><span class="value">+
                                    </span>
                                    Clients
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="box-counter">
                                <div class="box-counter__title">Users impacted in</div>
                                <div class="box-counter__value">
                                    <span data-num="10" class="value animate-number">0</span>
                                    Countries
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <section class="section section-default-top">
            <div class="container">
                <div class="section-heading section-heading_indentg04">
                    <div class="description"><i></i>Staff</div>
                    <h4 class="title">Meet Our Team</h4>
                </div>
                <div class="testimonials-uk-mobilevisble">

                    <picture>
                        <source srcset="../webp/unikwan_team.webp" type="image/webp">
                        <img width="100%" src="../images/unikwan_team.png" alt="Team Unikwan" />
                    </picture>
                </div>
                <div class="navbar-uk-mobilehid">
                    <picture>
                        <source srcset="../webp/unikwan_mteam.webp" type="image/webp">
                        <img width="100%" src="../images/unikwan_mteam.png" alt="Team Unikwan" />
                    </picture>

                </div>
            </div>
        </section>
        <!-- end section -->

        <!-- start section -->
        <section class="section section-default-top">
            <div class="container">
                <div class="section-heading section-heading_indentg02">
                    <div class="description"><i></i>Our Values</div>
                    <h4 class="title">Why We Are The Right Choice</h4>
                </div>
                <div class="listicon-col">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="listicon-col__item">
                                <div class="infobox01__title" style='text-align:center'>
                                    Design for everyday life
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="listicon-col__item">
                                <div class="infobox01__title" style='text-align:center'>
                                    Rich Perspective from various sectors
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="listicon-col__item">
                                <div class="infobox01__title" style='text-align:center'>
                                    Open and Critical thinking
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="listicon-col__item">
                                <div class="infobox01__title" style='text-align:center'>
                                    Our talent is nurtured to lead
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="listicon-col__item">
                                <div class="infobox01__title" style='text-align:center'>
                                    ROI Driven
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="listicon-col__item">
                                <div class="infobox01__title" style='text-align:center'>
                                    Diversity
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="listicon-col__item">
                                <div class="infobox01__title" style='text-align:center'>
                                    Inclusion
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="listicon-col__item">
                                <div class="infobox01__title" style='text-align:center'>
                                    Sustainability
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <?php require '../elements/footerinner.php'; ?>

    </main>

    <?php require '../elements/svgcodeinner.php'; ?>
</body>

</html>