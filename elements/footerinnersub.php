 <div class="uk-getintouch-section section-default-top">
     <article class="getintouch-heading">
         Get in touch
     </article>
     <article class="getintouch-subtext">
         Let's create something awesome together!
     </article>
     <a class="btn-link-icon btn-link-icon__md" href="../../contact">
         <i class="btn__icon btn__iconBg">
             <svg>
                 <use xlink:href="#arrow_right"></use>
             </svg>
         </i>
         <span class="btn__text" style="color: white;">CONTACT US</span>
     </a>
 </div>
 <div class="footer-section-bg">
     <div class="footer-section container">
         <div class="footer-top-section">
             <div class="footer-left-part">
                 <img src="../../images/unikwanlogo/colorlogo.png" width="160px" />
                 <article class="footer-subheading">
                     We craft meaningful
                     <span class="footer-subheading-bold">digital experiences </span><br />
                     that resonate with the future
                 </article>
                 <br>
                 <article class="footer-contactheading footer-subheading-bold" style='margin-bottom: 20px;'>Follow us
                     here. </article>
                 <div class="social-icons-blocks-conts">
                     <div class="footeruk-social-holder">
                         <div class="footer-uk-socialimg">
                             <img id="icons" onClick="Gotolink('https://www.facebook.com/UniKwan')"
                                 src="../../images/footer/facebook.png" alt="" style="cursor:pointer" />
                         </div>
                         <div class="footer-uk-socialimg">
                             <img id="icons" onClick="Gotolink('https://twitter.com/UniKwan')"
                                 src="../../images/footer/twitter.png" alt="" style="cursor:pointer" />
                         </div>
                         <div class="footer-uk-socialimg">
                             <img id="icons" onClick="Gotolink('https://www.instagram.com/_unikwan_')"
                                 src="../../images/footer/instagram.png" alt="" style="cursor:pointer;" />
                         </div>
                         <div class="footer-uk-socialimg">
                             <img id="icons" onClick="Gotolink('https://dribbble.com/unikwan_studios')"
                                 src="../../images/footer/dribbble.png" alt="" style="cursor:pointer;" />
                         </div>
                     </div>
                     <div class="footeruk-social-holder">
                         <div class="footer-uk-socialimg">
                             <img onClick="Gotolink('https://medium.com/@UniKwan')" id="icons"
                                 src="../../images/footer/Medium.png" alt="" style="cursor:pointer" />
                         </div>
                         <div class="footer-uk-socialimg">
                             <img id="icons" onClick="Gotolink('https://www.behance.net/unikwaninnovations')"
                                 src="../../images/footer/Behance.png" alt="" style="cursor:pointer" />
                         </div>
                         <div class="footer-uk-socialimg">
                             <img id="icons"
                                 onClick="Gotolink('https://www.linkedin.com/company/unikwan-innovations-private-limited')"
                                 src="../../images/footer/Linkedin.png" alt="" style="cursor:pointer;" />
                         </div>
                         <div class="footer-uk-socialimg">
                             <img id="icons" onClick="Gotolink('https://in.pinterest.com/unikwan_studios/')"
                                 src="../../images/footer/Pinterest.png" alt="" style="cursor:pointer;" />
                         </div>
                     </div>
                 </div>
             </div>
             <div class="footer-right-part">
                 <article class="footer-contactheading">
                     <span class="footer-subheading-bold">Find us here. </span><br />
                     1115, 2nd floor,<br />
                     23rd Main Road, Sector 2, <br />HSR Layout, Bengaluru,<br />
                     Karnataka 560102
                 </article>
                 <div class="footer-rating-Holder">
                     <div class="footer-rating-block">
                         <img class="" width="40px" src="../../images/NEW-VISUAL-DESIGN.svg" />
                         <div class="number-rating-holder">
                             <article class="footer-rating-number">
                                 4.5
                             </article>
                             <article class="footer-rating-number">
                                 Reviews (5)
                             </article>
                         </div>
                     </div>
                     <div class="footer-rating-block">
                         <img class="" width="40px" src="../../images/googleicon.svg" />
                         <div class="number-rating-holder">
                             <article class="footer-rating-number">
                                 4.9
                             </article>
                             <article class="footer-rating-number">
                                 Reviews (16)
                             </article>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="footer-bottom-section">
             <div class="footer-links-holder">
                 <article class="footer-copyrights">
                     Copyrights 2020 UniKwan Innovations Pvt Ltd - Design Agency
                 </article>
             </div>
             <div class="footer-links-holder">
                 <a href='../../terms-of-service' style='text-decoration:none'>
                     <article class="footer-links-right">Terms of Service</article>
                 </a>
                 <a href='../../privacy-policy' style='text-decoration:none'>
                     <article class="footer-links-right">Privacy Policy</article>
                 </a>
                 <a href='../../terms-of-service' style='text-decoration:none'>
                     <article class="footer-links-right">Cookie Policy</article>
                 </a>
             </div>
         </div>
     </div>
 </div>
 <div class='cockie-banner-main' id="CockieBanner">
     <div class='cockie-banner-block'>
         <article class='cockie-banner-text'>
             We use cookies to ensure you get the best experience on our website. If you continue on this page, you
             accept the use of cookies.<br> Read more about our Privacy Policy <a href='privacy-policy'>here</a>.
         </article>
         <div class='cockie-banner-flex'>
             <div onclick="cockiesActions()" class='cockie-banner-btn'>
                 <article class='cockie-btn-text'>ACCEPT & CLOSE</article>
             </div>
         </div>
     </div>
 </div>