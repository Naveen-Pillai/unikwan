<meta charset="utf-8" />
    <meta name="keywords" content="Unikwan | UI UX Design Agency in Bangalore, India" />
    <meta name="author" content="Unikwan" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link rel="icon" type="image/pngs" href="../images/favicon.png">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/favicon.ico" />
    <link rel="stylesheet" href="../css/style.css" />
    <link rel="stylesheet" href="../css/style2.css" />
    <link rel="stylesheet" href="../css/style3.css" />
    <link rel="stylesheet" href="../css/style4.css" />
    <link rel="stylesheet" href="../css/styleuk.css" />
    <link rel="stylesheet" href="../css/heroarea.css" />
    <!-- Load google font
	================================================== -->
    <script>
      WebFontConfig = {
        google: { families: ["Roboto:300,400,400i,500,700", "Montserrat:700"] },
      };
      (function () {
        var wf = document.createElement("script");
        wf.src =
          ("https:" == document.location.protocol ? "https" : "http") +
          "://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js";
        wf.type = "text/javascript";
        wf.async = "true";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(wf, s);
      })();
    </script>
    <script
  src="http://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script src="https://smtpjs.com/v3/smtp.js"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134144391-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134144391-1');
</script>
