<header id="top-bar" class="no-indent-mainContent color-scheme01">
    <div class="container-fluid">
        <div class="row justify-content-between no-gutters ">
            <div class="col-auto side-col d-flex align-items-center text-nowrap">
                <!-- start navigation-toggle -->
                <a href="#" id="top-bar__navigation-toggler">
                    <span></span>
                </a>
                <!-- end navigation-toggle -->
                <!-- start logo -->
                <!-- <a href="index.php" class="top-bar__logo">
					Unikwan
				</a> -->
                <a href="../../" style='margin-left:20px'>
                    <div class='navbar-uk-mobilehid'>
                        <img id='unikwanlogom' src="../../images/unikwanlogo/colorlogo.png" width="160px" />
                    </div>
                </a>
                <a href="../../">
                    <div class='testimonials-uk-mobilevisble'>
                        <img id='unikwanlogo' src="../../images/unikwanlogo/whitelogo.png" width="160px" />
                    </div>
                </a>

                <!-- end logo -->
            </div>
            <div class="col-auto">
                <!-- start desktop menu -->
                <nav id="top-bar__navigation">
                    <ul>
                        <!-- <li>
						<a href="services"><span>SERVICES</span></a>
						</li>
						
						
						<li>
						<a href="portfolio"><span>PORTFOLIO</span></a>
						</li>  -->
                    </ul>
                    <ul class="uk-mobile-navbar">
                        <li><a href="../../services">SERVICES</a></li>
                        <li> <a href="../../portfolio">PORTFOLIO</a></li>
                        <li><a href="../../sectors">SECTORS</a></li>
                        <li><a href="../../about">ABOUT US</a></li>
                        <li><a href="../../blog">BLOG</a></li>
                        <li><a href="../../careers">CAREERS</a></li>
                        <li><a href="../../contact">CONTACT</a></li>
                    </ul>
                </nav>
                <!-- end desktop menu -->
            </div>
            <div class="col-auto side-col">

                <!-- start custom btn -->
                <a href="../../contact" id="top-bar-bbtn" class="top-bar__custom-btn"><span>LET'S TALK</span></a>
                <!-- end custom btn -->
            </div>
        </div>
    </div>
</header>