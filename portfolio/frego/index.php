<!DOCTYPE html>
<html lang="en">

<head>
    <title>Frego Project | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content="Frego">
    <meta name="description"
        content="Check out our latest work for Frego. Users will be able to navigate through recipes by filtering based on Mood and Cuisine." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#2C8663'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>Frego
                                </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Consumer Product
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/frego.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/frego.jpg" alt="Frego Consumer product" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/frego/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/frego/main.jpg" alt="Frego Consumer product" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#2C8663'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Frego</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Consumer Products</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        People lack inspiration and time to think of new food ideas during the hustle of
                                        bustle of day-to-day life. These people are typically between 21-35. They tend
                                        to shop when they’re finishing their commute home. They tend to have no
                                        children, live in populous areas and like to try new things. They want to try
                                        new things but lack confidence and don’t know where to start. They don’t know
                                        what they can cook from ingredients currently in the fridge (kitchen/house).
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                Frego alleviates the stress of deciding what to eat each night by matching meal options
                                to its users based on their mood, tastes and ingredients available. </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/portfolio/frego/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/frego/view.jpg" alt="Frego Screenshot">
                                    </picture>

                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        Users will be able to navigate through these recipes, but the main means of
                                        navigation is by filtering based on Mood and Cuisine.
                                        When a user selects a recipe, they are able to read through the full details of
                                        the recipe, as well as the method needed to cook the option selected.
                                        They will also be able to Like, add to Favourites and rate the recipe. Users
                                        should also be able to share the recipe using the typical social media channels.
                                    </p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>User Research
                                        </li>
                                        <li>Design Sprint
                                        </li>
                                        <li>Information architecture
                                        </li>
                                        <li>Wireframing
                                        </li>
                                        <li>Content Inventory & Copy
                                        </li>
                                        <li>User Interface & Visual Design
                                        </li>
                                        <li>Prototyping</li>
                                        <li>Mobile App Development
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" style="overflow: hidden;">

                <div class="navbar-uk-mobilehid">

                    <picture>
                        <source srcset="../../webp/portfolio/mobile_projects/frego.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/mobile_projects/frego.png" alt="Frego Design" />
                    </picture>
                </div>
                <div class="testimonials-uk-mobilevisble">

                    <picture>
                        <source srcset="../../webp/portfolio/frego/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/frego/screen.jpg" alt="Frego Design" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            The company raised seed capital and incubation in London UK. </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../dsb" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../gigsasa" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>