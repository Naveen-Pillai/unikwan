<!DOCTYPE html>
<html lang="en">

<head>
    <title>Nagara | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content="Nagara">
    <meta name="description"
        content="Check out our latest work for Nagara. The design gave a visual narrative for very technical content." />
    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#21497E'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>Nagra
                                    UEX Studio
                                </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Interactive Infographics for
                                    Tablet

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/Nagara.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/Nagara.jpg"
                        alt="Nagara Web Digital presence" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/Nagara/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/Nagara/main.jpg" alt="Nagara Web Digital presence" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#21497E'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Nagra Kudelski</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>OTT/Digital TV</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Nagra UEX Studio strategized the UEX solutions needed a design to make it easy
                                        to read, visual, fun, straight to the point.
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                Content providers suffer poor content communication that creates lost revenue
                                opportunities. Success is about the Right UEX Design Principles and Optimal
                                Implementation.</div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">

                                    <picture>
                                        <source srcset="../../webp/portfolio/Nagara/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/Nagara/view.jpg" alt="Nagara Screenshot">
                                    </picture>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        Simplifying and clarifying the message. The process involved gathering all he
                                        data and scraping the important information to create hierarchy. The design gave
                                        a visual narrative for a very technical content. </p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>Gathering Data

                                        </li>
                                        <li>Reading Everything

                                        </li>
                                        <li>Finding the Narrative

                                        </li>
                                        <li>Identifying problems

                                        </li>
                                        <li>Creating a hierarchy

                                        </li>
                                        <li>Building a wireframe

                                        </li>
                                        <li>Choosing a format
                                        </li>
                                        <li>Determining a visual approach

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" style="overflow: hidden;">
                <div class="">

                    <picture>
                        <source srcset="../../webp/portfolio/Nagara/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/Nagara/screen.jpg" alt="Nagara Design" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">
                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            The sales teams for UEX found the overall information design very useful for them to gather
                            attention of customers.

                        </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../doorsnmore" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../kiba" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>