<!DOCTYPE html>
<html lang="en">
<head>
  <title>Projects & Works | Unikwan Design Studio Portfolio</title>
  <meta name="description" content="Welcome to our design portfolio. Check out our latest projects here. Have a project in mind? Contact us today." />
  <meta name="keywords" content="Portfolio">
  <?php require '../elements/headerinner.php'; ?>
   </head>
<body>
<!-- start Header -->
<?php require '../elements/navbarinner.php'; ?>
<!-- end Header -->
<main id="mainContent">
	<div class="section--bg-wrapper-02">
		<!-- start section -->
		<section class="section--no-padding section">
			<div class="">
				<div class="subpage-header__bg bg-02" >
					<div class="container">
						<div class="subpage-header__block">
							<h1 class="subpage-header__title">Portfolio</h1>
							<div class="subpage-header__line"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
		<!-- start section -->
		<section class="section section-default-top section--bg-00">
			<div class="container">
				<div class="nav-tabs-dafault">
					<!-- Nav tabs -->
					<div class='tabs-uk-overflow'>
					<ul class="nav nav-tabs" style='min-width:440px'>
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#tab-01">ALL</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tab-03">USER EXPERIENCE</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tab-02">CUSTOMER EXPERIENCE </a>
						</li>
						
					</ul>
					</div>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="tab-01">
						<div class="list-videoblock">
					<div class="row">
					<div class="col-sm-6">
							<a href="kiba" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_kiba2">
								
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Kiba.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Kiba.jpg" alt="kiba">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Kiba</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/170746442?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								
								<picture>
								<source srcset="../webp/portfolio/cx_cards/hpe.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/hpe.jpg" alt="hpe">
								</picture>
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">HPE </h4>
								<div class="promobox03__show">
									<p>Corporate Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="dsb" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_dsb">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/DSB.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/DSB.jpg" alt="dsb">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">DSB</h4>
								<div class="promobox03__show">
									<p>Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/127231540?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/arkin.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/arkin.jpg" alt="arkin">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Arkin Net</h4>
								<div class="promobox03__show">
									<p>Explainer Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="frego" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_frego">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/frego.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/frego.jpg" alt="frego">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Frego</h4>
								<div class="promobox03__show">
									<p> Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/109419199?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/lensbricks.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/lensbricks.jpg" alt="lensbricks">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Lens Bricks </h4>
								<div class="promobox03__show">
									<p>Explainer Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="gigsasa" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_gigs">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Gigsasa.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Gigsasa.jpg" alt="gigsasa">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Gigsasa</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/77578593?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/emle.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/emle.jpg" alt="emle">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div>
							 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">eMLE </h4>
								<div class="promobox03__show">
									<p>Explainer Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="dialogtv" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_kiba">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/DialogTv.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/DialogTv.jpg" alt="dialog">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Dialog TV</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>  
						<div class="col-sm-6">
							<a href="https://player.vimeo.com/video/157925562?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/Yourstorybasha.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/Yourstorybasha.jpg" alt="yourstory">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">YourStory Basha</h4>
								<div class="promobox03__show">
									<p>Launch Promo</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="edtech" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_colored">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Edtech.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Edtech.jpg" alt="edtech">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">EdTech</h4>
								<div class="promobox03__show">
									<p> Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="dishtv" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_kiba">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Dishtv.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Dishtv.jpg" alt="dishtv">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Dishtv</h4>
								<div class="promobox03__show">
									<p> Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="innovatekarnataka" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_edfina">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/InnovateKarnataka.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/InnovateKarnataka.jpg" alt="innovatekarnataka">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Innovate Karnataka</h4>
								<div class="promobox03__show">
									<p> Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="atyati" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_atyati">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Atyati.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Atyati.jpg" alt="atyati">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Atyati</h4>
								<div class="promobox03__show">
									<p>TV Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="jitfin" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_jitfin">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/jitfin.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/jitfin.jpg" alt="jitfin">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Jitfin</h4>
								<div class="promobox03__show">
									<p>Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						
						
						
						<div class="col-sm-6">
							<a href="lightmetrics" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_lightm">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/LightMetrics.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/LightMetrics.jpg" alt="lightmetrics">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Light Metrics</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="edfina" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_edfina">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Edfina.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Edfina.jpg" alt="edfina">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Edfina</h4>
								<div class="promobox03__show">
									<p> Website</p>
								</div>
								</div>
							</div>
							</a>
						</div> 
						
						<div class="col-sm-6">
							<a href="doorsnmore" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_edfina">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Doors.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Doors.jpg" alt="doors">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
									<h4 class="promobox03__title">Doors & More</h4>
									<div class="promobox03__show">
										<p> Website</p>
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="nagra" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_colored">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Nagara.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Nagara.jpg" alt="nagra">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
									<h4 class="promobox03__title">Nagara</h4>
									<div class="promobox03__show">
										<p> Website</p>
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/160249555?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/yourstory.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/yourstory.jpg" alt="yourstory">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">YourStory </h4>
								<div class="promobox03__show">
									<p>YourStory Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/155485166?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/in.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/in.jpg" alt="IN">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">I Node  </h4>
								<div class="promobox03__show">
									<p>Corporate Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/178180367?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/ratnaakar.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/ratnaakar.jpg" alt="ratnakar">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Ratnaakar</h4>
								<div class="promobox03__show">
									<p>Logo Launch Promo</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="https://player.vimeo.com/video/215614513?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/RareRabit.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/RareRabit.jpg" alt="rarerabit">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Rare Rabbit</h4>
								<div class="promobox03__show">
									<p> Ad Commercial</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/130767730?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/treatcard.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/treatcard.jpg" alt="treatcard">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div>  
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">TreatCard</h4>
								<div class="promobox03__show">
									<p>Animation</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/103690785?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/trooper.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/trooper.jpg" alt="trooper">
								</picture>
								
								<div class="videoblock__icon"></div>
							</div>  
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Troopr</h4>
								<div class="promobox03__show">
									<p>Brand Promo</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						
					</div> 
				</div>
					</div>
						<div class="tab-pane " id="tab-02">
						<div class="list-videoblock">
					<div class="row">
					<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/170746442?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								
								<picture>
								<source srcset="../webp/portfolio/cx_cards/hpe.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/hpe.jpg" alt="hpe">
								</picture>
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">HPE </h4>
								<div class="promobox03__show">
									<p>Corporate Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
					 
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/127231540?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/arkin.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/arkin.jpg" alt="arkin">
								</picture>
								<div class="videoblock__icon"></div>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Arkin Net</h4>
								<div class="promobox03__show">
									<p>Explainer Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						 
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/109419199?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/lensbricks.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/lensbricks.jpg" alt="lensbricks">
								</picture>
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Lens Bricks </h4>
								<div class="promobox03__show">
									<p>Explainer Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/77578593?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/emle.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/emle.jpg" alt="emle">
								</picture>
								<div class="videoblock__icon"></div>
							</div>
							 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">eMLE </h4>
								<div class="promobox03__show">
									<p>Explainer Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/160249555?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/yourstory.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/yourstory.jpg" alt="yourstory">
								</picture>
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">YourStory </h4>
								<div class="promobox03__show">
									<p>YourStory Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						 
						<div class="col-sm-6">
							<a href="https://player.vimeo.com/video/157925562?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/Yourstorybasha.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/Yourstorybasha.jpg" alt="yourstorybasha">
								</picture>
								<div class="videoblock__icon"></div>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">YourStory Basha</h4>
								<div class="promobox03__show">
									<p>Launch Promo</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/155485166?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/in.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/in.jpg" alt="IN">
								</picture>
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">I Node  </h4>
								<div class="promobox03__show">
									<p>Corporate Video</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/178180367?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/ratnaakar.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/ratnaakar.jpg" alt="ratnakar">
								</picture>
								<div class="videoblock__icon"></div>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Ratnaakar</h4>
								<div class="promobox03__show">
									<p>Logo Launch Promo</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="https://player.vimeo.com/video/215614513?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/RareRabit.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/RareRabit.jpg" alt="rarerabit">
								</picture>
								<div class="videoblock__icon"></div>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Rare Rabbit</h4>
								<div class="promobox03__show">
									<p> Ad Commercial</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/130767730?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/treatcard.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/treatcard.jpg" alt="treatcard">
								</picture>
								<div class="videoblock__icon"></div>
							</div>  
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">TreatCard</h4>
								<div class="promobox03__show">
									<p>Animation</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/103690785?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/trooper.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/trooper.jpg" alt="trooper">
								</picture>
								<div class="videoblock__icon"></div>
							</div>  
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Troopr</h4>
								<div class="promobox03__show">
									<p>Brand Promo</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						 
						<div class="col-sm-6">
						<a href="https://player.vimeo.com/video/167714445?autoplay=1&title=0&byline=0&portrait=0" class=" videoblock promobox03 block-once video-popup ">
							<div class="promobox03__img">
								<picture>
								<source srcset="../webp/portfolio/cx_cards/in.webp" type="image/webp">
								<img src="../images/portfolio/cx_cards/in.jpg" alt="i node">
								</picture>
								<div class="videoblock__icon"></div>
							</div> 
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">I Node Promo</h4>
								<div class="promobox03__show">
									<p>Animation & VFX</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						
						
						 
						
						
						
						
						
						
						
					</div>
				</div>
					</div>
						<div class="tab-pane " id="tab-03">
						<div class="list-videoblock">
					<div class="row">
					<div class="col-sm-6">
							<a href="kiba" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_kiba2">
							<picture>
								<source srcset="../webp/portfolio/thumbnails/Kiba.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Kiba.jpg" alt="kiba">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Kiba</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="dsb" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_dsb">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/DSB.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/DSB.jpg" alt="dsb">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">DSB</h4>
								<div class="promobox03__show">
									<p>Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="frego" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_frego">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/frego.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/frego.jpg" alt="frego">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Frego</h4>
								<div class="promobox03__show">
									<p> Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="gigsasa" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_gigs">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Gigsasa.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Gigsasa.jpg" alt="gigsasa">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Gigsasa</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="dialogtv" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_kiba">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/DialogTv.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/DialogTv.jpg" alt="dialog">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Dialog TV</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="investkarnataka" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_kiba2">
							<picture>
								<source srcset="../webp/portfolio/thumbnails/InvestKarnataka.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/InvestKarnataka.jpg" alt="investkarnataka">
								</picture>
								
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Invest Karnataka</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="edtech" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_colored">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Edtech.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Edtech.jpg" alt="edtech">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">EdTech</h4>
								<div class="promobox03__show">
									<p> Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="dishtv" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_kiba">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Dishtv.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Dishtv.jpg" alt="dishtv">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Dishtv</h4>
								<div class="promobox03__show">
									<p> Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="innovatekarnataka" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_edfina">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/InnovateKarnataka.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/InnovateKarnataka.jpg" alt="innovatekarnataka">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Innovate Karnataka</h4>
								<div class="promobox03__show">
									<p> Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="atyati" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_atyati">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Atyati.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Atyati.jpg" alt="atyati">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Atyati</h4>
								<div class="promobox03__show">
									<p>TV Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="jitfin" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_jitfin">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/jitfin.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/jitfin.jpg" alt="jitfin">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Jitfin</h4>
								<div class="promobox03__show">
									<p>Website</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						
						
						
						<div class="col-sm-6">
							<a href="lightmetrics" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_lightm">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/LightMetrics.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/LightMetrics.jpg" alt="lightmetrics">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Light Metrics</h4>
								<div class="promobox03__show">
									<p>Mobile Application</p>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="edfina" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_edfina">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Edfina.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Edfina.jpg" alt="edfina">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
								<h4 class="promobox03__title">Edfina</h4>
								<div class="promobox03__show">
									<p> Website</p>
								</div>
								</div>
							</div>
							</a>
						</div> 
						
						<div class="col-sm-6">
							<a href="doorsnmore" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_edfina">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Doors.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Doors.jpg" alt="doors">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
									<h4 class="promobox03__title">Doors & More</h4>
									<div class="promobox03__show">
										<p> Website</p>
									</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="nagra" class=" videoblock promobox03 block-once">
							<div class="promobox03__img promobox_colored">
								<picture>
								<source srcset="../webp/portfolio/thumbnails/Nagara.webp" type="image/webp">
								<img src="../images/portfolio/thumbnails/Nagara.jpg" alt="nagara">
								</picture>
							</div>
							<div class="promobox03__description">
								<div class="promobox03__layout" style='background:transparent'>
									<h4 class="promobox03__title">Nagara</h4>
									<div class="promobox03__show">
										<p> Website</p>
									</div>
								</div>
							</div>
							</a>
						</div>
					</div>
				</div>
					</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
		<?php require '../elements/footerinner.php'; ?>
	</div>
</main>
<?php require '../elements/svgcodeinner.php'; ?>
</body>
</html>