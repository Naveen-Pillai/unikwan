<!DOCTYPE html>
<html lang="en">

<head>
    <title>Kiba Project | Unikwan Design Studio Portfolio</title>
    <meta name="description"
        content="Check out our latest work for Kiba. We have designed the  UX keeping in mind that users should not worry about losing any moments." />
    <meta name="keywords" content="Kiba">
    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#F67143'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>KIBA
                                </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">World's First Self Editing
                                    Camera
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/Kiba.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/Kiba.jpg" alt="Kiba UX Design Project" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/kiba/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/kiba/main.jpg" alt="Kiba UX Design Project" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#F67143'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Lensbricks</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Consumer Tech</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Kiba allows users to enjoy their best moments with friends and family. Kiba was
                                        conceptualised as an application to imaging technology and artificial
                                        intelligence. </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                It uses intelligent, patented "joy ranking" technology to capture and curate footage,
                                providing users with beautifully edited, easily shareable video clips.</div>
                            <br>
                            <br>
                            <br>
                        </div>

                        <div class='portfilio-video-holdr'>
                            <div class="navbar-uk-mobilehid">
                                <iframe
                                    src="https://player.vimeo.com/video/140883335?autoplay=1&title=0&byline=0&portrait=0"
                                    width="300" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen
                                    allowfullscreen></iframe>
                            </div>
                            <div class="testimonials-uk-mobilevisble">
                                <iframe
                                    src="https://player.vimeo.com/video/140883335?autoplay=1&title=0&byline=0&portrait=0"
                                    width="1089" height="613" frameborder="0" webkitallowfullscreen mozallowfullscreen
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">

                                    <picture>
                                        <source srcset="../../webp/portfolio/kiba/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/kiba/view.jpg" alt="Kiba Video">
                                    </picture>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        The design for the mobile apps acts as an interface between the machine and the
                                        user. The user experience was designed keeping in mind that users should not
                                        worry about losing any moments and that it has been captured and processed. It
                                        is easily accessible and editable as well. </p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>Lean UX
                                        </li>
                                        <li>Conceptualisation </li>
                                        <li> Information architecture
                                        </li>
                                        <li>Wireframing
                                        </li>
                                        <li>Visual Layout using Android & iOS Guidelines
                                        </li>
                                        <li>Prototyping </li>
                                        <li>UX writing
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/mobile_projects/kiba.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/mobile_projects/kiba.png" alt="Kiba Screenshot" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/kiba/screen.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/kiba/screen.jpg" alt="Kiba Screenshot" />
                </picture>
            </div>
            <br>
            <br>
            <section class="section" style="overflow: hidden;">
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            Kiba has been recognised as a CES 2016 Innovation Award honoree in two categories: "Portable
                            Media Players and Accessories" and "High-Performance Home Audio/Video".
                        </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../nagra" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../dsb" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>