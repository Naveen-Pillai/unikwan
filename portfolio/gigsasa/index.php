<!DOCTYPE html>
<html lang="en">

<head>
    <title>Gigsasa Project | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content="Gigsasa">
    <meta name="description"
        content="Check out our latest work for Gigsasa. We ran a competitor analysis comparing Gigsasa with other platforms." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#008A48'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Mobile Application</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>GIGSASA
                                </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Gig Economy Job Platform</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/Gigsasa.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/Gigsasa.jpg"
                        alt="Gigsasa | Gig economy job platform" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/gigsasa/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/gigsasa/main.jpg"
                        alt="Gigsasa | Gig economy job platform" />
                </picture>

            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#008A48'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Gigsasa</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Enterprise</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        The struggle of finding a job and the right candidate in the service industry is
                                        real. Gigsasa is trying to reshape the employment landscape for professionals
                                        and hourly employers by making it instant, accessible, and efficient.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                In today’s digitalized world, where most things happen with a tap or swipe, applying for
                                a job and the recruitment process should be time saving and cost-effective.
                            </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">

                                    <picture>
                                        <source srcset="../../webp/portfolio/gigsasa/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/gigsasa/view.jpg" alt="Gigsasa Screenshot">
                                    </picture>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        We ran a competitor analysis comparing Gigsasa with other platforms. We
                                        evaluated them against standard parameters.
                                    </p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>Defining the problem and brand</li>
                                        <li>Identifying the use cases</li>
                                        <li>Information Architecture</li>
                                        <li>Wireframing </li>
                                        <li>Visual Design</li>
                                        <li>Hybrid Development </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" style="overflow: hidden;">
                <div class="navbar-uk-mobilehid">

                    <picture>
                        <source srcset="../../webp/portfolio/mobile_projects/frego.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/mobile_projects/frego.jpg" alt="Gigsasa Design" />
                    </picture>
                </div>
                <div class="testimonials-uk-mobilevisble">

                    <picture>
                        <source srcset="../../webp/portfolio/gigsasa/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/gigsasa/screen.jpg" alt="Gigsasa Design" />
                    </picture>

                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            We feel that we tackled the project successfully. Our prototype met the needs of our job
                            seeker persona by making the job searching process quick, easy, convenient, and transparent.
                        </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../frego" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../dialogtv" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>