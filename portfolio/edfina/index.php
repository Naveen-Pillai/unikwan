<!DOCTYPE html>
<html lang="en">
  <head>
  <title>Edfina Project | Unikwan Design Studio Portfolio </title>
  <meta name="keywords" content="Edfina">

  <meta name="description" content="Check out our latest work for Edfina. The website launch saw customers and potential partners from various countries taking notice and increasing the leads and collaborations." />
  
    <?php require '../../elements/headerinnersub.php'; ?>
   </head>
  <body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
      <div class="section--bg-wrapper-02">
        <!-- start section -->
        <section class="section--no-padding section">
          <div class="">
            <div class="subpage-header__bg bg-02" style='background:#171F47'>
              <div class="container">
                <div class="subpage-header__block">
                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                  <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>EDFINA</h1>
                  <div class="subpage-header__caption gigsasa_heading_max">Web Digital Experience</div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- end section -->
        <div class="navbar-uk-mobilehid">
           
            <picture>
              <source srcset="../../webp/portfolio/thumbnails/Edfina.webp" type="image/webp">
              <img
              width="100%"
              src="../../images/portfolio/thumbnails/Edfina.jpg"
              alt="Edfina Web Digital presence"
            />
            </picture>
          </div>
        <div class="testimonials-uk-mobilevisble">
            
            <picture>
              <source srcset="../../webp/portfolio/edfina/main.webp" type="image/webp">
              <img
              width="100%"
              src="../../images/portfolio/edfina/main.jpg"
              alt="Edfina Web Digital presence"
            />
            </picture>
          </div>
        <!-- start section -->
        <section class="section portfolio-margin-uk" style="overflow: hidden;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="chessbox chessbox-top-01">
						<div class="chessbox__item">
							<div class="chessbox__img">
              <div class="box-table-01" style='background:#171F47'>
                  <table>
                    <tbody>
                      <tr>
                        <td>Category:</td>
                        <td>User Experience</td>
                      </tr>
                      <tr>
                        <td>Client:</td>
                        <td>GrayMatters</td>
                      </tr>
                      <tr>
                        <td>Industry:</td>
                        <td>Education</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
							</div>
							<div class="chessbox__description">
								<div class="section-heading">
									<h4 class="title">Purpose</h4>
								</div>
								<p>
                EdFina has been setup as a vehicle to achieve the objectives of providing access to education and improving the quality of education leading to better employability. EdFina will follow a Consultative, Collaborative and Partnership approach within Education ecosystem.
              </p>
							</div>
						</div>
						 
					</div>
				</div>
			</div>
		</section>
    <section class="section portfolio-margin-body" style="overflow: hidden;">
			<div class="container">
				<div class="row justify-content-center">
        <div class="col-10 col-lg-8">
						<div class="text-center text-md " style='color:black'>
            EdFina intends to provide these resources at an affordable price point by reducing the cost of the entire ecosystem.
            <br><br>
EdFina aims to achieve these objectives sustainably, by earning with the ecosystem, and not off it.
						</div>
					</div>
					<div class="chessbox chessbox-top-01">
						<div class="chessbox__item" style='display:none'> </div>
						<div class="chessbox__item">
							<div class="chessbox__img">
              <picture>
              <source srcset="../../webp/portfolio/edfina/view.webp" type="image/webp">
              <img src="../../images/portfolio/edfina/view.jpg" alt="Edfina Screenshot">
            </picture>
								
							</div>
							<div class="chessbox__description">
								<div class="section-heading">
									<h4 class="title">Defining the Solution</h4>
								</div>
								<p>
                EdFina has been setup as a vehicle to achieve the objectives of providing access to education and improving the quality of education leading to better employability. EdFina will follow a Consultative, Collaborative and Partnership approach within Education ecosystem.
              </p>
              <h6 class="sub-title">What we Did</h6>
              <ul class="list-marker">
                      <li>User Research
</li>
                      <li>Design Sprint
</li>
                      <li>Information architecture
</li>
                      <li>Wire-framing
</li>
                      <li>Content Inventory & Copy
</li>
                      <li>User Interface & Visual Design
</li>
                      <li>Prototyping</li>
                    </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
         
        <section class="section" style="overflow: hidden;">
        <div class="">
           
            <picture>
              <source srcset="../../webp/portfolio/edfina/screen.webp" type="image/webp">
              <img
              width="100%"
              src="../../images/portfolio/edfina/screen.jpg"
              alt="Edfina Design"
            />
            </picture>
          </div>
          <br>
          <br>
          <div class="">
            
          <picture>
              <source srcset="../../webp/portfolio/edfina/screen2.webp" type="image/webp">
              <img
              width="100%"
              src="../../images/portfolio/edfina/screen2.jpg"
              alt="Edfina Design"
            />
            </picture>
           
          </div>
          <br>
          <br>
			<div class="container">
     
      <div class="chessbox__description container">
								<div class="section-heading">
									 
									<h4 class="title" style='text-align:center;'>Impact of Design</h4>
								</div>
                <div class="text-center text-md " style='text-align:center;color:black'>
                The website launch saw customers and potential partners from various countries taking notice and increasing the leads and collaborations.	</div>
            <br>
            <br>
            <div class="page-nav-img">
              <a href="../lightmetrics" class="page-nav-img__btn btn-prev">
                <span class="wrapper-img"></span>
                <i class="btn__icon">
                  <svg><use xlink:href="#arrow_left"></use></svg>
                </i>
                <span class="btn__text">PREV</span>
              </a>
              <a href="../" class="">
                <div>
                  <img
                  class='projectsHome-btn'
                    src="../../images/portfolio/projects_home.png"
                    alt="home"
                  />
                </div>
              </a>
              <a href="../doorsnmore" class="page-nav-img__btn btn-next">
                <span class="wrapper-img"></span>
                <span class="btn__text">NEXT</span>
                <i class="btn__icon">
                  <svg><use xlink:href="#arrow_right"></use></svg>
                </i>
              </a>
            </div>
              </div>
              
    </section>
     
        <?php require '../../elements/footerinnersub.php'; ?>
	</div>
</main>
<?php require '../../elements/svgcodeinnersub.php'; ?>
  </body>
</html>
