<!DOCTYPE html>
<html lang="en">

<head>
    <title>EdTech Project | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content=" Edtech ">
    <meta name="description"
        content="Check out our latest work for EdTech. The Design Sprint process of 4 days with a team of 6 people seemed to be the best approach for this project. Keyword: Edtech" />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#225290'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>GEFC
                                </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Edtech Platform</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/Edtech.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/Edtech.jpg" alt="EdTech Platform" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/edtech/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/edtech/main.jpg" alt="EdTech Platform" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#225290'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>GrayMatters</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Education</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Affordable and private schools have difficulty in getting loans from banks. For
                                        them to provide quality education, they not only need financial support but also
                                        access to other services like curriculum planning, scholarships for students,
                                        keeping parents involved in their child's educational growth, improving teaching
                                        skills, using new teaching methods.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                The platform enables schools, teachers,
                                parents and students with the world’s
                                best education resources at their
                                fingertips.
                                <br><br>Two people from the client's team had visited Lagos, Nigeria. It was important
                                for the rest of us to know about their experience and the problems they identified while
                                interacting with the school authorities and teachers.
                            </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/portfolio/edtech/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/edtech/view.jpg" alt="EdTech Screenshot">
                                    </picture>

                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        The Design Sprint process of 4 days with a team of 6 people
                                        seemed to be the best approach for this project.
                                    </p>
                                    <h6 class="sub-title">User Research
                                    </h6>
                                    <ul class="list-marker">
                                        <li>Design Sprint</li>
                                        <li>Product Visualisation</li>
                                        <li>Information architecture</li>
                                        <li>Wireframing
                                        </li>
                                        <li>User Interface</li>
                                        <li>Prototyping
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" style="overflow: hidden;">

                <div class="navbar-uk-mobilehid">

                    <picture>
                        <source srcset="../../webp/portfolio/mobile_projects/edtech.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/mobile_projects/edtech.png" alt="EdTech Design" />
                    </picture>
                </div>
                <div class="testimonials-uk-mobilevisble">

                    <picture>
                        <source srcset="../../webp/portfolio/edtech/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/edtech/screen.jpg" alt="EdTech Design" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            Using the Design Sprint, we transformed an idea into a functioning prototype that can be
                            tested with the target audience.
                        </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../investkarnataka" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../dishtv" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>