<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dish TV Project | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content=" Dish TV ">

    <meta name="description"
        content="Check out our latest work for Dish TV. The design is one of its kind and inspired the design philosophy of DishTV design language for the hardware and UI." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#CC333E'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>Nagra
                                    Kudelski </h1>
                                <div class="subpage-header__caption gigsasa_heading_max"> DishTV EPG Design with New Age
                                    Remote

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/Dishtv.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/Dishtv.jpg" alt="Dish EPG Design" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/Dishtv/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/Dishtv/main.jpg" alt="Dish EPG Design" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#CC333E'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Nagra Kudelski</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Digital TV</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Nagra Kudelski intends to design and migrate Dish TV EPG UI to next-generation
                                        UI. The UI design intent is to cater to Metro and Tier 1 audience. The remote
                                        for the next-gen UI is also re-designed to make a significant impact on how
                                        users interact with EPG.
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                This is the most important phase of the design work as change in UI and most importantly
                                the
                                change in the design of remote will have a substantial impact on the user interactions.
                            </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/portfolio/Dishtv/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/Dishtv/view.jpg" alt="Dish TV Screenshot">
                                    </picture>

                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        We approach each project as a true collaboration in ideation --the process of
                                        creating new Ideas. The methodology will be to understand the impact on users
                                        interaction based on the change in the design of remote and UI of
                                        next-generation version 2.
                                        <br><br>The idea is to keep the UX process “LEAN” to deploy changes in UI
                                        quickly.
                                        UniKwan will work in line with the project management plan to execute the
                                        planned items as
                                        per phases keeping it agile and ensuring timely delivery.
                                    </p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>Identifying the primary and secondary function of the EPG and Remote.

                                        </li>
                                        <li>Information Architecture

                                        </li>
                                        <li>Structural and Navigational Design of interface

                                        </li>
                                        <li>Compositional Design of Interface (Creation of HD UI proposal)

                                        </li>
                                        <li>Creating assets for the HD version

                                        </li>
                                        <li>Usability study of the remote

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" style="overflow: hidden;">

                <div class="navbar-uk-mobilehid">
                    <picture>
                        <source srcset="../../webp/portfolio/mobile_projects/dishtv.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/mobile_projects/dishtv.png"
                            alt="Dish TV Design" />
                    </picture>

                </div>
                <div class="testimonials-uk-mobilevisble">

                    <picture>
                        <source srcset="../../webp/portfolio/Dishtv/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/Dishtv/screen.jpg" alt="Dish TV Design" />
                    </picture>
                </div>
                <br>
                <div class="">

                    <picture>
                        <source srcset="../../webp/portfolio/Dishtv/screen2.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/Dishtv/screen2.jpg" alt="gigsasa" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">
                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            The design is one of its kind and inspired the design philosophy of DishTV design language
                            for the hardware and UI.
                        </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../edtech" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../innovatekarnataka" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>