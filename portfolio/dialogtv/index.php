<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dialog TV Project | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content="Dialog TV">
    <meta name="description"
        content="Check out our latest work for Dialog TV. The design solution was crafted around giving a seamless experience while introducing a plethora of new features." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#E3544A'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>Dialog
                                </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">TV EPG Design</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/DialogTv.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/DialogTv.jpg" alt="Dialog TV EPG Design" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/dialogTV/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/dialogTV/main.jpg" alt="Dialog TV EPG Design" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#E3544A'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Dialog</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>OTT/Digital TV</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Dialog Television, Sri Lanka’s premier pay television service provider,
                                        announced the launch of the next-generation television revolution with a smart
                                        device, powered by Android TV™. <br><br>This portable device powered by Android
                                        TV allows any user to convert their regular television to a Smart TV, by simply
                                        plugging it in and streaming their favourite movies, TV shows, music, games and
                                        more.
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                The Dialog ViU Mini comes with the Google Assistant built-in, which helps you quickly
                                access your favourite entertainment, get answers from Google on-screen, manage tasks,
                                and control smart home devices using your voice. Voice search and personalised
                                recommendations mean you spend more time being entertained and less time hunting for
                                what to watch. </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">

                                    <picture>
                                        <source srcset="../../webp/portfolio/dialogTV/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/dialogTV/view.jpg" alt="Dialog TV Screenshot">
                                    </picture>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        The design solution was crafted around giving a seamless experience while
                                        introducing a plethora of new features. Google Assistant built-in, which helps
                                        you quickly access your favourite entertainment, get answers from Google
                                        on-screen, manage tasks, and control smart home devices using your voice. Voice
                                        search and personalised recommendations mean you spend more time being
                                        entertained and less time hunting for what to watch.</p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>User Interface Usability and Audit
                                        </li>
                                        <li>Information architecture
                                        </li>
                                        <li>Wireframing
                                        </li>
                                        <li>Visual Layout using Android Guidelines
                                        </li>
                                        <li>UX writing
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section" style="overflow: hidden;">

                <div class="navbar-uk-mobilehid">

                    <picture>
                        <source srcset="../../webp/portfolio/mobile_projects/dialogtv.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/mobile_projects/dialogtv.png"
                            alt="Dialog TV Design" />
                    </picture>
                </div>
                <div class="testimonials-uk-mobilevisble">

                    <picture>
                        <source srcset="../../webp/portfolio/dialogTV/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/dialogTV/screen.jpg" alt="Dialog TV Design" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            The launch of the Dialog ViU Mini device will significantly change the television experience
                            of Sri Lankans, allowing them to benefit from an experience powered by Android TV that is
                            both affordable and easily accessible for one of the first times in Sri Lanka. <br>
                            <br>
                            <div class="page-nav-img">
                                <a href="../gigsasa" class="page-nav-img__btn btn-prev">
                                    <span class="wrapper-img"></span>
                                    <i class="btn__icon">
                                        <svg>
                                            <use xlink:href="#arrow_left"></use>
                                        </svg>
                                    </i>
                                    <span class="btn__text">PREV</span>
                                </a>
                                <a href="../" class="">
                                    <div>
                                        <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                            alt="home" />
                                    </div>
                                </a>
                                <a href="../investkarnataka" class="page-nav-img__btn btn-next">
                                    <span class="wrapper-img"></span>
                                    <span class="btn__text">NEXT</span>
                                    <i class="btn__icon">
                                        <svg>
                                            <use xlink:href="#arrow_right"></use>
                                        </svg>
                                    </i>
                                </a>
                            </div>
                        </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>