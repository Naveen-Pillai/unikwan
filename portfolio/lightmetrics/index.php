<!DOCTYPE html>
<html lang="en">

<head>
    <title>LightMetrics Project | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content="LightMetrics">
    <meta name="description"
        content="Check out our latest work for LightMetrics. UniKwan with its lean UX process identified the touchpoints and came up with a better value proposition." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#41A1FF'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>
                                    Lightmetrics </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Web Digital Presence </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/LightMetrics.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/LightMetrics.jpg"
                        alt="LightMetrics Web Digital presence" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">
                <picture>
                    <source srcset="../../webp/portfolio/lightmetrics/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/lightmetrics/main.jpg"
                        alt="LightMetrics Web Digital presence" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#41A1FF'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Lightmetrics</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Telematics</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Light Metrics took a challenge to solve this gap in the driving ecosystem and
                                        bring about a platform that can give valuable insights about the condition of
                                        the vehicle and monitor the driving patterns. The team Lightmetrics wanted to
                                        convey the complex technology in a very simple and approachable manner through
                                        website. </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                Helping mobility businesses become more efficient and productive through a better
                                understanding of driving behaviour </div>
                            <br>
                            <br>
                            <br>
                        </div>

                        <div class='portfilio-video-holdr'>
                            <div class="navbar-uk-mobilehid">
                                <iframe src="https://www.youtube.com/embed/EK3gZ0duVbM" width="300" height="200"
                                    frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                            <div class="testimonials-uk-mobilevisble">
                                <iframe src="https://www.youtube.com/embed/EK3gZ0duVbM" width="1089" height="613"
                                    frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                        </div>


                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">

                                    <picture>
                                        <source srcset="../../webp/portfolio/lightmetrics/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/lightmetrics/view.jpg"
                                            alt="LightMetrics Motion Insights Video">
                                    </picture>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        UniKwan with its lean UX process identified the touchpoints and came up with a
                                        better value proposition. The idea of keeping user safe & focused on driving and
                                        keeping the rest with the app was the approach identified. The various factors
                                        to the offering from technology to solutions. </p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>Lean UX
                                        </li>
                                        <li>Conceptualisation </li>
                                        <li> Information architecture
                                        </li>
                                        <li>Wireframing
                                        </li>
                                        <li>Visual Layout using Android & iOS Guidelines
                                        </li>
                                        <li>Prototyping </li>
                                        <li>UX writing </li>
                                        <li>Web Development
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="">

                <picture>
                    <source srcset="../../webp/portfolio/lightmetrics/screen.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/lightmetrics/screen.jpg"
                        alt="LightMetrics Screenshot" />
                </picture>
            </div>
            <br>
            <br>
            <section class="section" style="overflow: hidden;">
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            Lightmetrics with new digital preens and application saw a double-digit increase in
                            international leads and conversions.</div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../jitfin" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../edfina" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>