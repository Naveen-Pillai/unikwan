<!DOCTYPE html>
<html lang="en">
  <head>
  <title>DSB Project | Unikwan Design Studio Portfolio </title>
  <meta name="keywords" content="DSB">
  <meta name="description" content="Check out our latest work for DSB. We have designed an app to make our transaction process easy." />
  
    <?php require '../../elements/headerinnersub.php'; ?>
   </head>
  <body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
      <div class="section--bg-wrapper-02">
        <!-- start section -->
        <section class="section--no-padding section">
          <div class="">
            <div class="subpage-header__bg bg-02" style='background:#1D74BA'>
              <div class="container">
                <div class="subpage-header__block">
                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                  <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>DSB</h1>
                  <div class="subpage-header__caption gigsasa_heading_max">Consumer Application
</div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- end section -->
        <div class="navbar-uk-mobilehid">
            
            <picture>
              <source srcset="../../webp/portfolio/thumbnails/DSB.webp" type="image/webp">
              <img
              width="100%"
              src="../../images/portfolio/thumbnails/DSB.jpg"
              alt="DSB Consumer Application"
            />
            </picture>
          </div>
        <div class="testimonials-uk-mobilevisble">
            
            <picture>
              <source srcset="../../webp/portfolio/dsb/main.webp" type="image/webp">
              <img
              width="100%"
              src="../../images/portfolio/dsb/main.jpg"
              alt="DSB Consumer Application"
            />
            </picture>
          </div>
        <!-- start section -->
        <section class="section portfolio-margin-uk" style="overflow: hidden;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="chessbox chessbox-top-01">
						<div class="chessbox__item">
							<div class="chessbox__img">
              <div class="box-table-01" style='background:#1D74BA'>
                  <table>
                    <tbody>
                      <tr>
                        <td>Category:</td>
                        <td>User Experience</td>
                      </tr>
                      <tr>
                        <td>Client:</td>
                        <td>Atyati </td>
                      </tr>
                      <tr>
                        <td>Industry:</td>
                        <td>Fintech</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
							</div>
							<div class="chessbox__description">
								<div class="section-heading">
									<h4 class="title">Purpose</h4>
								</div>
								<p>
                With DSB you can do your transaction by staying at your place. The one place for multiple services, with multiple banks.  </p>
							</div>
						</div>
						 
					</div>
				</div>
			</div>
		</section>
    <section class="section portfolio-margin-body" style="overflow: hidden;">
			<div class="container">
				<div class="row justify-content-center">
        <div class="col-10 col-lg-8">
						<div class="text-center text-md " style='color:black'>
            In today's modern world, no one has the time to wait in the long queue at the bank. So with DSB - no more going to the bank.	</div>
					</div>
					<div class="chessbox chessbox-top-01">
						<div class="chessbox__item" style='display:none'> </div>
						<div class="chessbox__item">
							<div class="chessbox__img">
              <picture>
              <source srcset="../../webp/portfolio/dsb/view.webp" type="image/webp">
              <img src="../../images/portfolio/dsb/view.jpg" alt="DSB Doorstep Banking">
            </picture>
							
							</div>
							<div class="chessbox__description">
								<div class="section-heading">
									<h4 class="title">Defining the Solution</h4>
								</div>
								<p>
                The app designed to make our transaction process easy. Tell us what you need, at any time anywhere. With the agent network spread across the country, we assure the comfort of banking at your door.  </p>
              <h6 class="sub-title">What we Did</h6>
              <ul class="list-marker">
                      <li>User Interface design</li>
                      <li>Front end development</li>
                      <li>CMS</li>
                    </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
         
        <section class="section" style="overflow: hidden;">
         
          <div class="navbar-uk-mobilehid">
            
            <picture>
              <source srcset="../../webp/portfolio/mobile_projects/dsb.webp" type="image/webp">
              <img
              width="100%"
              src="../../images/portfolio/mobile_projects/dsb.png"
              alt="DSB Screenshot"
            />
            </picture>
          </div>
        <div class="testimonials-uk-mobilevisble">
           
            <picture>
              <source srcset="../../webp/portfolio/dsb/screen.webp" type="image/webp">
              <img
              width="100%"
              src="../../images/portfolio/dsb/screen.jpg"
              alt="DSB Screenshot"
            />
            </picture>
          </div>
          <br>
          <br>
			<div class="container">
     
      <div class="chessbox__description container">
								<div class="section-heading">
									 
									<h4 class="title" style='text-align:center;'>Impact of Design</h4>
								</div>
                <div class="text-center text-md " style='text-align:center;color:black'>
                Doorstep banking digital application is expanding the horizon of services and extending the existing relationship with customers & banks for Atyati.  </div>
            <br>
            <br>
            <div class="page-nav-img">
              <a href="../kiba" class="page-nav-img__btn btn-prev">
                <span class="wrapper-img"></span>
                <i class="btn__icon">
                  <svg><use xlink:href="#arrow_left"></use></svg>
                </i>
                <span class="btn__text">PREV</span>
              </a>
              <a href="../" class="">
                <div>
                  <img
                  class='projectsHome-btn'
                    src="../../images/portfolio/projects_home.png"
                    alt="home"
                  />
                </div>
              </a>
              <a href="../frego" class="page-nav-img__btn btn-next">
                <span class="wrapper-img"></span>
                <span class="btn__text">NEXT</span>
                <i class="btn__icon">
                  <svg><use xlink:href="#arrow_right"></use></svg>
                </i>
              </a>
            </div>
              </div>
              
    </section>
     
        <?php require '../../elements/footerinnersub.php'; ?>
	</div>
</main>
<?php require '../../elements/svgcodeinnersub.php'; ?>
  </body>
</html>
