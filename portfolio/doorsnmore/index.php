<!DOCTYPE html>
<html lang="en">

<head>
    <title>Doors & More | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content="Doors & More">
    <meta name="description"
        content="Check out our latest work for Doors & More. The website was designed to keep an international audience in mind." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#80B1DC'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>Doors &
                                    More</h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Product Catalogue Website

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/Doors.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/Doors.jpg"
                        alt="Doors & More Web Digital presence" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/Doors/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/Doors/main.jpg"
                        alt="Doors & More Web Digital presence" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#80B1DC'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>AVT Group</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Retail</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Doors & More, headquartered at Coimbatore, INDIA manufactures wide range of
                                        exquisitely designed “DOORS AND ALLIED WOOD PRODUCTS”. The company needed a
                                        strong web presence to reach to a wider audience.
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                The innovations in the processing of timber are driven by our vision for an ecologically
                                sustainable model for the production of wood products.</div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/portfolio/Doors/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/Doors/view.jpg" alt="Doors & More Screenshot">
                                    </picture>

                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        The website was designed to keep an international audience in mind. The range
                                        and the importance of sustainable practices of the company to build trust and
                                        invite them to inquire. The process involved categorising the product range and
                                        also replicating the physical setup of the factory. The design is inspired by
                                        the history and state of the art woodworking units.
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>User Research
                                        </li>
                                        <li>Card sorting & Information architecture

                                        </li>
                                        <li>Wireframing
                                        </li>
                                        <li>User Interface
                                        </li>
                                        <li>Prototyping</li>
                                        <li>Web Development </li>
                                        <li>CMS</li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" style="overflow: hidden;">
                <div class="">

                    <picture>
                        <source srcset="../../webp/portfolio/Doors/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/Doors/screen.jpg" alt="Doors & More Design" />
                    </picture>
                </div>
                <br>
                <div class="">

                    <picture>
                        <source srcset="../../webp/portfolio/Doors/screen2.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/Doors/screen2.jpg" alt="Doors & More Design" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            Doors & more increased the sales by 150% post launch of new website.

                        </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../../edfina" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../nagra" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>