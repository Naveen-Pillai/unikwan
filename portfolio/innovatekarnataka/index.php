<!DOCTYPE html>
<html lang="en">

<head>
    <title>Innovate Karnataka Project | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content="Innovate Karnataka">
    <meta name="description"
        content="Check out our latest work for Innovate Karnataka. The brand identity was designed with consideration of having a presence on all the platforms." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#002C58'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>Innovate
                                    Karnataka
                                </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Omnichannel Experience
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/InnovateKarnataka.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/InnovateKarnataka.jpg"
                        alt="Innovate Karnataka Logo" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">

                <picture>
                    <source srcset="../../webp/portfolio/innovatek/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/innovatek/main.jpg" alt="Innovate Karnataka Logo" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#002C58'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Gov of Karnataka</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Public Sector</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        The Department of IT BT was having internal communication and identification
                                        problems for external stakeholders globally. For Internal stakeholders -
                                        employees and partners needed to come together for a common purpose and work
                                        toward achieving the same goal. For External stakeholders, the government is
                                        looking to transition from an IT service mindset to an innovation mindset.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                In its endeavour to maintain its pre-eminent position and remain the driving force for
                                India, the Department of IT, BT and S&T have conceptualised “Innovate Karnataka”. </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/portfolio/innovatek/view2.webp" type="image/webp">
                                        <img src="../../images/portfolio/innovatek/view2.jpg"
                                            alt="Innovate Karnataka Screenshot">
                                    </picture>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        The brand Innovate Karnataka has a fresh, sincere yet modern feel to it. The
                                        brand identity was designed with consideration of having presence on all the
                                        platforms and communication channels both offline and online. The brand and its
                                        vision were instrumental in boarding various partners and stakeholders, thereby
                                        making the entire narrative stronger. </p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>User Research & Participatory workshops
                                        </li>
                                        <li>Brand narrative
                                        </li>
                                        <li>Omnichannel Strategy</li>
                                        <li>Brand & Identity
                                        </li>
                                        <li>Digital Presence
                                        </li>
                                        <li>Data Strategy and analytics
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class='portfilio-video-holdr'>
                        <iframe src="https://www.youtube.com/embed/bVmPq_VtD6E" width="1089" height="613"
                            frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                </div>
            </section>


            <section class="section" style="overflow: hidden;">
                <div class="">

                    <picture>
                        <source srcset="../../webp/portfolio/innovatek/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/innovatek/screen.jpg"
                            alt="Innovate Karnataka Video" />
                    </picture>
                </div>
                <section class="section portfolio-margin-body" style="overflow: hidden;">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-10 col-lg-8">
                                <div class="text-center text-md " style='color:black'>
                                    In today’s digitalised world, where most things happen with a tap or swipe, applying
                                    for a job and the recruitment process should be time saving and cost-effective.
                                </div>
                            </div>
                            <div class="chessbox chessbox-top-01">
                                <div class="chessbox__item">
                                    <div class="chessbox__img">
                                        <picture>
                                            <source srcset="../../webp/portfolio/innovatek/view3.webp"
                                                type="image/webp">
                                            <img src="../../images/portfolio/innovatek/view3.jpg"
                                                alt="Innovate Karnataka Corporate Identity">
                                        </picture>

                                    </div>
                                    <div class="chessbox__description">
                                        <div class="section-heading">
                                            <h4 class="title">Defining the Solution</h4>
                                        </div>
                                        <p>
                                            We ran a competitor analysis comparing Gigsasa with other platforms. We
                                            evaluated them against standard parameters.
                                        </p>
                                        <h6 class="sub-title">What we Did</h6>
                                        <ul class="list-marker">
                                            <li>Staghorn sculpin</li>
                                            <li>Plownose chimaera</li>
                                            <li>Sawfish temperate</li>
                                            <li>Perch goatfish</li>
                                            <li>Jackfish</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="">

                    <picture>
                        <source srcset="../../webp/portfolio/innovatek/screen2.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/innovatek/screen2.jpg" alt="K-tech Logo" />
                    </picture>
                </div>
                <section class="section " style="overflow: hidden;">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="chessbox">
                                <div class="chessbox__item" style='display:none'> </div>
                                <div class="chessbox__item">
                                    <div class="chessbox__img">

                                        <picture>
                                            <source srcset="../../webp/portfolio/innovatek/view.webp" type="image/webp">
                                            <img src="../../images/portfolio/innovatek/view.jpg" alt="K-tech Office">
                                        </picture>
                                    </div>
                                    <div class="chessbox__description">
                                        <div class="section-heading">
                                            <h4 class="title">Defining the Solution</h4>
                                        </div>
                                        <p>
                                            In its endeavour to maintain its pre-eminent position and remain the driving
                                            force for India, the Department of IT, BT and S&T have conceptualised
                                            “Innovate Karnataka”.</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <br>
                <br>
                <br>
                <div class="">

                    <picture>
                        <source srcset="../../webp/portfolio/innovatek/screen3.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/innovatek/screen3.jpg" alt="innovatekarnataka" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="">

                    <picture>
                        <source srcset="../../webp/portfolio/innovatek/screen4.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/innovatek/screen4.jpg" alt="innovatekarnataka" />
                    </picture>
                </div>
                <br>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            There is great synergy being created with international partners. With the new brand, the
                            trust was established write from local to international partners who helped them work and
                            get on board with the dept of ITBT to shape the vision of the brand.</div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../dishtv" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../atyati" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>