<!DOCTYPE html>
<html lang="en">

<head>
    <title>Invest Karnataka Project | Unikwan Design Studio Portfolio </title>
    <meta name="keywords" content="Invest Karnataka">
    <meta name="description"
        content="Check out our latest work for Invest Karnataka. Invest Karnataka with its various platforms including the new website attracted Rs. 28K crore investment." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#F67143'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>Invest
                                    Karnataka</h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Customer Digital Experience
                                    Strategy
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/InvestKarnataka.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/InvestKarnataka.jpg"
                        alt="Invest Karnataka Customer digital experience strategy" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">


                <picture>
                    <source srcset="../../webp/portfolio/investk/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/investk/main.jpg"
                        alt="Invest Karnataka Customer digital experience strategy" />
                </picture>
            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#F67143'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Gov of Karnataka, India</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Public Sector</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Invest Karnataka aims at leading partnerships with existing and potential
                                        investors by guiding and mentoring them through the entire gamut of their
                                        investment journey.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                The forum and global investor meet needed a complete restructuring of the offerings and
                                also convey showcase Karnataka as the bright spot in the developmental story of India.
                            </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">

                                    <picture>
                                        <source srcset="../../webp/portfolio/investk/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/investk/view.jpg"
                                            alt="Invest Karnataka Screenshot">
                                    </picture>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        UniKwan with its strong understanding of mapping the gaps in the journeys for
                                        various stakeholders. The idea was to map the entire ecosystem and important
                                        internal and external stakeholders. The single goal and narrative of inviting
                                        investments and supporting existing investors, the website needed right section
                                        to guide investors at every level from ease of doing business to getting
                                        approvals. </p>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>Stakeholders workshop
                                        </li>
                                        <li>Journey mapping
                                        </li>
                                        <li>Service Design
                                        </li>
                                        <li>Information architecture
                                        </li>
                                        <li>Wireframing</li>
                                        <li>Visual Layout
                                        </li>
                                        <li>Prototyping </li>
                                        <li>UX writing
                                        </li>
                                        <li>Web Development
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" style="overflow: hidden;">

                <div class="navbar-uk-mobilehid">

                    <picture>
                        <source srcset="../../webp/portfolio/mobile_projects/investkarnataka2.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/mobile_projects/investkarnataka2.png"
                            alt="Invest Karnataka Design" />
                    </picture>
                </div>
                <div class="testimonials-uk-mobilevisble">

                    <picture>
                        <source srcset="../../webp/portfolio/investk/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/investk/screen.jpg"
                            alt="Invest Karnataka Design" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            Invest Karnataka with its various platform and programs including the new website attracted
                            Rs.
                            28K crore investment in the last 4 months sign website launch.
                            It is expected to generate 45,000 jobs in the state.
                        </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../dialogtv" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../edtech" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>