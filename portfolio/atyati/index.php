<!DOCTYPE html>
<html lang="en">

<head>
    <title>Atyati Project | Unikwan Design Studio Portfolio</title>
    <meta name="keywords" content="Atyati">
    <meta name="description"
        content="Check out our latest work for Atyati. We revamped the website which was outdated to build a digital presence to enhance the brand." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg bg-02" style='background:#FC912BED'>
                        <div class="container">
                            <div class="subpage-header__block">
                                <div class="subpage-header__caption gigsasa_heading_m">Project</div>
                                <h1 class="subpage-header__title" style='font-weight:normal;padding-bottom:4px'>Atyati
                                </h1>
                                <div class="subpage-header__caption gigsasa_heading_max">Web Digital Presence </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <div class="navbar-uk-mobilehid">

                <picture>
                    <source srcset="../../webp/portfolio/thumbnails/Atyati.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/thumbnails/Atyati.jpg" alt="Atyati Design" />
                </picture>
            </div>
            <div class="testimonials-uk-mobilevisble">
                <picture>
                    <source srcset="../../webp/portfolio/atyati/main.webp" type="image/webp">
                    <img width="100%" src="../../images/portfolio/atyati/main.jpg" alt="Atyati Design" />
                </picture>

            </div>
            <!-- start section -->
            <section class="section portfolio-margin-uk" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <div class="box-table-01" style='background:#FC912BED'>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>User Experience</td>
                                                </tr>
                                                <tr>
                                                    <td>Client:</td>
                                                    <td>Atyati Technologies</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry:</td>
                                                    <td>Fintech</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Purpose</h4>
                                    </div>
                                    <p>
                                        Revamp of the website which was outdated and to built a digital presence to
                                        enhance the brand and also reach potential customers and partners.<br><br>
                                        Convey business context and the capability of the organisation, scale of
                                        operations grown significantly enough to be showcased. The old website did not
                                        match the scale and presence of the company in the country.
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="section portfolio-margin-body" style="overflow: hidden;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <div class="text-center text-md " style='color:black'>
                                Convey the organisation value proposition to stakeholders as well as people interested
                                in domain technology. </div>
                        </div>
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item">
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/portfolio/atyati/view.webp" type="image/webp">
                                        <img src="../../images/portfolio/atyati/view.jpg" alt="Atyati Screenshot">
                                    </picture>

                                </div>
                                <div class="chessbox__description">
                                    <div class="section-heading">
                                        <h4 class="title">Defining the Solution</h4>
                                    </div>
                                    <p>
                                        Identifying the most important stakeholders who will benefit and extend the
                                        brand positioning of the organisation as a go to partner for last mile
                                        solutions.
                                    </p>
                                    <br>
                                    <ul class="list-marker">
                                        <li>Convey competence in domain, technology & operations to design & manage
                                            their financial inclusion projects</li>
                                        <li>First level detail of technology platform & operational process</li>
                                        <li>Demonstrate the ability to handle scale and the experience in building the
                                            operations grounds up</li>
                                    </ul>
                                    <br>
                                    <h6 class="sub-title">What we Did</h6>
                                    <ul class="list-marker">
                                        <li>Stakeholder interviews </li>
                                        <li>Design thinking workshops</li>
                                        <li>User Research
                                        </li>
                                        <li>Ethnography
                                        </li>
                                        <li>Data synthesis
                                        </li>
                                        <li>Information architecture
                                        </li>
                                        <li>Wireframing
                                        </li>
                                        <li>Visual Layout
                                        </li>
                                        <li>Development </li>
                                        <li>UX writing
                                        </li>
                                        <li>SEO
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" style="overflow: hidden;">

                <div class="navbar-uk-mobilehid">

                    <picture>
                        <source srcset="../../webp/portfolio/mobile_projects/atyati.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/mobile_projects/atyati.png"
                            alt="Atyati Platform" />
                    </picture>
                </div>
                <div class="testimonials-uk-mobilevisble">

                    <picture>
                        <source srcset="../../webp/portfolio/atyati/screen.webp" type="image/webp">
                        <img width="100%" src="../../images/portfolio/atyati/screen.jpg" alt="Atyati Platform" />
                    </picture>
                </div>
                <br>
                <br>
                <div class="container">

                    <div class="chessbox__description container">
                        <div class="section-heading">

                            <h4 class="title" style='text-align:center;'>Impact of Design</h4>
                        </div>
                        <div class="text-center text-md " style='text-align:center;color:black'>
                            Atyati has grown leaps and bound from 200 people to 800 people company in the last few years
                            post an extensive research and UX revamp of the website.
                        </div>
                        <br>
                        <br>
                        <div class="page-nav-img">
                            <a href="../innovatekarnataka" class="page-nav-img__btn btn-prev">
                                <span class="wrapper-img"></span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_left"></use>
                                    </svg>
                                </i>
                                <span class="btn__text">PREV</span>
                            </a>
                            <a href="../" class="">
                                <div>
                                    <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png"
                                        alt="home" />
                                </div>
                            </a>
                            <a href="../jitfin" class="page-nav-img__btn btn-next">
                                <span class="wrapper-img"></span>
                                <span class="btn__text">NEXT</span>
                                <i class="btn__icon">
                                    <svg>
                                        <use xlink:href="#arrow_right"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>

            </section>

            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>