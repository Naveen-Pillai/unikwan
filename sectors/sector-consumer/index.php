<!DOCTYPE html>
<html lang="en">

<head>
    <title>Consumer technology Sector | Unikwan Design Studio </title>
    <meta name="keywords" content="Consumer technology Sector">
    <meta name="description"
        content="The consumer technology landscape is at the cusp of exciting transformation and expansion due to emerging technologies like 5G. Hit us up for seamless consumer tech design." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <section class="section" style="overflow: hidden;">
                <div class="container">
                    <div class="justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item" style='align-items: flex-start;'>
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/sectors/details/consumer.webp" type="image/webp">
                                        <img src='../../webp/sectors/details/consumer.webp' alt="consumer">
                                    </picture>

                                </div>
                                <div class="chessbox__description chessbox__itemsectors"
                                    style='padding-right:15px;margin-left:-15px'>
                                    <div class="section-heading">
                                        <h4 class="title">Consumer Technology</h4>
                                    </div>
                                    <p style='font-weight:bold;color:#1e1e2d'>About</p>
                                    <p style=''>
                                        Consumer technology is any technology that is directly purchased and used by
                                        individuals to meet their needs. The consumer technology landscape is at the
                                        cusp of exciting transformation and expansion due to emerging technologies like
                                        5G and the Internet of Things, which will become prevalent in the future.
                                        Changing market norms and behaviours affecting our daily lives as consumer
                                        experience is evolving could be anything from delivering pizza to packages.


                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Purpose</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            Consumer Technology companies contend with twin challenges – reaching out to new-age
                            consumers with hyper-personalized services across a multitude of devices and use cases as
                            well as meeting competitive price points. Hyper-personalization leverages to deliver more
                            relevant content, product, services information according to their needs. It is possible to
                            gather real-time behavioural data of consumers and keep track of their preferences. The
                            purpose is to come up with the proper channels and the right messages in real-time. We need
                            to make things that can benefit people as it is an element of the company's responsibility.
                        </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Solution</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            To effectively tackle specific challenges in Consumer Technology, it becomes important for
                            us to become good value providers by transforming products and services with cutting-edge
                            technologies to deliver exceptional customer experience, accelerate time-to-market and
                            ensure business success. Today's consumers are motivated by the value and feasibility of use
                            based on more than just price. The changing scenarios in the world of innovation are getting
                            integrated into personality and human behaviour. The solution should evolve around
                            developing a product and application from how it will be understood and used by any human
                            user. We need to work side by side with design and technology to keep users at the centre of
                            any product/services application design.
                        </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Emerging Technologies</h4>
                        </div>
                        <ul class="list-marker">
                            <li>Intelligent Spaces And Smart Places</li>
                            <li>Blockchains</li>
                            <li>The Internet Of Things</li>
                            <li>Artificial Intelligence (AI) And Machine Learning</li>
                            <li>Big Data And Augmented Analytics</li>
                            <li>Voice Interfaces And Chatbots</li>
                            <li>5G Technology</li>
                            <li>Digital Platforms</li>
                            <li>Computer Vision And Facial Recognition</li>

                        </ul>
                    </div>
                </div>
            </div>

            <!-- start section -->


            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Case Study</h4>
                        </div>

                    </div>
                </div>
                <div class="list-videoblock">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="../../portfolio/kiba" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_kiba2">

                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/Kiba.webp" type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/Kiba.jpg" alt="kiba">
                                    </picture>
                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Kiba</h4>
                                        <div class="promobox03__show">
                                            <p>Mobile Application</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="../../portfolio/frego" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_frego">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/frego.webp" type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/frego.jpg" alt="frego">
                                    </picture>

                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Frego</h4>
                                        <div class="promobox03__show">
                                            <p> Mobile Application</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="chessbox__description container">
                <br>
                <br>
                <div class="page-nav-img">
                    <a href="../sector-retail" class="page-nav-img__btn btn-prev">
                        <span class="wrapper-img"></span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_left"></use>
                            </svg>
                        </i>
                        <span class="btn__text">PREV</span>
                    </a>
                    <a href="../" class="">
                        <div>
                            <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png" alt="home" />
                        </div>
                    </a>
                    <a href="../sector-education" class="page-nav-img__btn btn-next">
                        <span class="wrapper-img"></span>
                        <span class="btn__text">NEXT</span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_right"></use>
                            </svg>
                        </i>
                    </a>
                </div>
            </div>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>