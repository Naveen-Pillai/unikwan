<!DOCTYPE html>
<html lang="en">

<head>
    <title>Healthcare Sector | Unikwan Design Studio </title>
    <meta name="keywords" content="Healthcare Sector">
    <meta name="description"
        content="UX in healthcare has gained increasing attention in recent years. The Healthcare sector has to be innovative and focus on a human-centric approach." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <section class="section" style="overflow: hidden;">
                <div class="container">
                    <div class="justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item" style='align-items: flex-start;'>
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/sectors/details/health.webp" type="image/webp">
                                        <img src='../../webp/sectors/details/health.webp' alt="health">
                                    </picture>

                                </div>
                                <div class="chessbox__description chessbox__itemsectors"
                                    style='padding-right:15px;margin-left:-15px'>
                                    <div class="section-heading">
                                        <h4 class="title">Healthcare Sector</h4>
                                    </div>
                                    <p style='font-weight:bold;color:#1e1e2d'>About</p>
                                    <p style=' '>
                                        Healthcare is a sector where poor usability isn’t just a slight annoyance; it
                                        can lead to life-threatening medical errors and outcomes. It is always better to
                                        design interfaces that prevent mistakes from happening in the first place, like
                                        death due to medical errors and poor design usability reported in the United
                                        States and other countries. Due to this, UX in healthcare has gained increasing
                                        attention in recent years.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Purpose</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            Healthcare management has changed since the COVID-19 and concentrates on convenient services
                            such as telemedicine, mobile phones and applications, wearable devices, robotics, virtual
                            reality space, and Conversational Artificial Intelligence. To maximize the patient and
                            clinical experience, the Healthcare sector has to be innovative and focus on a human-centric
                            approach. A bad user experience in the healthcare sector starts from hospital administration
                            to health care processes and much more. While designing healthcare management systems, it is
                            not easy to forget its vital stakeholder in the ecosystem comprising Patients, Providers,
                            Administrative Staff, and Policymakers.

                        </p>
                        <p style='text-align:left;'>The key stakeholders play a crucial part in the navigation of
                            healthcare management systems and designing and implementing the care management programs.
                            Due to the COVID-19 Pandemic, we must consider a whole new approach to the healthcare
                            delivery system. We need to design seamless facilities and always test solutions as bad user
                            experience can impact the daily delivery system. The focus has been shifted to design for
                            the patient’s safety. </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Solution</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            The solution to this problem is adherence to design, which needs to be simple, intuitive,
                            and not distracting. Most effective practices to reduce cognitive load on healthcare design
                            systems include ideation validation and thorough testing of the design concepts. We can
                            reduce distractions by avoiding irrelevant data, irrelevant tabs, and bold designs. We can
                            develop a minimalistic and interactive user experience design by making the right
                            information available at the right time, as it will be more patient-centric. So, it will be
                            easy for highly experienced Doctors, nurses, and administration staff to process information
                            in the right way. </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Emerging Technologies</h4>
                        </div>
                        <ul class="list-marker">
                            <li>3-D Printing </li>
                            <li>Virtual Reality</li>
                            <li>Augmented Reality</li>
                            <li>Artificial Intelligence</li>
                            <li>Internet of Things</li>
                            <li>Drones</li>
                            <li>Robots</li>
                            <li>Blockchain Technology</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- start section -->

            <div class="chessbox__description container">
                <br>
                <br>
                <div class="page-nav-img">
                    <a href="../sector-automobile" class="page-nav-img__btn btn-prev">
                        <span class="wrapper-img"></span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_left"></use>
                            </svg>
                        </i>
                        <span class="btn__text">PREV</span>
                    </a>
                    <a href="../" class="">
                        <div>
                            <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png" alt="home" />
                        </div>
                    </a>
                    <a href="../sector-media" class="page-nav-img__btn btn-next">
                        <span class="wrapper-img"></span>
                        <span class="btn__text">NEXT</span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_right"></use>
                            </svg>
                        </i>
                    </a>
                </div>
            </div>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>