<!DOCTYPE html>
<html lang="en">

<head>
    <title>Finance Sector | Unikwan Design Studio </title>
    <meta name="keywords" content="Finance Sector">
    <meta name="description"
        content="Customer demand for financial services is increasing, but they ask for services that are very different from what was offered in the past. Contact us for fintech design needs." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <section class="section" style="overflow: hidden;">
                <div class="container">
                    <div class="justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item" style='align-items: flex-start;'>
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/sectors/details/finance.webp" type="image/webp">
                                        <img src='../../webp/sectors/details/finance.webp' alt="finance">
                                    </picture>

                                </div>
                                <div class="chessbox__description chessbox__itemsectors"
                                    style='padding-right:15px;margin-left:-15px'>
                                    <div class="section-heading">
                                        <h4 class="title">Financial Sector</h4>
                                    </div>
                                    <p style='font-weight:bold;color:#1e1e2d'>About</p>
                                    <p style=' '>
                                        Customer demand for financial services is increasing, but they ask for services
                                        that are very different from what was offered in the past, which means the
                                        design of financial services, products, and customer experiences has to stand
                                        out differently. It is crucial to be at the forefront of the providers’ efforts
                                        to remain relevant in a rapidly changing landscape. With the deployment of
                                        technological trends, Millennials and Gen-Z are reshaping the human-centric
                                        experience of services. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Purpose</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            Consultancy to strategize the requirements has become extremely important to work on design
                            space, which will define exciting technological advances and trends in the coming decade. To
                            provide positive experiences to keep our users engaged and loyal to financial platforms and
                            brands, we must design a meaningful customer journey by examining and thorough research of
                            forces and emerging customers’ attitudes. The adoption of disruptive technologies studies
                            the forces that alter the role, structure, and competitive environment of financial
                            institutions and the markets and companies they operate.</p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Solution</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            Millennials and Gen-Z are now opening up to fresh approaches such as Artificial
                            Intelligence, the Internet of Things, big data, intelligent spaces, and much more means that
                            today’s competitive fintech products and services are innovating heavily around the user
                            experience associated with conducting the same financial transactions their parents and
                            grandparents did during retail banking and brick-and-mortar services. The changes will be
                            driven by different preferences revolving around different technical approaches with the
                            adoption of emerging technologies. </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Emerging Technologies</h4>
                        </div>
                        <ul class="list-marker">
                            <li>Artificial Intelligence</li>
                            <li>Robotics</li>
                            <li>Blockchain Technology</li>
                            <li>Conversational AI</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- start section -->


            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Case Study</h4>
                        </div>

                    </div>
                </div>
                <div class="list-videoblock">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="../../portfolio/dsb" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_dsb">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/DSB.webp" type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/DSB.jpg" alt="dsb">
                                    </picture>

                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">DSB</h4>
                                        <div class="promobox03__show">
                                            <p>Website</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="../../portfolio/atyati" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_atyati">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/Atyati.webp" type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/Atyati.jpg" alt="atyati">
                                    </picture>

                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Atyati</h4>
                                        <div class="promobox03__show">
                                            <p>TV Application</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="../../portfolio/jitfin" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_jitfin">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/jitfin.webp" type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/jitfin.jpg" alt="jitfin">
                                    </picture>

                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Jitfin</h4>
                                        <div class="promobox03__show">
                                            <p>Website</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chessbox__description container">
                <br>
                <br>
                <div class="page-nav-img">
                    <a href="../sector-public" class="page-nav-img__btn btn-prev">
                        <span class="wrapper-img"></span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_left"></use>
                            </svg>
                        </i>
                        <span class="btn__text">PREV</span>
                    </a>
                    <a href="../" class="">
                        <div>
                            <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png" alt="home" />
                        </div>
                    </a>
                    <a href="../sector-automobile" class="page-nav-img__btn btn-next">
                        <span class="wrapper-img"></span>
                        <span class="btn__text">NEXT</span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_right"></use>
                            </svg>
                        </i>
                    </a>
                </div>
            </div>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>