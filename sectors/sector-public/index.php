<!DOCTYPE html>
<html lang="en">

<head>
    <title>Public Sector | Unikwan Design Studio </title>
    <meta name="keywords" content="Public Sector">
    <meta name="description"
        content="Public sector is usually behind on tech trends. There’s a need for the government to embrace Research and Consulting to deliver better business results." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <section class="section" style="overflow: hidden;">
                <div class="container">
                    <div class="justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item" style='align-items: flex-start;'>
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/sectors/details/public.webp" type="image/webp">
                                        <img src='../../webp/sectors/details/public.webp' alt="public">
                                    </picture>

                                </div>
                                <div class="chessbox__description chessbox__itemsectors"
                                    style='padding-right:15px;margin-left:-15px'>
                                    <div class="section-heading">
                                        <h4 class="title">Public Sector </h4>
                                    </div>
                                    <p style='font-weight:bold;color:#1e1e2d'>About</p>
                                    <p style=' '>
                                        We always expect developments across various departments from Government sectors
                                        by focusing on several policies and innovatively performing day-to-day tasks. It
                                        stresses the role of research in public sector decision making, the part of
                                        academia, the relationship between economic imperatives and scientific
                                        information, and the management of uncertainty and change. Innovation has given
                                        rise to the expectations of people from government sectors, and it impacts our
                                        society.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Purpose</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            With changing times, this has posed a challenge for public sector entities to embrace
                            innovative tools in such a short period. The problem is that the public sector is usually
                            behind on tech trends. There’s a significant gap between designing policies meeting
                            customers’ expectations and innovation as it can’t be defined as a one-time project. It is a
                            learned process that requires a shift in thinking and a disciplined approach. </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Solution</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            In the new era of technological advancement, there’s a need for the government to embrace
                            two disciplines: Research and Consulting for improving, adapting, designing, or developing a
                            product, system, or service to deliver better business results and create value for others.
                            New approaches and policies can be defined after thorough research to provide quality
                            services and better response to society’s needs. </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Emerging Technologies</h4>
                        </div>
                        <ul class="list-marker">
                            <li>Internet of Things</li>
                            <li>Machine Learning</li>
                            <li>Video Analytics</li>
                            <li>Biometrics/Identity Analytics </li>
                            <li>Big Data</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- start section -->


            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Case Study</h4>
                        </div>

                    </div>
                </div>
                <div class="list-videoblock">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="../../portfolio/investkarnataka" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_kiba2">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/InvestKarnataka.webp"
                                            type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/InvestKarnataka.jpg"
                                            alt="investkarnataka">
                                    </picture>

                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Invest Karnataka</h4>
                                        <div class="promobox03__show">
                                            <p>Mobile Application</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="../../portfolio/innovatekarnataka" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_edfina">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/InnovateKarnataka.webp"
                                            type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/InnovateKarnataka.jpg"
                                            alt="innovatekarantaka">
                                    </picture>
                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Innovate Karnataka</h4>
                                        <div class="promobox03__show">
                                            <p> Website</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chessbox__description container">
                <br>
                <br>
                <div class="page-nav-img">
                    <a href="../sector-education" class="page-nav-img__btn btn-prev">
                        <span class="wrapper-img"></span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_left"></use>
                            </svg>
                        </i>
                        <span class="btn__text">PREV</span>
                    </a>
                    <a href="../" class="">
                        <div>
                            <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png" alt="home" />
                        </div>
                    </a>
                    <a href="../sector-finance" class="page-nav-img__btn btn-next">
                        <span class="wrapper-img"></span>
                        <span class="btn__text">NEXT</span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_right"></use>
                            </svg>
                        </i>
                    </a>
                </div>
            </div>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>