<!DOCTYPE html>
<html lang="en">

<head>
    <title>Hospitality & Tourism Sector | Unikwan Design Studio </title>
    <meta name="keywords" content="Hospitality & Tourism Sector">
    <meta name="description"
        content="The hospitality sector needs clever design, as it provides users the information they need at the right time and moment. Contact us for Tourism UX." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <section class="section" style="overflow: hidden;">
                <div class="container">
                    <div class="justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item" style='align-items: flex-start;'>
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/sectors/details/hospitality.webp" type="image/webp">
                                        <img src='../../webp/sectors/details/hospitality.webp' alt="hospitality">
                                    </picture>

                                </div>
                                <div class="chessbox__description chessbox__itemsectors"
                                    style='padding-right:15px;margin-left:-15px'>
                                    <div class="section-heading">
                                        <h4 class="title">Hospitality & tourism</h4>
                                    </div>
                                    <p style='font-weight:bold;color:#1e1e2d'>About</p>
                                    <p style=''>
                                        In the hospitality sector, the world of comfort has excellent potential to
                                        improve the user experience design and depends on how customers engage with
                                        services. Millennials and Gen-Z stand out for the use of technology and look for
                                        solutions that are easily accessible to them. Pandemic has modified the sector
                                        as virtual tourism has attracted travellers all around. It can take different
                                        forms and comes in varying degrees of technological capability. These shifts are
                                        taking place to enhance the experience of travellers.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Purpose</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            The hospitality sector needs clever design, as it provides users with the information they
                            need at the right time and moment. The features should be designed after a thorough
                            understanding of the users' requirements which might include: Mobile Check-in, Quick access
                            to booking locations around you, Navigation to their booking, viewing their booking,
                            Extending the stay, and discovering about their location by navigating nearby places. </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Solution</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            UX in Tourism can be more personal and emotional because there are specific questions we'll
                            come across before designing the customer experience. Like how do we want them to feel about
                            the destination? What do we want them to think and express? What information will they get
                            and understand? Is the impact positive or negative? These play a very crucial role because
                            travel is about a human-centric approach to co-create an innovative experience. The main aim
                            should be to think carefully about what kind of tools need to be provided to make the users'
                            life easier and how the design would be better if related to users' context and solutions.
                            We work across customer touchpoints and initiate the design research.</p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Emerging Technologies</h4>
                        </div>
                        <ul class="list-marker">
                            <li>Internet of Things</li>
                            <li>Artificial Intelligence</li>
                            <li>Machine Learning</li>
                            <li>Virtual Reality</li>
                            <li>Augmented Reality</li>
                            <li>Big Data</li>
                            <li>Automated check-in and check out</li>
                            <li>Conversational AI</li>
                            <li>Smart Concierge And Mobility Solutions</li>
                            <li>Recognition Technology</li>
                            <li>Near-field Communication (NFC) Technology</li>

                        </ul>
                    </div>
                </div>
            </div>

            <!-- start section -->


            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Case Study</h4>
                        </div>

                    </div>
                </div>
                <div class="list-videoblock">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="../../portfolio/gigsasa" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_gigs">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/Gigsasa.webp" type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/Gigsasa.jpg" alt="gigsasa">
                                    </picture>

                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Gigsasa</h4>
                                        <div class="promobox03__show">
                                            <p>Mobile Application</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chessbox__description container">
                <br>
                <br>
                <div class="page-nav-img">
                    <a href="../sector-media" class="page-nav-img__btn btn-prev">
                        <span class="wrapper-img"></span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_left"></use>
                            </svg>
                        </i>
                        <span class="btn__text">PREV</span>
                    </a>
                    <a href="../" class="">
                        <div>
                            <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png" alt="home" />
                        </div>
                    </a>
                    <a href="../sector-retail" class="page-nav-img__btn btn-next">
                        <span class="wrapper-img"></span>
                        <span class="btn__text">NEXT</span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_right"></use>
                            </svg>
                        </i>
                    </a>
                </div>
            </div>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>