<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sectors | Unikwan Design Studio
    </title>
    <meta name="keywords" content="Sectors">
    <meta name="description"
        content="Over the last decade, we’ve worked across many different sectors and businesses to create a powerful combination of industry-leading designs." />

    <?php require '../elements/headerinner.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../elements/navbarinner.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding">
                <div class="">
                    <div class="subpage-header__bg">
                        <div class="container">
                            <div class="subpage-header__block">
                                <h1 class="subpage-header__title">Sectors</h1>
                                <div class="subpage-header__line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section section-default-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xl-4">
                            <div class="section-heading">
                                <h4 class="title">
                                    Overcoming The <br>Design Gaps<br> In Different Sectors
                                </h4>

                            </div>
                        </div>
                        <div class="col-md-8 col-xl-8">
                            <strong class="base-color-02" style='font-size:20px'>Overcoming The Design Gaps In Different
                                Sectors
                                We create a meaningful relationship between design and branding.

                            </strong>
                            <p> UniKwan helps Startups and Businesses across a diverse range of sectors. With a diverse
                                set of industry experiences, we bring along different approaches and a broad
                                understanding of user experience to create a holistic customer experience.
                            </p>

                        </div>
                    </div>
                </div>
            </section>
            <!-- end section --><br><br>
            <div class='container'>

                <div class="sectors_holder">
                    <div class='single_sector_box'>
                        <img width='100%' alt='education sector' src='../webp/sectors/tiles/education.webp' />
                        <a href='sector-education'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'>Education</article>
                            </div>
                        </a>
                    </div>
                    <div class='single_sector_box'>
                        <img width='100%' alt='public sector' src='../webp/sectors/tiles/public.webp' />
                        <a href='sector-public'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'>Public Sector </article>
                            </div>
                        </a>
                    </div>
                    <div class='single_sector_box'>
                        <img width='100%' alt='finance sector' src='../webp/sectors/tiles/finance.webp' />
                        <a href='sector-finance'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'>Financial Sector</article>
                            </div>
                        </a>
                    </div>
                    <div class='single_sector_box'>
                        <img width='100%' alt='automobile sector' src='../webp/sectors/tiles/automobile.webp' />
                        <a href='sector-automobile'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'>Automobile Industry</article>
                            </div>
                        </a>
                    </div>
                    <div class='single_sector_box'>
                        <img width='100%' alt='health sector' src='../webp/sectors/tiles/health.webp' />
                        <a href='sector-healthcare'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'>Healthcare</article>
                            </div>
                        </a>
                    </div>
                    <div class='single_sector_box'>
                        <img width='100%' alt='media sector' src='../webp/sectors/tiles/media.webp' />
                        <a href='sector-media'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'> Entertainment </article>
                            </div>
                        </a>
                    </div>
                    <div class='single_sector_box'>
                        <img width='100%' alt='hospitality sector' src='../webp/sectors/tiles/hospitality.webp' />
                        <a href='sector-hospitality'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'>Hospitality </article>
                            </div>
                        </a>
                    </div>
                    <div class='single_sector_box'>
                        <img width='100%' alt='retail sector' src='../webp/sectors/tiles/retail.webp' />
                        <a href='sector-retail'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'> E-commerce</article>
                            </div>
                        </a>
                    </div>
                    <div class='single_sector_box'>
                        <img width='100%' alt='consumer sector' src='../webp/sectors/tiles/consumer.webp' />
                        <a href='sector-consumer'>
                            <div class='sector_texthold'>
                                <article class='sectors_heading'> Consumer Tech</article>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- add is-open class to make open default -->

            <!-- end section -->
            <?php require '../elements/footerinner.php'; ?>
        </div>
    </main>
    <script src="tabs.js"></script>

    <?php require '../elements/svgcodeinner.php'; ?>
</body>

</html>