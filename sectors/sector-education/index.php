<!DOCTYPE html>
<html lang="en">

<head>
    <title>Education Sector | Unikwan Design Studio </title>
    <meta name="keywords" content="Education Sector">
    <meta name="description"
        content="UX design is essential in the education department because technology has affected the modern education system. Contact us for your EdTech design needs." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <section class="section" style="overflow: hidden;">
                <div class="container">
                    <div class="justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item" style='align-items: flex-start;'>
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/sectors/details/education.webp" type="image/webp">
                                        <img src='../../webp/sectors/details/education.webp' alt="education">
                                    </picture>

                                </div>
                                <div class="chessbox__description chessbox__itemsectors"
                                    style='padding-right:15px;margin-left:-15px'>
                                    <div class="section-heading">
                                        <h4 class="title">Education</h4>
                                    </div>
                                    <p style='font-weight:bold;color:#1e1e2d'>About</p>
                                    <p style='text-align:left;'>
                                        Over the past decade, the education industry has been making tremendous changes
                                        from the traditional teaching method to the accessible ways of the digital
                                        learning experience. Since the pandemic, BYJU’S an Indian educational technology
                                        and online tutoring firm, have raised several billions of investments, and over
                                        20 million new students have started learning from its platform. Other learning
                                        platforms such as Unacademy and Toppr also raised funds during the pandemic
                                        period, which leads to the future of education.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Purpose</h4>
                        </div>
                        <p class="" style=' '>
                            The development of User Experience and Interface design has seen a competitive advantage in
                            the education sector, which suits both students and instructors. The deployment of the
                            internet and technology has challenged the conventional structures of education and forced
                            them to keep up with the changing norms of E-learning Design models. Technological
                            advancement has made people more concerned with the learning environment without time
                            constraints. Thus, experience design is essential in the education department because
                            technology has affected the modern education system. This requires a portable design
                            structure to ensure success.</p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Solution</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            Design solutions for the education sector consider the right engageable and active users as
                            a priority as they get involved with solutions around the clock. The involvement of the key
                            stakeholders while developing and designing the user experience will play a crucial role.
                            The stakeholders in the education system involve staff, administrators, parents, students,
                            community members, and school board members. The process begins such as 'why the project is
                            initiated' and ‘what business value does this design bring'. We need to map the
                            psychological factors and problems faced by stakeholders at each step of the user-friendly
                            design process so that the result offers a technologically rich and coherent learning
                            experience.
                        </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Emerging Technologies</h4>
                        </div>
                        <ul class="list-marker">
                            <li>Artificial Intelligence</li>
                            <li>Virtual Reality</li>
                            <li>Augmented Reality</li>
                            <li>Automation </li>
                            <li>Learning Analytics</li>
                            <li>Computational Thinking</li>
                            <li>Usage of 5G Tech</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- start section -->


            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Case Study</h4>
                        </div>

                    </div>
                </div>
                <div class="list-videoblock">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="../../portfolio/edtech" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_colored">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/Edtech.webp" type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/Edtech.jpg" alt="edtech">
                                    </picture>

                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">EdTech</h4>
                                        <div class="promobox03__show">
                                            <p> Website</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="../../portfolio/edfina" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_edfina">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/Edfina.webp" type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/Edfina.jpg" alt="edfina">
                                    </picture>
                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Edfina</h4>
                                        <div class="promobox03__show">
                                            <p> Website</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chessbox__description container">
                <br>
                <br>
                <div class="page-nav-img">
                    <a href="../sector-consumer" class="page-nav-img__btn btn-prev">
                        <span class="wrapper-img"></span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_left"></use>
                            </svg>
                        </i>
                        <span class="btn__text">PREV</span>
                    </a>
                    <a href="../" class="">
                        <div>
                            <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png" alt="home" />
                        </div>
                    </a>
                    <a href="../sector-public" class="page-nav-img__btn btn-next">
                        <span class="wrapper-img"></span>
                        <span class="btn__text">NEXT</span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_right"></use>
                            </svg>
                        </i>
                    </a>
                </div>
            </div>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>