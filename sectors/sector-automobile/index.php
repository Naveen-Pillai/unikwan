<!DOCTYPE html>
<html lang="en">

<head>
    <title>Automobile Sector | Unikwan Design Studio </title>
    <meta name="keywords" content="Automobile Sector">
    <meta name="description"
        content="The design process in the automotive industry is different from that of technology companies. Hit us up for automobile designs needs." />

    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <section class="section" style="overflow: hidden;">
                <div class="container">
                    <div class="justify-content-center">
                        <div class="chessbox chessbox-top-01">
                            <div class="chessbox__item" style='display:none'> </div>
                            <div class="chessbox__item" style='align-items: flex-start;'>
                                <div class="chessbox__img">
                                    <picture>
                                        <source srcset="../../webp/sectors/details/mobility.webp" type="image/webp">
                                        <img src='../../webp/sectors/details/mobility.webp' alt="mobility">
                                    </picture>

                                </div>
                                <div class="chessbox__description chessbox__itemsectors"
                                    style='padding-right:15px;margin-left:-15px'>
                                    <div class="section-heading">
                                        <h4 class="title">Automobile Industry</h4>
                                    </div>
                                    <p style='font-weight:bold;color:#1e1e2d'>About</p>
                                    <p style=''>
                                        Due to the disruption of technology and digitization, the interaction with
                                        products and services in the market is transformed in a very different manner.
                                        These advances also affected the Automobile Industry, how we’re interacting with
                                        cars from the first radio to voice assistants. The integration of digital
                                        technology has been leading our daily lives. Now, functionalities are more
                                        integrated into one device with the introduction of mobile internet.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Purpose</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            The design process in the automotive industry is different from that of technology
                            companies. The software industry is changing rapidly, with rapid iterations and updates.
                            These are often technology-driven ideas rather than user-driven, as it is challenging to
                            predict future user needs. With the evolution of car technology, comfort played a vital role
                            and invented digital scenes in the car’s digital space and dashboard. Integrated
                            functionality now includes media, connectivity, and navigation to provide a seamless and
                            portable experience. </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Solution</h4>
                        </div>
                        <p class="" style='text-align:left;'>
                            The solutions have to be user-centric instead of technology-pushed ideas, and future
                            predictions need to be done through detailed design consulting. The software in cars has to
                            work as a key differentiator as it is becoming the more significant part of car experience
                            in contextual terms. When we talk about solutions in a car, it has to be limited in space to
                            provide the best customer and human-centric expertise; thus, interactions need to be easy to
                            understand. </p>
                    </div>
                </div>

                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Emerging Technologies</h4>
                        </div>
                        <ul class="list-marker">
                            <li>Internet of Vehicles</li>
                            <li>Vehicle-to-Vehicle (V2V) & Vehicle-to-Infrastructure (V2I)</li>
                            <li>Smart Sensors</li>
                            <li>Autonomous Driving</li>
                            <li>Electrification</li>
                            <li>Blockchain Technology</li>
                            <li>Augmented Reality</li>
                            <li>Artificial Intelligence</li>
                            <li>Big Data</li>
                            <li>Conversational AI</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- start section -->


            <div class='container'>
                <div class="chessbox__item" style='    margin-top: 20px;'>
                    <div class="chessbox__description container" style='padding:0'>
                        <div class="section-heading">
                            <h4 class="title" style='text-align:left;'>Case Study</h4>
                        </div>

                    </div>
                </div>
                <div class="list-videoblock">
                    <div class="row">

                        <div class="col-sm-4">
                            <a href="../../portfolio/lightmetrics" class=" videoblock promobox03 block-once">
                                <div class="promobox03__img promobox_lightm">
                                    <picture>
                                        <source srcset="../../webp/portfolio/thumbnails/LightMetrics.webp"
                                            type="image/webp">
                                        <img src="../../images/portfolio/thumbnails/LightMetrics.jpg"
                                            alt="lightmetrics">
                                    </picture>
                                </div>
                                <div class="promobox03__description">
                                    <div class="promobox03__layout" style='background:transparent'>
                                        <h4 class="promobox03__title">Light Metrics</h4>
                                        <div class="promobox03__show">
                                            <p>Mobile Application</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chessbox__description container">
                <br>
                <br>
                <div class="page-nav-img">
                    <a href="../sector-finance" class="page-nav-img__btn btn-prev">
                        <span class="wrapper-img"></span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_left"></use>
                            </svg>
                        </i>
                        <span class="btn__text">PREV</span>
                    </a>
                    <a href="../" class="">
                        <div>
                            <img class='projectsHome-btn' src="../../images/portfolio/projects_home.png" alt="home" />
                        </div>
                    </a>
                    <a href="../sector-healthcare" class="page-nav-img__btn btn-next">
                        <span class="wrapper-img"></span>
                        <span class="btn__text">NEXT</span>
                        <i class="btn__icon">
                            <svg>
                                <use xlink:href="#arrow_right"></use>
                            </svg>
                        </i>
                    </a>
                </div>
            </div>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>