<?php

function build_emailbody_with_link($firstName,$lastName,$email,$subject, $plink, $gotopage='index.html') 
{ 
 $emailBody="<html><head><style>.main{font-family:arial,verdana;color:#000000;font-size:12px;}</style></head><body>";	
	$emailBody .="<table cellpadding='3' cellspacing=0 border='0'>";
	
	$emailBody .="<tr align=left>";
	$emailBody .="<td class='main'>".$firstName." ".$lastName.",</td>";
	$emailBody .="</tr>";
	$emailBody .="<tr align=left>";
	$emailBody .="<td class='main'>Here is the requested <a href='".$plink."'>link<a></td>";
	$emailBody .="</tr>";
	
	$emailBody .="</table>";
	$emailBody .="<br/>";
	$emailBody .="<div class='main'>Thanks</div>";
	$emailBody .="<div class='main'>info@openstream.ai</div>";
	
	$emailBody .="<hr/>";
	$emailBody .="<div class='main' style='font-size:0.5em;'>NOTICE TO RECIPIENT: THIS E-MAIL IS MEANT FOR ONLY THE INTENDED RECIPIENT OF THE TRANSMISSION, AND MAY BE A COMMUNICATION PRIVILEGED BY LAW. IF YOU RECEIVED THIS E-MAIL IN ERROR, ANY REVIEW, USE, DISSEMINATION, DISTRIBUTION, OR COPYING OF THIS E-MAIL IS STRICTLY PROHIBITED. PLEASE NOTIFY US IMMEDIATELY OF THE ERROR BY RETURN E-MAIL AND PLEASE DELETE THIS MESSAGE FROM YOUR SYSTEM. THANK YOU IN ADVANCE FOR YOUR COOPERATION. Reply to : <a href='mailto:legal@openstream.com'>legal@openstream.com</a></div>";
	
	$emailBody .="</html>";
 
 error_log($emailBody);
 error_log($email);
 $fromemail = "info@openstream.ai";
 
 if (mail($email, $subject,$emailBody, 
		"From:  <".$fromemail.">\n" .  
	    "MIME-Version: 1.0\n" . 
	    "Content-type: text/html; charset=iso-8859-1"))	
  {
	
	echo("<script>alert('An e-mail with access details will be sent.');window.location='$gotopage';</script>");
  } else {
	echo("<script>alert('Mail delivery failed');window.location='$gotopage';</script>");
  }	
}
function build_gartner_report_emailbody_with_link($firstName,$lastName,$email,$subject, $plink, $gotopage='index.html') 
{ 
 $emailBody="<html><head><style>.main{font-family:arial,verdana;color:#000000;font-size:12px;}</style></head><body>";	
 $emailBody .="<div class='main'>".$firstName." ".$lastName.",</div><br/>";
	
	$emailBody .="<div class='main'>As you requested, here is a <a href='".$plink."'>link<a> to the - ".$subject." </div>";
	
	$emailBody .="<div class='main'>";
	$emailBody .="This report will assist you in assessing how and where new, innovative approaches in the field of natural language technologies can best be applied.";
	$emailBody .="</div>";
	$emailBody .="<br/>";
	
	$emailBody .="<div class='main'>Openstream helps customers with their digital transformation, through our Context-aware Virtual Assistant technology. To learn more, <a href='https://www.openstream.ai/request-demo.html'>request a demo</a> today.</div>";
	$emailBody .="<div class='main'>Cheers,</div>";
    $emailBody .="<div class='main'>Alexandre Heupel</div>";
    $emailBody .="<div class='main'>Vice President (Marketing)</div>";

	$emailBody .="<div class='main'></div><br/>";
	$emailBody .="<div class='main'>info@openstream.ai</div>";
	
	$emailBody .="<hr/>";
	$emailBody .="<div class='main' style='font-size:0.5em;'>NOTICE TO RECIPIENT: THIS E-MAIL IS MEANT FOR ONLY THE INTENDED RECIPIENT OF THE TRANSMISSION, AND MAY BE A COMMUNICATION PRIVILEGED BY LAW. IF YOU RECEIVED THIS E-MAIL IN ERROR, ANY REVIEW, USE, DISSEMINATION, DISTRIBUTION, OR COPYING OF THIS E-MAIL IS STRICTLY PROHIBITED. PLEASE NOTIFY US IMMEDIATELY OF THE ERROR BY RETURN E-MAIL AND PLEASE DELETE THIS MESSAGE FROM YOUR SYSTEM. THANK YOU IN ADVANCE FOR YOUR COOPERATION. Reply to : <a href='mailto:legal@openstream.com'>legal@openstream.com</a></div>";
	
	$emailBody .="</html>";
 
 error_log($emailBody);
 error_log($email);
 $fromemail = "info@openstream.ai";
 
 if (mail($email, $subject,$emailBody, 
		"From:  <".$fromemail.">\n" .  
	    "MIME-Version: 1.0\n" . 
	    "Content-type: text/html; charset=iso-8859-1"))	
  {
	
	echo("<script>alert('An e-mail with access details will be sent.');window.location='$gotopage';</script>");
  } else {
	echo("<script>alert('Mail delivery failed');window.location='$gotopage';</script>");
  }	
}

function build_webinar_emailbody_with_link($firstName,$lastName,$email,$subject, $plink, $gotopage='index.html') 
{ 
 $emailBody="<html><head><style>.main{font-family:arial,verdana;color:#000000;font-size:12px;}</style></head><body>";	
 $emailBody .="<div class='main'>".$firstName." ".$lastName.",</div><br/>";
	
	$emailBody .="<div class='main'>As you requested, the link to watch Gartner Webinar featuring Gartner's Sr.Director Analyst Mr.Anthony Mullen, Openstream's Chief Scientist Dr.Phil Cohen and CEO Mr.Raj Tumuluri is <a href='".$plink."'>here</a> </div>";
	
	$emailBody .="<br/>";
	$emailBody .="<div class='main'>Openstream helps customers with their digital transformation, through our Context-aware Virtual Assistant technology. To learn more, <a href='https://www.openstream.ai/request-demo.html'>request a demo</a> today.</div>";
$emailBody .="<div class='main'>Cheers,</div>";
$emailBody .="<div class='main'>Alexandre Heupel</div>";
$emailBody .="<div class='main'>Vice President (Marketing)</div>";
	$emailBody .="<div class='main'></div><br/>";
	$emailBody .="<div class='main'>info@openstream.ai</div>";
	
	$emailBody .="<hr/>";
	$emailBody .="<div class='main' style='font-size:0.5em;'>NOTICE TO RECIPIENT: THIS E-MAIL IS MEANT FOR ONLY THE INTENDED RECIPIENT OF THE TRANSMISSION, AND MAY BE A COMMUNICATION PRIVILEGED BY LAW. IF YOU RECEIVED THIS E-MAIL IN ERROR, ANY REVIEW, USE, DISSEMINATION, DISTRIBUTION, OR COPYING OF THIS E-MAIL IS STRICTLY PROHIBITED. PLEASE NOTIFY US IMMEDIATELY OF THE ERROR BY RETURN E-MAIL AND PLEASE DELETE THIS MESSAGE FROM YOUR SYSTEM. THANK YOU IN ADVANCE FOR YOUR COOPERATION. Reply to : <a href='mailto:legal@openstream.com'>legal@openstream.com</a></div>";
	
	$emailBody .="</html>";
 
 error_log($emailBody);
 error_log($email);
 $fromemail = "info@openstream.ai";
 
 if (mail($email, $subject,$emailBody, 
		"From:  <".$fromemail.">\n" .  
	    "MIME-Version: 1.0\n" . 
	    "Content-type: text/html; charset=iso-8859-1"))	
  {
	
	echo("<script>alert('An e-mail with access details will be sent.');window.location='$gotopage';</script>");
  } else {
	echo("<script>alert('Mail delivery failed');window.location='$gotopage';</script>");
  }	
}
?>