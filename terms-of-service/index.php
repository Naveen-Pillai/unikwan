<!DOCTYPE html>
<html lang="en">
  <head>
  <title>UniKwan | UI UX Design Agency in Bangalore, India</title>
  <meta name="description" content="Unikwan is a renowned place for next-level user experience design, customer experience design, innovation, and strategic consulting." />
  
    <?php require '../elements/headerinner.php'; ?>
   </head>
  <body>
    <!-- start Header -->
    <?php require '../elements/navbarinner.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
      <div class="section--bg-wrapper-02">
        <!-- start section -->
        <section class="section--no-padding section">
          <div class="">
            <div
              class="subpage-header__bg slider"
              data-bg="images/subpage-title-img/subpage-title-img01.jpg"
            >
              <div class="container">
                <div class="subpage-header__block">
                  <h1 class="subpage-header__title">Terms of Service</h1>
                  <div class="subpage-header__line"></div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <section class="section-default-top">
          <div class="container">
            <div class="content-container">
              <h3>Terms of Service</h3>
              <h4 class="sub-title">Terms</h4>
              <p>By accessing the website at <a href="../http://unikwan.com">https://unikwan.com</a>, you are agreeing to be bound by these terms of service, all applicable laws, regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trademark law.
              </p>
              
              <h4 class="sub-title">Use License </h4>
              <p>
              <ol class="list-number">
                    <li>Permissions are granted to temporarily download one copy of the materials (information or software) on UniKwan Innovations Pvt. Ltd.’s website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license, you may not:</li>
                    <li>Modify or copy the materials;</li>
                    <li>Use the materials for any commercial purpose, or any public display (commercial or non-commercial);</li>
                    <li>Attempt to decompile or reverse engineer any software contained on UniKwan Innovations Pvt. Ltd.’s website;</li>
                    <li>Remove any copyright or other proprietary notations from the materials; or</li>
                    <li>Transfer the materials to another person or "mirror" the materials on any other server.</li>
                    <li>This license shall automatically terminate if you violate any of these restrictions and may be terminated by UniKwan Innovations Pvt. Ltd at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</li>
                  </ol>
              </p>
              <h4 class="sub-title">Disclaimer</h4>
              <p>
              <ol class="list-number">
                    <li>The materials on UniKwan Innovations Pvt. Ltd.’s website is provided on an 'as is' basis. UniKwan Innovations Pvt. Ltd makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.</li>
                    <li>Further, UniKwan Innovations Pvt. Ltd does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.</li>
                    
              </ol>
              </p>
              <h4 class="sub-title">Limitations</h4>
              <p> 
              In no event shall UniKwan Innovations Pvt. Ltd or its suppliers be liable for any damages (including without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on UniKwan Innovations Pvt. Ltd.’s website, even if UniKwan Innovations Pvt. Ltd or a UniKwan Innovations Pvt. Ltd authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
              </p>
              <h4 class="sub-title">Accuracy of materials</h4>
              <p> 
              The materials appearing on UniKwan Innovations Pvt. Ltd.’s website could include technical, typographical, or photographic errors. UniKwan Innovations Pvt. Ltd does not warrant that any of the materials on its website are accurate, complete, or current. UniKwan Innovations Pvt. Ltd may make changes to the materials contained on its website at any time without notice. However, UniKwan Innovations Pvt. Ltd does not make any commitment to update the materials.</p>
              <h4 class="sub-title">Links</h4>
              <p> 
              UniKwan Innovations Pvt. Ltd has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by UniKwan Innovations Pvt. Ltd of the site. Use of any such linked website is at the user's own risk.</p>
              <h4 class="sub-title">Modifications</h4>
              <p> 
              UniKwan Innovations Pvt. Ltd may revise these terms of service for its website at any time without notice. By using this website, you agree to be bound by these Terms of Service. You can review the most current version of the Terms of Service at any time on this page.</p>
              <h4 class="sub-title">Governing Law</h4>
              <p> 
              These terms and conditions are governed by and construed in accordance with the laws of Bengaluru and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.</p>




             
            </div>
          </div>
        </section>
        <!-- end section -->
        <?php require '../elements/footerinner.php'; ?>
	</div>
</main>
<?php require '../elements/svgcodeinner.php'; ?>
  </body>
</html>
