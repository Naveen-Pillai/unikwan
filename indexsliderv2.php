<!DOCTYPE html>
<html lang="en">
  <head>
  <title>UniKwan | UI UX Design Agency in Bangalore, India</title>
  <meta name="description" content="Unikwan is a renowned place for next-level user experience design, customer experience design, innovation, and strategic consulting." />
  <?php require '../elements/headerinner.php'; ?>
    
   </head>
  <body>
    <!-- start Header -->
    <?php require '../elements/navbarinner.php'; ?>
<!-- end Header -->
    
    <main id="mainContent">
      <div class="new-hero-sec-holder">
        <div class="container">
          <div class="hero-sec-two-columns">
            <div class="new-hero-sec-righttext-holder">
              <div>
                <svg viewBox="0 0 830 780">
                  <defs>
                    <pattern
                      id="myPattern"
                      x="90"
                      y="90"
                      width="20"
                      height="20"
                      patternUnits="userSpaceOnUse"
                      patternTransform="rotate(90)"
                    >
                      <animateTransform
                        attributeType="xml"
                        attributeName="patternTransform"
                        type="rotate"
                        from="1290 100 100"
                        to="0 100 100"
                        begin="-290"
                        dur="300s"
                        repeatCount="indefinite"
                      />
                      <circle
                        id="arrowcircle"
                        cx="10"
                        cy="10"
                        r="10"
                        stroke="none"
                        fill="#FF51E8"
                      >
                        <animate
                          attributeName="r"
                          type="xml"
                          from="1"
                          to="1"
                          values="2; 5; 2"
                          begin="0s"
                          dur="3s"
                          repeatCount="indefinite"
                        />
                      </circle>
                    </pattern>
                  </defs>

                  <path
                    style="fill: url(#myPattern);"
                    d="M30.525,740.585a109.343,109.343,0,0,1,5.4-154.54L428.713,219.762H109.342a109.342,109.342,0,0,1,0-218.685H692.064A111.99,111.99,0,0,1,819.023,110.419V663.4a109.342,109.342,0,0,1-218.685,0V358.732L185.064,745.981a109.342,109.342,0,0,1-154.539-5.4Z"
                    transform="translate(0 0.001)"
                  />
                </svg>
              </div>
            </div>
            <div class="hero-sec-left-3items-holder">
              <div class="new-hero-sec-lefttext-holder1">
                <article   class="hero-white-text-heading ">
                <article class="hero-text-special-color"> 
                Discover and Design
                </article> 
                <span> experiences to <br>transform <br>business enterprises. </span> 
                </article>
                <br>
                <a class="btn-link-icon btn-link-icon__md" href="../contact">
                <i class="btn__icon btn-previous btn__iconBg">
                  <svg><use xlink:href="#arrow_right"></use></svg>
                </i>
                  <span class="btn__text" style="color: white;">&nbsp;GET IN TOUCH</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
     

      <section class="container-indent section--bg-vertical-line">
		<div class="mainSlider-layout">
			<div class="loading-content"></div>
			<div class="mainSlider mainSlider-size-02 slick-nav-02" data-arrow="true" data-dots="true">
        <div class="slide">
				<div class="v2slider-uk-mobilevisble">
					  <div class="img--holder" data-bg="webp/projectslider/v2/dialog.webp"></div>
          </div>
          <div class="v2slider-uk-mobilehid">
              <div class="img--holder" data-bg="webp/projectslider/v2/dialogmsite.webp"></div>
          </div>
					<div class="slide-content">
						<div class="container" data-animation="fadeInRightSm" data-animation-delay="0s">
							<div class="slide-layout-03 text-left">
								<img class="slide-icon" src="../images/main-slider/01/main-slider-03-icon.png" alt="">
								<div style='padding:0' class="section-heading"          >
            <div class="description" style='color:white'><i></i>Project</div></div>
								<div class="slide-title">
                Dialog
								</div>
								<div class="slide-description">
                Dialog Viu is based on Cloud TV technology and powered by AndroidTV, to offer TV experiences to their audiences globally. 
								</div>
								<a class="btn-link-icon btn-link-icon__md" href="../dialogtv">
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
					</div>
				</div>
        <div class="slide">
        <div class="v2slider-uk-mobilevisble">
					  <div class="img--holder" data-bg="webp/projectslider/v2/gigsasa.webp"></div>
          </div>
          <div class="v2slider-uk-mobilehid">
              <div class="img--holder" data-bg="webp/projectslider/v2/gigsasamsite.webp"></div>
          </div>
           
					<div class="slide-content">
						<div class="container" data-animation="fadeInRightSm" data-animation-delay="0s">
							<div class="slide-layout-03 text-left">
								<img class="slide-icon" src="../images/main-slider/01/main-slider-03-icon.png" alt="">
								<div style='padding:0' class="section-heading"          >
            <div class="description" style='color:white'><i></i>Project</div></div>
								<div class="slide-title">
									Gigsasa
								</div>
								<div class="slide-description">
                Gigsasa is an on-demand pool and management of quality local workers service.
								</div>
								<a class="btn-link-icon btn-link-icon__md" href="../gigsasa">
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
					</div>
				</div>
        <div class="slide">
        <div class="v2slider-uk-mobilevisble">
					  <div class="img--holder" data-bg="webp/projectslider/v2/dishtv.webp"></div>
          </div>
          <div class="v2slider-uk-mobilehid">
              <div class="img--holder" data-bg="webp/projectslider/v2/dishtvmsite.webp"></div>
          </div>
           
					<div class="slide-content">
						<div class="container" data-animation="fadeInRightSm" data-animation-delay="0s">
							<div class="slide-layout-03 text-left">
								<img class="slide-icon" src="../images/main-slider/01/main-slider-03-icon.png" alt="">
								<div style='padding:0' class="section-heading"          >
            <div class="description" style='color:white'><i></i>Project</div></div>
								<div class="slide-title">
                DishTV
								</div>
								<div class="slide-description">
                The Electronics program guide based on next generation User interface and remote with minimalist design. 
								</div>
								<a class="btn-link-icon btn-link-icon__md" href="../dishtv">
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
					</div>
				</div>
        <div class="slide">
        <div class="v2slider-uk-mobilevisble">
					  <div class="img--holder" data-bg="webp/projectslider/v2/atyati.webp"></div>
          </div>
          <div class="v2slider-uk-mobilehid">
              <div class="img--holder" data-bg="webp/projectslider/v2/atyatimsite.webp"></div>
          </div>
           
					<div class="slide-content">
						<div class="container" data-animation="fadeInRightSm" data-animation-delay="0s">
							<div class="slide-layout-03 text-left">
								<img class="slide-icon" src="../images/main-slider/01/main-slider-03-icon.png" alt="">
								<div style='padding:0' class="section-heading"          >
            <div class="description" style='color:white'><i></i>Project</div></div>
								<div class="slide-title">
                Atyati
								</div>
								<div class="slide-description">
                Digital transformation for last mile banking services with distribution capacity of more than 4 million customers across INDIA.  
								</div>
								<a class="btn-link-icon btn-link-icon__md" href="../atyati">
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
					</div>
				</div>
        <div class="slide">
        <div class="v2slider-uk-mobilevisble">
					  <div class="img--holder" data-bg="webp/projectslider/v2/kiba.webp"></div>
          </div>
          <div class="v2slider-uk-mobilehid">
              <div class="img--holder" data-bg="webp/projectslider/v2/kibamsite.webp"></div>
          </div>
           
					<div class="slide-content">
						<div class="container" data-animation="fadeInRightSm" data-animation-delay="0s">
							<div class="slide-layout-03 text-left">
								<img class="slide-icon" src="../images/main-slider/01/main-slider-03-icon.png" alt="">
								<div style='padding:0' class="section-heading"          >
            <div class="description" style='color:white'><i></i>Project</div></div>
								<div class="slide-title">
                KIBA
								</div>
								<div class="slide-description">
                The world first self editing camera with an elegant, simple design and accessible via iOS, Android, and web apps.
								</div>
								<a class="btn-link-icon btn-link-icon__md" href="../kiba">
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
					</div>
				</div>
        <div class="slide">
        <div class="v2slider-uk-mobilevisble"> 
					  <div class="img--holder" data-bg="webp/projectslider/v2/frego.webp"></div>
          </div>
          <div class="v2slider-uk-mobilehid">
              <div class="img--holder" data-bg="webp/projectslider/v2/fregomsite.webp"></div>
          </div>
           
					<div class="slide-content">
						<div class="container" data-animation="fadeInRightSm" data-animation-delay="0s">
							<div class="slide-layout-03 text-left">
								<img class="slide-icon" src="../images/main-slider/01/main-slider-03-icon.png" alt="">
								<div style='padding:0' class="section-heading"          >
            <div class="description" style='color:white'><i></i>Project</div></div>
								<div class="slide-title">
                Frego
								</div>
								<div class="slide-description">
                Frego mobile application caters to youngster and working people to cook at home with ease and minimum effort.
								</div>
								<a class="btn-link-icon btn-link-icon__md" href="../frego">
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
					</div>
				</div>
        
      </div>
     <div class='container' id="mainSlider-nav"></div> 
			
		 
		</div>
	</section>
    <!-- start section -->
		<section class="section section__indent-03" style="overflow: hidden;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-10 col-lg-8">
          <div class="section-heading">
									<!-- <div class="description"><i></i>02</div> -->
									<h4 class="title" style='color:#1b0334;text-align:center'>We anticipate, design, craft meaningful and delightful digital customer experiences for the future.</h4>
								</div>
					</div>
					<div class="chessbox chessbox-top-01">
						
						<div class="chessbox__item">
							<div class="chessbox__img">
              <picture>
              <source srcset="../webp/services/service1.webp" type="image/webp">
              <img src="../images/services/service1.jpg" alt="">
            </picture>
							
							</div>
							<div class="chessbox__description">
								<div class="section-heading">
									<!-- <div class="description"><i></i>02</div> -->
									<h4  class="title">User Experience Design</h4>
								</div>
								<p  >
                Designing usable and delightful interactions between users and products.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                </p>
								<a class="btn-link-icon btn-top" onclick='ServiceCockies(0,"services#service_tabs")'>
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
            </div>
            <div class="chessbox__item">
							<div class="chessbox__img">
              <picture>
              <source srcset="../webp/services/service2.webp" type="image/webp">
              <img src="../images/services/service2.jpg" alt="">
            </picture>
							
							</div>
							<div class="chessbox__description">
								<div class="section-heading">
									 
									<h4 class="title">Customer Experience Design</h4>
								</div>
								<p  >
                Crafting a cohesive brand narrative and a seamless experience to builds strong ties with the customer at the centre. 
								</p>
								<a class="btn-link-icon btn-top" onclick='ServiceCockies(1,"services#service_tabs")' >
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
            <div class="chessbox__item">
							<div class="chessbox__img">
              <picture>
              <source srcset="../webp/services/service3.webp" type="image/webp">
              <img src="../images/services/service3.jpg" alt="">
            </picture>
								
							</div>
							<div class="chessbox__description">
								<div class="section-heading">
									 
									<h4 class="title">Innovation & Strategic Design consulting</h4>
								</div>
								<p  >
                Transforming businesses by using outside in system’s perspective and a human centred approach led digital future.
                	</p>
								<a class="btn-link-icon btn-top" onclick='ServiceCockies(2,"services#service_tabs")'>
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
	  <div class="heroarea-imagesection">
		<div class="heroarea-imgsec-holder"> 
		<div class="imgsection-text-container container">
			<div class="imgsection-left-part">
			  <article class="imgsection-texthead ">
				We build modern digital products to develop strong relationship with the users.  
			  </article>
			  <!-- <a class="btn-link-icon btn-link-icon__md testimonials-uk-mobilevisble" href="../portfolio">
          <i class="btn__icon btn__iconBg" >
            <svg><use xlink:href="#arrow_right"></use></svg>
          </i>
          <span class="btn__text" style="color: white;">DISCOVER</span>
        </a> -->
			</div>
			<div class="imgsection-right-part ">
			  <div>
				<article class="imgsection-subheading">Fintech</article>
				<article class="imgsection-subheading">Ecommerce</article>
				<article class="imgsection-subheading">Public Sector</article>
				<article class="imgsection-subheading">Brand</article>
				<article class="imgsection-subheading">IT Services</article>
			  </div>
			  <div>
				<article class="imgsection-subheading">
				  AI Conversational
				</article>
				<article class="imgsection-subheading">Healthcare</article>
				<article class="imgsection-subheading">Education</article>
				<article class="imgsection-subheading">Media</article>
				<article class="imgsection-subheading">Telematics</article>
			  </div>
      </div>
      <!-- <div class='testimonials-uk-mobilehid'>
      <a class="btn-link-icon btn-link-icon__md " href="../portfolio">
          <i class="btn__icon btn__iconBg" >
            <svg><use xlink:href="#arrow_right"></use></svg>
          </i>
          <span class="btn__text" style="color: white;">DISCOVER</span>
        </a>
        <br>
        <br>
        <br>
		  </div> -->
		  </div>
		  
		</div>
	  </div>
	</div>
      <!-- start main slider -->
       <!-- start section -->
		<section class="section section__indent-03" style="overflow: hidden;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="chessbox chessbox-top-01">
						<div class="chessbox__item" style='margin:0'>
							<div class="chessbox__img">
              <picture>
              <source srcset="../webp/services/approach_pic.webp" type="image/webp">
              <img src="../images/services/approach_pic.jpg" alt="">
            </picture>
							</div>
							<div class="chessbox__description">
								<div class="section-heading">
									<div class="description"><i></i>Our Approach</div>
									<h4 class="title">How We Help Our Clients</h4>
								</div>
                <p  >
                We believe in context immersion and focus on inducing customer-centricity and participation in our process. We are agile, lean and bring insights from a macro to micro-level. We believe in expanding the horizon and creating robust eco-systems to be future-ready.</p>
								<p style='font-weight:bold;color:black'>
                Foresee a vision that resonates with the future
                </p>
                <p   style='font-weight:bold;color:black'>
                Aligning your vision with user's
                </p>
                <p   style='font-weight:bold;color:black'>
                Inspiring teams with scenarios
                </p>
                <p   style='font-weight:bold;color:black'>
                Building a delightful output
                </p>
								<a class="btn-link-icon btn-top" href="../services#our_approach">
									<i class="btn__icon">
										<svg><use xlink:href="#arrow_right"></use></svg>
									</i>
									<span class="btn__text">DISCOVER</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
 <!-- start section -->
 <section class="section--no-padding unikwan-projects section">
   
  <div class="js-carusel-external-box carusel-external-box">
     
    <div class="item">
      <a href="../dialogtv" class="link-icon-video02">
      <picture>
              <source srcset="../webp/projectslider/Dialogtv.webp" type="image/webp">
              <img src="../images/projectslider/Dialogtv.jpg" alt=""> 
            </picture>
       
      </a>
      <div class="layout-external-box">
        <div class="col-title">
          <div class="section-heading size-sm">
            <div class="description"><i></i>Projects</div>
            <h4 class="title">Dialog </h4>
          </div>
        </div>
        <div class="col-description">
          <p>
          Dialog Viu is based on Cloud TV technology and powered by AndroidTV, to offer TV experiences to their audiences globally. 
          </p>
        </div> 
      </div>
    </div>  
     
    <div class="item">
      <a href="../gigsasa" class="link-icon-video02">
      <picture>
              <source srcset="../webp/projectslider/Gigsasa.webp" type="image/webp">
              <img src="../images/projectslider/Gigsasa.jpg" alt=""> 
            </picture>
      </a>
      <div class="layout-external-box">
        <div class="col-title">
          <div class="section-heading size-sm">
            <div class="description"><i></i>Projects</div>
            <h4 class="title">Gigsasa  </h4>
          </div>
        </div>
        <div class="col-description">
          <p>
          Gigsasa is an on-demand pool and management of quality local workers service. <br><br>
</p>
        </div> 
      </div>
    </div>
    <div class="item">
      <a href="../dishtv" class="link-icon-video02">
      <picture>
              <source srcset="../webp/projectslider/Dishtv.webp" type="image/webp">
              <img src="../images/projectslider/Dishtv.jpg" alt=""> 
            </picture>
      </a>
      <div class="layout-external-box">
        <div class="col-title">
          <div class="section-heading size-sm">
            <div class="description"><i></i>Projects</div>
            <h4 class="title">DishTV  </h4>
          </div>
        </div>
        <div class="col-description">
          <p>
          The Electronics program guide based on next generation User interface and remote with minimalist design. 
 </p>
        </div> 
      </div>
    </div>
    <div class="item">
      <a href="../atyati" class="link-icon-video02">
        <picture>
              <source srcset="../webp/projectslider/Atyati.webp" type="image/webp">
              <img src="../images/projectslider/Atyati.jpg" alt=""> 
            </picture>
      </a>
      <div class="layout-external-box">
        <div class="col-title">
          <div class="section-heading size-sm">
            <div class="description"><i></i>Projects</div>
            <h4 class="title">Atyati  </h4>
          </div>
        </div>
        <div class="col-description">
          <p>
          Digital transformation for last mile banking services with distribution capacity of more than 4 million customers across INDIA.  
 </p>
        </div> 
      </div>
    </div>
    <div class="item">
      <a href="../kiba" class="link-icon-video02">
        <picture>
              <source srcset="../webp/projectslider/Kiba.webp" type="image/webp">
              <img src="../images/projectslider/Kiba.jpg" alt=""> 
            </picture>
      </a>
      <div class="layout-external-box">
        <div class="col-title">
          <div class="section-heading size-sm">
            <div class="description"><i></i>Projects</div>
            <h4 class="title">KIBA  </h4>
          </div>
        </div>
        <div class="col-description">
          <p>
          The world first self editing camera with an elegant, simple design and accessible via iOS, Android, and web apps.
 </p>
        </div> 
      </div>
    </div>
    <div class="item">
      <a href="../frego" class="link-icon-video02">
        <picture>
              <source srcset="../webp/projectslider/Frego.webp" type="image/webp">
              <img src="../images/projectslider/Frego.jpg" alt=""> 
            </picture>
      </a>
      <div class="layout-external-box">
        <div class="col-title">
          <div class="section-heading size-sm">
            <div class="description"><i></i>Projects</div>
            <h4 class="title">Frego </h4>
          </div>
        </div>
        <div class="col-description">
          <p>
          Frego mobile application caters to youngster and working people to cook at home with ease and minimum effort.

</p>
        </div> 
      </div>
    </div>
    
   
   
  </div>
  <div  class="layout-external-box external-boxuk-styles" style=''>
  <div  class="external-box-ukstyles">
  <div class="col-nav-slider">
        <i class="btn__icon btn-previous">
            <svg><use xlink:href="#arrow_left"></use></svg>
          </i>
          <i class="btn__icon btn-next" >
            <svg><use xlink:href="#arrow_right"></use></svg>
          </i>
          </div>
        </div>
        </div>
  <div class="btn-row btn-top text-center">
    <a class="btn-link-icon" href="../portfolio">
      <i class="btn__icon">
        <svg><use xlink:href="#arrow_right"></use></svg>
      </i>
      <span class="btn__text">VIEW ALL</span>
    </a>
  </div>
</section>
<!-- end section -->
<!-- start section -->
<section class="section section-default-top">
		<div class="container section--pr">
    <div class="section-heading section-heading_indentg03 section-heading__right-arrow"          >
            <div class="description"><i></i>Blogs</div>
            <h3 class="title">News & Insights</h3>
          </div>
			<div class="slick-arrow-extraright">
			<div class="slick-arrow slick-prev">
					<svg><use xlink:href="#arrow_left"></use></svg>
				</div>
				<div class="slick-arrow  slick-next">
					<svg><use xlink:href="#arrow_right"></use></svg>
        </div>
        
			</div>
			<div class="js-carusel-news promobox02__slider">
				<div class="item">
        <a href="../award-from-clutch-2020" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            
            <picture>
              <source srcset="../webp/blog/slider/clutch2020.webp" type="image/webp">
              <img src="../images/blog/slider/clutch2020.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">Award from Clutch</h4>
              <div class="promobox03__show">
                <p>
                  UI/UX Design Award from Clutch 2020.</p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
				</div>
				<div class="item">
        <a href="../is-branding-important" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/Branding.webp" type="image/webp">
              <img src="../images/blog/slider/Branding.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
            <h4 class="promobox03__title">Company Branding</h4>
              <div class="promobox03__show">
                <p>
                Why Is Branding Important To Your Company?</p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
				</div>
				<div class="item">
        <a href="../5-best-ui-ux-trends" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/5uiux.webp" type="image/webp">
              <img src="../images/blog/slider/5uiux.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">UI and UX trends</h4>
              <div class="promobox03__show">
                <p>
                5 UI/UX Trends To Watch Out In 2020 </p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
				</div>
				<div class="item">
        <a href="../design-thinking" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/designthinking.webp" type="image/webp">
              <img src="../images/blog/slider/designthinking.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">Design Thinking</h4>
              <div class="promobox03__show">
                <p>
                Using Design Thinking As A Strategy For Innovation </p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
        </div>
        <div class="item">
        <a href="../banglore_based_top_design_agency" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/unikwan.webp" type="image/webp">
              <img src="../images/blog/slider/unikwan.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">Bangalore based</h4>
              <div class="promobox03__show">
                <p>
                Bangalore based Top design agency </p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
        </div>
        <div class="item">
        <a href="../5-reasons-why-you-need-to-hire-a-design-team-for-your-company" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/hire.webp" type="image/webp">
              <img src="../images/blog/slider/hire.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">5 reasons to hire</h4>
              <div class="promobox03__show">
                <p>
                5 Reasons Why You Should Hire A Design Team </p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
        </div>
        <div class="item">
        <a href="../award-from-clutch-2019" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/clutch2019.webp" type="image/webp">
              <img src="../images/blog/slider/clutch2019.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">Award from clutch</h4>
              <div class="promobox03__show">
                <p>
                UniKwan is a Top 2019 Design Agency on Clutch 2019</p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
        </div>
        <div class="item">
        <a href="../What-is-design-for-problem-solving" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/whatdesign.webp" type="image/webp">
              <img src="../images/blog/slider/whatdesign.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">Design for problem solving</h4>
              <div class="promobox03__show">
                <p>
                Redefining How Design Thinking Helps Problem Solving </p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
        </div>
        <div class="item">
        <a href="../User-Research-and-the-benefits" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/uxresearch.webp" type="image/webp">
              <img src="../images/blog/slider/uxresearch.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">User Research benefits </h4>
              <div class="promobox03__show">
                <p>
                Benefits Of User Research In Design </p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
        </div>
        <div class="item">
        <a href="../chrome-plugins-to-accelerate-your-UX-design-research" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/chrome.webp" type="image/webp">
              <img src="../images/blog/slider/chrome.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">Chrome plugins to accelerate </h4>
              <div class="promobox03__show">
                <p>
                5 Google Chrome Extension For Designers [2020 Update]</p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
        </div>
        <div class="item">
        <a href="../How-to-make-your-UI-design-CRAP" class="blogs-uk-padings promobox03 block-once">
          <div class="promobox03__img">
            <picture>
              <source srcset="../webp/blog/slider/crap.webp" type="image/webp">
              <img src="../images/blog/slider/crap.jpg" alt="blogs">
            </picture>
          </div>
          <div class="promobox03__description">
            <div class="promobox03__layout">
              <h4 class="promobox03__title">Make UI design CRAP </h4>
              <div class="promobox03__show">
                <p>
                How To Make Your UI Design CRAP</p>
                <span class="promobox03__link">MORE</span>
              </div>
            </div>
          </div>
        </a>
        </div>
        

      </div> 
    </div>
    <br>
    <div class="btn-row btn-top text-center">
      <a class="btn-link-icon" href="../blog">
        <i class="btn__icon">
          <svg><use xlink:href="#arrow_right"></use></svg>
        </i>
        <span class="btn__text">VIEW ALL</span>
      </a>
    </div>
	</section>
	<!-- end section -->
 
      <!-- start section -->
      <section class="section section-default-top section__indent-11 section--bg-03 section--pr">
        <div class="container">
          <div class="section">
            <div class="wrapper-carusel-partners wrapper-arrow-center">
              <div class="carusel-partners js-carusel-partners">
                <div class="item">
                  <a href="../"
                    ><img src="../images/clientlogos/AdityaBirla.png" alt=""
                  /></a>
                  <a href="../"
                    ><img src="../images/clientlogos/Adobe.png" alt=""
                  /></a>
                </div>
                <div class="item">
                  <a href="../"
                    ><img src="../images/clientlogos/Atyati.png" alt=""
                  /></a>
                  <a href="../"
                    ><img src="../images/clientlogos/Dialog.png" alt=""
                  /></a>
                </div>
                <div class="item">
                  <a href="../"
                    ><img src="../images/clientlogos/GMCapital.png" alt=""
                  /></a>
                  <a href="../"
                    ><img src="../images/clientlogos/GovKarnataka.png" alt=""
                  /></a>
                </div>
                <div class="item">
                  <a href="../"
                    ><img src="../images/clientlogos/Jitfin.png" alt=""
                  /></a>
                  <a href="../"
                    ><img src="../images/clientlogos/Lightmetrics.png" alt=""
                  /></a>
                </div>
                <div class="item">
                  <a href="../"
                    ><img src="../images/clientlogos/McAfee.png" alt=""
                  /></a>
                  <a href="../"
                    ><img src="../images/clientlogos/Microsoft.png" alt=""
                  /></a>
                </div>
                <div class="item">
                  <a href="../"
                    ><img src="../images/clientlogos/Mitlabs.png" alt=""
                  /></a>
                  <a href="../"
                    ><img src="../images/clientlogos/NagraKudelski.png" alt=""
                  /></a>
                </div>
                <div class="item">
                  <a href="../"
                    ><img src="../images/clientlogos/Nokia.png" alt=""
                  /></a>
                  <a href="../"
                    ><img src="../images/clientlogos/Philipscare.png" alt=""
                  /></a>
                </div>
                <div class="item">
                  <a href="../"
                    ><img src="../images/clientlogos/TataElxsi.png" alt=""
                  /></a>
                  <a href="../"
                    ><img src="../images/clientlogos/Yourstory.png" alt=""
                  /></a>
                </div>
              </div>
              <div class="slick-slick-arrow">
                <div class="slick-arrow slick-prev">
                  <svg><use xlink:href="#arrow_left"></use></svg>
                </div>
                <div class="slick-arrow slick-next">
                  <svg><use xlink:href="#arrow_right"></use></svg>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- end section -->
      <!-- start section -->
      <section class="section section-default-top">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-xl-3">
              <div class="section-heading">
                <div class="description"><i></i>Testimonials</div>
                <h4 class="title">What Our Clients Say About Us</h4>
              </div>
              <div class="slick-arrow-external testimonials-uk-mobilevisble">
                <div class="slick-arrow slick-prev">
                  <svg><use xlink:href="#arrow_left"></use></svg>
                </div>
                <div class="slick-arrow slick-next">
                  <svg><use xlink:href="#arrow_right"></use></svg>
                </div>
              </div>
            </div>
            <div
              class="divider divider__lg d-block d-xl-none d-lg-none d-md-none"
            ></div>
            <div class="col-md-8 col-xl-9">
              <div class="js-carusel-blockquote">
                <div class="item">
                  <div class="blockquote-box">
                    <div class="blockquote-box__icon">
                      <svg>
                        <use xlink:href="#quotes"></use>
                      </svg>
                    </div>
                    <div class="blockquote-box__description">
                    UniKwan is our preferred port of call for all of our design needs - from making
                    product showcase videos to app UX design. They do a deep dive of your
                    business and a holistic analysis of customer needs, and the end result is a very
                    intuitive and effective user experience. Would strongly recommend working
                    with them "
                    </div>
                    <div class="text-style01">
                      <p>
                        - SOUMIK UKIL Co-Founder, CEO, Lightmetrics
                      </p>
                    </div>
                    
                  </div>
                </div>
                <div class="item">
                  <div class="blockquote-box">
                    <div class="blockquote-box__icon">
                      <svg>
                        <use xlink:href="#quotes"></use>
                      </svg>
                    </div>
                    <div class="blockquote-box__description">
                    UniKwan always had amazing talent to accomplish all needs in UX (app
                    design, flyers, and Video editing). Whether, its your startup or an established
                    company, UniKwan can provide professional, end to end design solutions. They
                    have always delivered on or ahead of time within budget. They are very reliable
                    and resourceful. I have the highest recommendation for their work ethics ”
                    </div>
                    <div class="text-style01">
                      <p>
                        - RAJI KANNAN Innovator, Director of Product, Intel Coporation, USA
                      </p>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="blockquote-box">
                    <div class="blockquote-box__icon">
                      <svg>
                        <use xlink:href="#quotes"></use>
                      </svg>
                    </div>
                    <div class="blockquote-box__description">
                     In the past 5 years, UniKwan has been working with Nagra Bangalore to design
                    and develop user interfaces for leading PayTV operators in India. UniKwan has
                    also functioned as an integral part of the development team in the
                    design process. UniKwan has the agility of a start-up while still bringing it's
                    expertise in user experience to the fore when meeting strict deadlines ”
                    </div>
                    <div class="text-style01">
                      <p>
                        - RAGHUNANDAN BALASUBRAMANIAM Director Engineering, Nagra (Kudelski)
                      </p>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="blockquote-box">
                    <div class="blockquote-box__icon">
                      <svg>
                        <use xlink:href="#quotes"></use>
                      </svg>
                    </div>
                    <div class="blockquote-box__description">
                    UniKwan team did a great job taking our fuzzy concept and fleshing it
                    out into a really good prototype. They are self-starters and are able to
                    think on our behalf as well. With minimal iterations, we had an outcome
                    we were happy with ”
                    </div>
                    <div class="text-style01">
                      <p>
                        - PRAMOD KRISHNAMURTHY Entrepreneur, Ex-CTO, Birla Sunlife
                      </p>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="blockquote-box">
                    <div class="blockquote-box__icon">
                      <svg>
                        <use xlink:href="#quotes"></use>
                      </svg>
                    </div>
                    <div class="blockquote-box__description">
                    The design services & promo video’s release attracted 1,200 orders of
                    the device and won two innovation awards in 2016. UniKwan applied
                    creative skills at a competitive price to ensure a timely launch date ”
                    </div>
                    <div class="text-style01">
                      <p>
                        - PRANAV MISHRA CEO Jump, Ex-Nokia Top inventor, Co-founder Lensbricks
                      </p>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="blockquote-box">
                    <div class="blockquote-box__icon">
                      <svg>
                        <use xlink:href="#quotes"></use>
                      </svg>
                    </div>
                    <div class="blockquote-box__description">
                    UniKwan has a strong and a scientific design framework. Mapping out the customer journey and identifying the friction points was done extremely well. ”
                    </div>
                    <div class="text-style01">
                      <p>
                        -  PRAKASH PRABHU, CEO Atyati Technologies
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class='testimonials-uk-mobilehid'>
          <div class="slick-arrow-external">
                <div class="slick-arrow slick-prev">
                  <svg><use xlink:href="#arrow_left"></use></svg>
                </div>
                <div class="slick-arrow slick-next">
                  <svg><use xlink:href="#arrow_right"></use></svg>
                </div>
              </div>
          </div>
        </div>
      </section>
      <!-- end section -->
       <!-- start section -->
       <section class="section section-default-top">
        <div class="container">
          <div class="section-heading section-heading_indentg02">
            <div class="description"><i></i>Media</div>
            <h4 class="title">Featured</h4>
          </div>
          <div
            class="js-award-carusel carusel-award slick-arrow-center js-align-arrow-award"
          >
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/1.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/2.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/3.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/4.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/5.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/6.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/7.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/8.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/9.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
            <div class="item">
              <a href="#" class="award">
                <div class="award__img">
                  <img src="../images/featuredlogos/10.png" alt="" />
                </div>
                <div class="award__description">
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>
      <!-- end section -->
      <?php require '../elements/footerinner.php'; ?>
    
    </main>
    <a href="../" id="js-scroll-down" class="btn-scroll-down">
      Scroll down <i></i>
    </a>
    <a href="../" id="js-back-to-top" class="btn-scroll-top">
      <svg><use xlink:href="#arrow_left"></use></svg>
    </a>

    <?php require '../elements/svgcodeinner.php'; ?>
  </body>
</html>
