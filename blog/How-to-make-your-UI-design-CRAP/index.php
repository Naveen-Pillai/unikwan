<!DOCTYPE html>
<html lang="en">

<head>
    <title>How To Make Your UI Design CRAP</title>
    <meta name="description"
        content="In this post, learn about CRAP design fundamentals and how to use it in your UI design.">
    <meta name="keywords" content="CRAP">
    <meta property="og:image" content="../../images/blog/articles/crap.jpg" />
    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section">
                <div style='height:40px'></div>
                <div class="container">
                    <div class=" ">
                        <div class=" ">
                            <div class='blog-margin-holler'>
                                <div class="postsingle">
                                    <div class="section-heading" style='padding-bottom:0'>
                                        <a href='../../blog' style='text-decoration:none'>
                                            <div class="description"><i></i>Blog</div>
                                        </a>
                                        <h1 class="postsingle__title">
                                            How To Make Your UI Design CRAP

                                        </h1>
                                    </div>
                                    <p>Maybe you have already heard of it, maybe you are new to this game. But one more
                                        repetition
                                        doesn’t hurt, does it?. Have you ever looked at your UI design and wonder about
                                        that
                                        one thing
                                        missing? Have you ever looked at your shoulder for that angel to magically tell
                                        you
                                        what to fix
                                        so that your baby UI has that elite university future? Well here is your answer.
                                    </p>

                                    <div class='blog_autohr_holder'>
                                        <div class='blog_avatar_holder'>
                                            <img class='blog_img_avatar' width='30px' src="../../images/favicon.png"
                                                alt="Importance of Branding" />
                                            <div>
                                                <article class='blog_author_text'><strong>UniKwan</strong></article>
                                                <article class='blog_author_text'>January 2, 2020 · 4 min read</article>
                                            </div>
                                        </div>
                                        <div class="">
                                            <ul class="social-icon">
                                                <a class="share_icons_margin"
                                                    href="https://www.linkedin.com/company/unikwan-innovations-private-limited"
                                                    target='_blank'>
                                                    <img src="../../images/footer/Linkedin.png" alt="social media" />
                                                </a>
                                                <a class="share_icons_margin"
                                                    href="https://www.behance.net/unikwaninnovations" target='_blank'>
                                                    <img src="../../images/footer/Behance.png" alt="social media" />
                                                </a>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="postsingle__img">
                                <picture>
                                    <source srcset="../../webp/blog/articles/crap.webp" type="image/webp">
                                    <img src="../../images/blog/articles/crap.jpg"
                                        alt="	How To Make Your UI Design CRAP" />
                                </picture>

                            </div>
                            <div class='blog-margin-holler'>
                                <br>
                                <p> CRAP
                                    Robin Patricia Williams, the legendary educator , author and overall
                                    intellectual badass,
                                    proposed a system of fundamentals so that you could analyse your own design and
                                    find where
                                    you could fix it.
                                </p>
                                <p>Here is an excerpt from the book where she tells you why .
                                </p>
                                <p>"Many years ago I received a tree identification book for
                                    Christmas. I was at my parents'
                                    home, and after all the gifts had been opened I decided to go out and identify
                                    the trees in
                                    the neighbor-hood. Before I went out, I read through part of the book. The first
                                    tree in the
                                    book was the Joshua tree because it only took two clues to identify it. Now the
                                    Joshua
                                    tree is a really weird-looking tree and I looked at that picture and said to
                                    myself, "Oh, we
                                    don't have that kind of tree in Northern California. That is a weird-looking
                                    tree. I would
                                    know if I saw that tree, and I've never seen one before:</p>
                                <p>So I took my book and went outside. My parents lived in a
                                    cul-de-sac of six homes. Four
                                    of those homes had Joshua trees in the front yard. I had lived in that house for
                                    thirteen
                                    years, and I had never seen a Joshua tree. I took a walk around the block, and
                                    there
                                    must have been a sale at the nursery when everyone was landscaping their new
                                    homes
                                    —at least 80 percent of the homes had Joshua trees in the front yards. And I had
                                    never
                                    seen one before! Once I was conscious of the tree—once I could name it—I saw it
                                    everywhere. Which is exactly my point: Once you can name something, you're
                                    conscious of it. You have power over it. You own it. You're in control"</p>
                                <p>Don't you want to be in control, you fellow miserable designer? Well , here we
                                    go.
                                    CRAP signifies 4 core heuristic principles for you to check your designs with.
                                    They are,
                                    Contrast, Repetition, Alignment, Proximity. Sounds familiar right? But have you
                                    thought of them
                                    together? If you use these principles to evaluate your designs, It leads to a
                                    better design more
                                    often than not. Lets get in to details
                                </p>
                                <p style='font-weight:bold;font-size:24px;margin-top:25px;line-height:1.3;'>Contrast</p>
                                <p>Haven’t we all received an answer sheet from our teacher with a red ink underline
                                    all over ?
                                    Why did she do that? Well . Contrast. She increased the contrast between your
                                    right answers
                                    and wrong answers. Didn’t your eye just jump to those underlined parts? See?
                                    Again contrast.
                                    Increasing contrast between dissimilar elements is a foolproof way to
                                    communicate to the
                                    viewer that these elements are not about the same stuff.
                                </p>
                                <p>Tip #1: In typography to maintain contrast, skip a font weight or double the font
                                    size . Look at
                                    the following examples and see.
                                </p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/crap/image2.webp" type="image/webp">
                                        <img src="../../images/blog/articles/crap/image2.png"
                                            alt="Name as per the screenshot" />
                                    </picture>

                                </div>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/crap/image2_1.webp" type="image/webp">
                                        <img src="../../images/blog/articles/crap/image2_1.png"
                                            alt="Name as per the screenshot" />
                                    </picture>

                                </div>
                                <br>
                                <p>Tip #2: In colors, using opposing colors from the color wheel is a good thumb
                                    rule. But to really
                                    check if those colors work, make your design grayscale and see if the contrast
                                    you wanted is
                                    there.
                                </p>
                                <p>Tip #3: Count the points of contrast. More differences, more contrast. Make sure
                                    that the
                                    contrasting elements have at least more than one point of contrast. Look at the
                                    following
                                    example. The first two sets have one point of contrast. The third set has two
                                    points of contrast.</p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/crap/image3.webp" type="image/webp">
                                        <img style='width:60%' src="../../images/blog/articles/crap/image3.png"
                                            alt="Screenshot of *XYZ" />
                                    </picture>

                                </div>
                                <br>
                                <p style='font-weight:bold;font-size:24px;margin-top:25px;line-height:1.3;'>Repetition
                                </p>
                                <p>Repetition means familiarity. Most of the comfortable parts of our lives are
                                    repetitions or familiar.
                                    We go back to home because it is familiar, or repetitive. It is not an error but
                                    a comfort. Knowing
                                    everything about what they are , where they are, who they are. This simple
                                    analogy applies to
                                    UI design as well. Once you have sold a part of the UI design to the user, use
                                    that for anything
                                    similar. It is familiar and thus more friendly. Use repetition where and only
                                    where you want to tell
                                    your user that they are similar. If not, bring contrast.
                                </p>
                                <p>Tip #1 Repeat similar visual style to guide the eye. Amongst, contrasting
                                    elements, repeating
                                    visual styles would grab the users attention more effectively.
                                    Look at this stay detail page by Airbnb. Look how your eye wanders from the
                                    Title, to the type to
                                    the price. Even though the entire view has a lot more information in this, a
                                    similar typeface
                                    guides your eyes to the most relevant or sought after details.</p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/crap/image4.webp" type="image/webp">
                                        <img style='width:60%' src="../../images/blog/articles/crap/image4.png"
                                            alt="Screenshot of *XYZ" />
                                    </picture>

                                </div>
                                <br>
                                <p>Tip#2 If you are using shadows, think about all the shadows. As in, think about
                                    where the light is
                                    coming from. Make sure that all your shadows in the UI are made from a single
                                    light source.
                                    See ? repetition</p>
                                <p style='font-weight:bold;font-size:24px;margin-top:25px;line-height:1.3;'>Alignment
                                </p>
                                <p>Haven't we all liked how and when parades marched together in unison and rhythm?
                                    Isn’t it
                                    much easier on your eyes than a crowd that is mingling within themselves
                                    infinitely? Alignment
                                    is the answer. By default, nature doesn’t care about alignment. Alignment is a
                                    conscious choice
                                    brought in by humans to their actions. It makes us notice choices, and also
                                    tells us that nothing
                                    is placed arbitrarily.
                                </p>
                                <p>Tip #1 In some cases, top, bottom, left, right and center alignment is not the
                                    answer. You’ll often
                                    run into cases where elements are aligned perfectly, but do not appear to be due
                                    to the uneven
                                    distribution of visual weight of an element. In these cases you’ll have to use
                                    your eye and adjust
                                    the alignment according to what looks best. It's often called optical alignment.
                                </p>
                                <p style='font-weight:bold;font-size:24px;margin-top:25px;line-height:1.3;'>Proximity
                                </p>
                                <p>How do we read? How do we figure out from a potentially infinite array of
                                    letters, to form words
                                    and sentences. How do we know that they are the words that we are supposed to
                                    read?
                                    Proximity. We write in a way the letters that make up a word are in close
                                    proximity so that the
                                    reader knows how to read them.
                                </p>
                                <p>Proximity tells us how to group elements to make sense of them rather than their
                                    individual
                                    sense. Proximity helps us separate visual elements or puts them together. It is
                                    a visual tool that
                                    we are subconsciously already familiar with.</p>
                                <p>Look at the example below. The two sets of information are given in the same
                                    view, even
                                    though there is no distinguishing separation other than spacing, we are so clear
                                    on which
                                    information belongs to which set.</p>
                                <br>
                                <div class="postsingle__img">

                                    <picture>
                                        <source srcset="../../webp/blog/articles/crap/image5.webp" type="image/webp">
                                        <img style='width:60%' src="../../images/blog/articles/crap/image5.png"
                                            alt="Screenshot of *XYZ" />
                                    </picture>
                                </div>
                                <br>
                                <p>Armed with these guidelines, you can go through your designs when you are unsure
                                    and see
                                    where you missed something. These principles will definitely tell you why your
                                    eye is twitching.
                                    But there is also another part. This is just a set of guidelines for you to
                                    check yourself with.
                                    These are not rules set in stone. You can break them when you want, but if you
                                    don't have a
                                    solid reason to break one, or more of these, your design will stick out like a
                                    sore thumb. Ie.
                                    When you break one of these rules, make sure that you have a solid good reason
                                    to do so.
                                    Are you ready to go CRAP with your design?</p>
                                <br>
                                <div class="social-icon-wrapper">
                                    <div class="social-caption">
                                        Share:
                                    </div>
                                    <ul class="social-icon">
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("fb","How To Make Your UI Design CRAP")'
                                                src="../../images/footer/facebook.png" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("wapp","How To Make Your UI Design CRAP")'
                                                src="../../images/footer/whatsapp.svg" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("tweet","How To Make Your UI Design CRAP")'
                                                src="../../images/footer/twitter.png" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("li","How To Make Your UI Design CRAP")'
                                                src="../../images/footer/Linkedin.png" alt="social media" />
                                        </a>
                                    </ul>
                                </div>
                                <hr>
                            </div>


                        </div>
                    </div>
                    <?php require '../latest-blogs.php'; ?>
                </div>
        </div>
        </section>
        <!-- end section -->
        <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>