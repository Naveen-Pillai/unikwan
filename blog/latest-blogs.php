<article class='blogv2_header_title'>Also On UniKwan Blog</article>
<div class=''>
    <div class="blogv2_list_holder">
        <div class='blogv2_single_box'>
            <div class='blogv2_box_imghold'>
                <a class='textdecoration_style' href='../is-branding-important'>
                    <img class='blogv2_box_imge' width='100%' alt='education sector'
                        src='../../images/blog/v3/list/Branding.jpg' />
                </a>
            </div>
            <a class='textdecoration_style' href='../is-branding-important'>
                <article class='blogv2_heading'>Why Is Branding Important To Your Company?
                </article>
            </a>
            <article class='blogv2_sub_heading'>When you think of Raymond what is the first image
                that strikes you? When you think of Amazon,…</article>
        </div>
        <div class='blogv2_single_box'>
            <div class='blogv2_box_imghold'>
                <a class='textdecoration_style' href='../User-Research-and-the-benefits'>
                    <img class='blogv2_box_imge' width='100%' alt='unikwan blog'
                        src='../../images/blog/v3/list/uxresearch.jpg' />
                </a>
            </div>
            <a class='textdecoration_style' href='../User-Research-and-the-benefits'>
                <article class='blogv2_heading'>Benefits Of User Research In Design
                </article>
            </a>
            <article class='blogv2_sub_heading'>The term User Research and User Testing is not
                glamourised enough in the industry ecosystem to engage it…</article>
        </div>
        <div class='blogv2_single_box'>
            <div class='blogv2_box_imghold'>
                <a class='textdecoration_style' href='../design-thinking'>
                    <img class='blogv2_box_imge' width='100%' alt='education sector'
                        src='../../images/blog/v3/list/design.jpg' />
                </a>
            </div>
            <a class='textdecoration_style' href='../design-thinking'>
                <article class='blogv2_heading'>
                    Using Design Thinking As A Strategy For Innovation</article>
            </a>
            <article class='blogv2_sub_heading'>Using Design Thinking As A Strategy For Innovation
                There are clear distinctions between design thinking and design. If…</article>
        </div>
    </div>
</div>
<div class="btn-row text-center">
    <a class="btn-link-icon" href="../../blog">
        <i class="btn__icon">
            <svg>
                <use xlink:href="#arrow_right"></use>
            </svg>
        </i>
        <span class="btn__text">VIEW ALL</span>
    </a>
</div>