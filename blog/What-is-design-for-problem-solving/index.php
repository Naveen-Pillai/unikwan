<!DOCTYPE html>
<html lang="en">

<head>
    <title> Redefining How Design Thinking Helps Problem Solving </title>
    <meta name="description"
        content="Unikwan is a renowned place for next-level user experience design, customer experience design, innovation, and strategic consulting." />
    <meta property="og:image" content="../../images/blog/articles/design.jpg" />
    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section">
                <div style='height:40px'></div>
                <div class="container">
                    <div class=" ">
                        <div class=" ">
                            <div class='blog-margin-holler'>
                                <div class="postsingle">
                                    <div class="section-heading" style='padding-bottom:0'>
                                        <a href='../../blog' style='text-decoration:none'>
                                            <div class="description"><i></i>Blog</div>
                                        </a>
                                        <h1 class="postsingle__title">
                                            Redefining How Design Thinking Helps Problem Solving
                                        </h1>
                                    </div>
                                    <p>Understanding the common misunderstandings of what we know about design.
                                        Redefining how design thinking helps problem solving.

                                    </p>
                                    <div class='blog_autohr_holder'>
                                        <div class='blog_avatar_holder'>
                                            <img class='blog_img_avatar' width='30px' src="../../images/favicon.png"
                                                alt="Importance of Branding" />
                                            <div>
                                                <article class='blog_author_text'><strong>UniKwan</strong></article>
                                                <article class='blog_author_text'>12th April, 2020 · 3 min read
                                                </article>
                                            </div>
                                        </div>
                                        <div class="">
                                            <ul class="social-icon">
                                                <a class="share_icons_margin"
                                                    href="https://www.linkedin.com/company/unikwan-innovations-private-limited"
                                                    target='_blank'>
                                                    <img src="../../images/footer/Linkedin.png" alt="social media" />
                                                </a>
                                                <a class="share_icons_margin"
                                                    href="https://www.behance.net/unikwaninnovations" target='_blank'>
                                                    <img src="../../images/footer/Behance.png" alt="social media" />
                                                </a>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="postsingle__img">

                                <picture>
                                    <source srcset="../../webp/blog/articles/design.webp" type="image/webp">
                                    <img src="../../images/blog/articles/design.jpg" alt="" />
                                </picture>
                            </div>
                            <br>
                            <div class='blog-margin-holler'>
                                <p>In the world where we are thousands of miles away from our friends. Where we
                                    lacked space for storing information. When we were losing social interactions,
                                    smartphones came as a wave that swept away all of our problems. The question
                                    here is, “how did the person who made smartphones knew all our problems?” Even
                                    Alexander Graham Bell, the inventor of the telephone, never would have thought
                                    his one invention would solve so many problems of people.

                                </p>
                                <h2
                                    style='font-weight:bold;padding: 0;font-size:24px; margin-top:25px;line-height:1.3;'>
                                    Who designs nowadays?
                                </h2>
                                <p>Problems are an integral part of human beings and we value it to the utmost. If
                                    not we wouldn’t have been bothering about solving it. There have come, gone,
                                    failed and succeeded so many problem solvers in this world. But there are only a
                                    few who have attained success. It is a real known fact that not all can seek to
                                    solve their problems. It might seem nonsensical but it is true. Back then they
                                    were called philosophers, thinkers, scientists and now they are called
                                    designers.

                                </p>
                                <!-- <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/solving_inner.webp" type="image/webp">
                                        <img src="../../images/blog/articles/solving_inner.jpg" alt="" />
                                    </picture>
                                </div>
                                <p>Constructive design thinking has helped many designers solve problems faster and
                                    better
                                </p> -->

                                <h2
                                    style='font-weight:bold;padding: 0;font-size:24px; margin-top:25px;line-height:1.3;'>
                                    Why do we need designers?

                                </h2>
                                <p>Every creator’s intention while making the product is to cater to the needs of
                                    the problem. And finally make the product solve the problem. As simple as that.
                                    But what if the user finds it problematic to solve the problem itself. Imagine
                                    you wanted a watch with an alarm feature in it. The watchmaker adds an alarm
                                    feature in it with a small buzzer. Now you know you have an alarm in your watch
                                    but you find it difficult every time while setting an alarm. The watchmaker had
                                    made the buttons very confusing to use. Your problem is solved with an added
                                    problem. After a point, you might feel like throwing the watch away out of
                                    frustration.

                                </p>
                                <p>What we clearly see here is, we don’t just need people who solve our problems. We
                                    need people who can solve problems in a way we can solve problems. We need
                                    people who can understand our faults, who can understand behaviours. When we are
                                    in a hurry, when we can wait, even when we would be irritated or angry. One has
                                    to be empathetic and patient in understanding what we want actually. A person
                                    who has all these abilities is called a designer.

                                </p>
                                <h2
                                    style='font-weight:bold;padding: 0;font-size:24px; margin-top:25px;line-height:1.3;'>
                                    What is design actually?


                                </h2>
                                <p>Design is not just about adding nice looking colours to a wall or to a website
                                    for that matter. It is about being passionate about catering to one’s needs.
                                    Designers carefully planning the actions and emotions of users. Guiding users to
                                    a better experience. Providing simple and clear information. It is also about
                                    how it looks because we don’t want our users to run away from using it. After
                                    all this, the intention is to make the users feel proud and happy that they
                                    could solve their problems by themselves.

                                </p>
                                <p>What is design, is not what you have been thinking. It is what that made you not
                                    think.

                                </p>
                                <div class="blogquote_boxhold">
                                    <div class="blogquote_sidebar"></div>
                                    <article class="blog_quoite_texting">“Profound ideas are always obvious once
                                        they are understood.”<br>&nbsp;&nbsp;&nbsp;&nbsp;– Don Norman
                                    </article>
                                </div>
                                <p style='cursor:pointer'
                                    onClick="Gotolink('https://medium.com/@unikwaninnova/what-is-design-design-for-problem-solving-b92500957739')">
                                    https://medium.com/@unikwaninnova/what-is-design-design-for-problem-solving-b92500957739

                                </p>
                                <br>
                                <div class="social-icon-wrapper">
                                    <div class="social-caption">
                                        Share:
                                    </div>
                                    <ul class="social-icon">
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("fb","Redefining How Design Thinking Helps Problem Solving ")'
                                                src="../../images/footer/facebook.png" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("wapp","Redefining How Design Thinking Helps Problem Solving ")'
                                                src="../../images/footer/whatsapp.svg" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("tweet","Redefining How Design Thinking Helps Problem Solving ")'
                                                src="../../images/footer/twitter.png" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("li","Redefining How Design Thinking Helps Problem Solving ")'
                                                src="../../images/footer/Linkedin.png" alt="social media" />
                                        </a>
                                    </ul>
                                </div>
                                <hr />
                            </div>


                        </div>
                        <?php require '../latest-blogs.php'; ?>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>