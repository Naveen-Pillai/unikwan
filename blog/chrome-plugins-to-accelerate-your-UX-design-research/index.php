<!DOCTYPE html>
<html lang="en">

<head>
    <title>5 Google Chrome Extension For Designers [2020 Update]</title>
    <meta name="description"
        content="In this article, we reveal the 5 best chrome extensions that are useful for every designer in 2020. Find out more below." />
    <meta name="keywords" content="Google Chrome Extension">
    <meta property="og:image" content="../../images/blog/articles/chrome.jpg" />
    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section">
                <div style='height:40px'></div>

                <div class="container">
                    <div class="">
                        <div class="">
                            <div class='blog-margin-holler'>
                                <div class="postsingle">
                                    <div class="section-heading" style='padding-bottom:0'>
                                        <a href='../../blog' style='text-decoration:none'>
                                            <div class="description"><i></i>Blog</div>
                                        </a>
                                        <h1 class="postsingle__title">
                                            5 Google Chrome Extension For Designers [2020 Update]
                                        </h1>
                                    </div>
                                    <p>
                                        It is the bane of any UX designer to plow hours at a time through articles,
                                        references, competitors, to create clarity on any project .
                                        While there is no replacement for good legwork and research, there are
                                        some helpful tools that could make the process a bit more efficient or
                                        faster.
                                    </p>

                                    <div class='blog_autohr_holder'>
                                        <div class='blog_avatar_holder'>
                                            <img class='blog_img_avatar' width='30px' src="../../images/favicon.png"
                                                alt="Importance of Branding" />
                                            <div>
                                                <article class='blog_author_text'><strong>UniKwan</strong></article>
                                                <article class='blog_author_text'>2nd January, 2020 · 4 min read
                                                </article>
                                            </div>
                                        </div>
                                        <div class="">
                                            <ul class="social-icon">
                                                <a class="share_icons_margin"
                                                    href="https://www.linkedin.com/company/unikwan-innovations-private-limited"
                                                    target='_blank'>
                                                    <img src="../../images/footer/Linkedin.png" alt="social media" />
                                                </a>
                                                <a class="share_icons_margin"
                                                    href="https://www.behance.net/unikwaninnovations" target='_blank'>
                                                    <img src="../../images/footer/Behance.png" alt="social media" />
                                                </a>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="postsingle__img">
                                <picture>
                                    <source srcset="../../webp/blog/articles/chrome.webp" type="image/webp">
                                    <img src="../../images/blog/articles/chrome.jpg"
                                        alt="Google Chrome Extension For Designers" />
                                </picture>

                            </div>
                            <div class='blog-margin-holler'>
                                <br>


                                <p>Here are a few simple practical Google Chrome plugins that I have found to
                                    help a lot.
                                </p>
                                <p style='font-weight:bold;color:black;cursor:pointer;font-size:24px;margin-top:25px;line-height:1.3;'
                                    onClick="Gotolink('https://chrome.google.com/webstore/detail/whatfont/jabopobgcpjmedljpbcaablpmlmfcogm')">
                                    What font</p>
                                <p>Often while we shuffle through web pages, we try to figure out the font used
                                    in the page to make more sense of the design. A good font pairing is
                                    always a good note to make even if we don't use it right away. Sure, we
                                    can go to inspect the element and check the font. But, what font makes it a
                                    breeze. Hover over any element after turning it on, and the font , colour,
                                    size, spacing, even the service that's used to serve the font if any, will pop
                                    up on your screen. It is pretty neat.</p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/chrometools/image1.webp"
                                            type="image/webp">
                                        <img src="../../images/blog/articles/chrometools/image1.png"
                                            alt="Screenshot of What Font Chrome Extension" />
                                    </picture>

                                </div>
                                <br>
                                <p style='font-weight:bold;color:black;cursor:pointer;font-size:24px;margin-top:25px;line-height:1.3;'
                                    onClick="Gotolink('https://chrome.google.com/webstore/detail/session-buddy/edacconmaakjimmfgnblocblbcdcpbko')">
                                    Session Buddy</p>
                                <p>This plugin is going to be your best bud if you are one of those people who
                                    get to a thousand chrome tabs from a few by opening new links, looking up
                                    new topics. After 84 chrome tabs and a few hours browsing, it is a terrifying
                                    thought to lose all those tabs, but sometimes it is hard to continue after you
                                    feel saturated as well. Cue music, here comes Session buddy. Session
                                    buddy will let you save your chrome tabs grouped according to your
                                    preference, in a few simple clicks. It will show all your open tabs, and then
                                    you could name it as a particular session and save it so that you can open
                                    it the next day with a fresh mind</p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/chrometools/image2.webp"
                                            type="image/webp">
                                        <img src="../../images/blog/articles/chrometools/image2.png"
                                            alt="	Screenshot of *name of extension*" />
                                    </picture>

                                </div>
                                <br>
                                <p style='font-weight:bold;color:black;cursor:pointer;font-size:24px;margin-top:25px;line-height:1.3;'
                                    onClick="Gotolink('https://chrome.google.com/webstore/detail/gofullpage-full-page-scre/fdpohaocaechififmbbbbbknoalclacl')">
                                    GoFullPage</p>
                                <p>When you are creating moodboards and stacking references for a project, it
                                    is imperative that you take a lot of screenshots of particular web pages. It is
                                    not always the most glamorous thing to do but it is indeed necessary. And
                                    none of us wants to go back to taking multiple screenshots and assembling
                                    them manually to form a full page. ‘GofullPage’, like the name suggests, is
                                    one plugin that helps you with this.</p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/chrometools/image3.webp"
                                            type="image/webp">
                                        <img src="../../images/blog/articles/chrometools/image3.png"
                                            alt="Screenshot of What Font Chrome Extension " />
                                    </picture>

                                </div>
                                <br>
                                <p style='font-weight:bold;color:black;cursor:pointer;font-size:24px;margin-top:25px;line-height:1.3;'
                                    onClick="Gotolink('https://chrome.google.com/webstore/detail/image-downloader/cnpniohnfphhjihaiiggeabnkjhpaldj')">
                                    Image downloader</p>
                                <p>While creating high fidelity wireframes or , mockups or even a prototype to
                                    demo an interaction, you might need a few image assets to convincingly
                                    sell the whole design. And it is always a pain to open an image in a new tab
                                    or download or copy to your workspace. Especially when you have to do it
                                    one by one. This image downloader, is a rudimentary looking chrome
                                    plugin but that does its job very well. You can open this plugin from any
                                    webpage, and it will show you all the image assets loaded in that particular
                                    webpage.You can select all the ones you want and download all together in
                                    a single click.</p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/chrometools/image4.webp"
                                            type="image/webp">
                                        <img src="../../images/blog/articles/chrometools/image4.png"
                                            alt="	Google Chrome Extension For Designers" />
                                    </picture>

                                </div>
                                <br>
                                <p style='font-weight:bold;color:black;cursor:pointer;font-size:24px;margin-top:25px;line-height:1.3;'
                                    onClick="Gotolink('https://chrome.google.com/webstore/detail/google-dictionary-by-goog/mgijmajocgfcbeboacabfgobmjgjcoja')">
                                    Google Dictionary</p>
                                <p>This is one of those mind bendingly useful plugin that I have rarely seen
                                    people use in their browsers. Doubleclick any word from any article and the
                                    definition pops up on a tooltip with its pronunciation audio. You can always
                                    manually search as well. While you are in one of your late night binge
                                    reading of articles, this plugin will help you a lot.</p>

                                <p>
                                    Here is one tweet from
                                    Sasi Tharoor, perfect to test the plugin
                                    "Exasperating farrago of distortions, misrepresentations &
                                    outright lies
                                    being broadcast by an unprincipled showman masquerading as a
                                    journalist"
                                </p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/chrometools/image5.webp"
                                            type="image/webp">
                                        <img src="../../images/blog/articles/chrometools/image5.png"
                                            alt="Screenshot of *name of extension*" />
                                    </picture>

                                </div>
                                <br>
                                <p style='font-weight:bold;color:black;cursor:pointer;font-size:24px;margin-top:25px;line-height:1.3;'
                                    onClick="Gotolink('https://chrome.google.com/webstore/detail/requestly-redirect-url-mo/mdnleldcmiljblolnjhpnblkcekpdkpa')">
                                    Bonus : Requestly</p>
                                <p>This is borderline unethical if you have the means to pay. But if you are a
                                    freshly hatched designer hungry for knowledge and information, Medium is
                                    a good source for a lot of good articles on improving your craft. But the
                                    problem is, many of such articles are behind a paywall. You can use this
                                    plugin to make it look like your traffic to medium is from twitter. And guess
                                    what, twitter traffic doesn’t have paywall. Set it up once and forget it. It
                                    works for now, till they remove it. So, go crazy till then. <span
                                        style='cursor:pointer;color:black'
                                        onClick="Gotolink('https://miro.medium.com/max/1000/1*dheUob0j4VvZLAUgjeNLzg.gif')">Here</span>
                                    is how to set it
                                    up . Full article</p>
                                <br>
                                <div class="postsingle__img">
                                    <picture>
                                        <source srcset="../../webp/blog/articles/chrometools/image6.webp"
                                            type="image/webp">
                                        <img src="../../images/blog/articles/chrometools/image6.png"
                                            alt="Google Chrome Extension For Designers" />
                                    </picture>

                                </div>
                                <br>
                                <p>Try these plugins out and see if they go with your style of research. They
                                    have helped me save a lot of time over the years.</p>
                                <br>
                                <div class="social-icon-wrapper">
                                    <div class="social-caption">
                                        Share:
                                    </div>
                                    <ul class="social-icon">
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("fb","5 Google Chrome Extension For Designers [2020 Update]")'
                                                src="../../images/footer/facebook.png" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("wapp","5 Google Chrome Extension For Designers [2020 Update]")'
                                                src="../../images/footer/whatsapp.svg" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("tweet","5 Google Chrome Extension For Designers [2020 Update]")'
                                                src="../../images/footer/twitter.png" alt="social media" />
                                        </a>
                                        <a class="share_icons_margin">
                                            <img onclick='SocialLinks("li","5 Google Chrome Extension For Designers [2020 Update]")'
                                                src="../../images/footer/Linkedin.png" alt="social media" />
                                        </a>
                                    </ul>
                                </div>
                                <hr>
                            </div>
                        </div>


                    </div>
                    <?php require '../latest-blogs.php'; ?>
                </div>
        </div>
        </section>
        <!-- end section -->
        <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>