<!DOCTYPE html>
<html lang="en">

<head>
    <title>3 Nifty Things To Do When Publishing Your Website</title>
    <meta name="description"
        content="Ready to publish your website but have you gone through an important checklist on hand? Check out our article and learn things to do when publishing your website." />
    <meta name="keywords"
        content="website, publishing your website, publish, tag manager, tips to publish your website, improve your google analytics">
    <meta property="og:image" content="../../images/blog/articles/nifty.jpg" />
    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section">
                <div style='height:40px'></div>
                <div class="container">
                    <div class=" ">
                        <div class=" ">
                            <div class="postsingle">
                                <div class='blog-margin-holler'>
                                    <div class="section-heading" style='padding-bottom:0'>
                                        <a href='../../blog' style='text-decoration:none'>
                                            <div class="description"><i></i>Blog</div>
                                        </a>
                                        <h1 class="postsingle__title">
                                            3 Nifty Things To Do When You Are Publishing Your Website

                                        </h1>
                                    </div>

                                    <p>
                                        Publishing a website is certainly easy but doing it for a purpose and making
                                        sure that purpose is met is a bit more difficult business altogether.


                                    </p>
                                    <div class='blog_autohr_holder'>
                                        <div class='blog_avatar_holder'>
                                            <img class='blog_img_avatar' style='height:38px' width='38px'
                                                src="../../images/blog/ritwik.png" alt="Importance of Branding" />
                                            <div>
                                                <article class='blog_author_text'><strong>Ritwik SB</strong>
                                                </article>
                                                <article class='blog_author_text'>15th Jan, 2021 · 8 min read</article>
                                            </div>
                                        </div>
                                        <div class="">
                                            <ul class="social-icon">
                                                <a class="share_icons_margin"
                                                    href="https://www.linkedin.com/in/ritwik-s-b-99285476"
                                                    target='_blank'>
                                                    <img src="../../images/footer/Linkedin.png" alt="social media" />
                                                </a>

                                            </ul>
                                        </div>
                                    </div>
                                </div>



                                <div class="postsingle__img">

                                    <img src="../../images/blog/articles/nifty.jpg"
                                        alt="Discussing three important things to make website more effective and useful" />

                                </div>

                                <br>
                                <div class='blog-margin-holler'>
                                    <p>There are a gazillion websites in the world according to Wikipedia (It is a real
                                        number, don't fact check that) They have all been published. Which makes it
                                        appear like the easiest thing to do in the world, which is not the case. Well,
                                        publishing a website is certainly easy but doing it for a purpose and making
                                        sure that purpose is met is a bit more difficult business altogether. While
                                        there are practically infinite resources on doing that business better, it is
                                        often easy to overlook some of the simplest (but powerful) things around. Here
                                        are a few things I found useful while trying to make a website work. It is not a
                                        complete list but it will certainly make your life a little bit easier.

                                    </p>
                                    <h2
                                        style='font-weight:bold;padding: 0;font-size:24px;margin-top:25px;line-height:1.3;'>
                                        Analytics
                                    </h2>

                                    <div class="postsingle__img" style='margin:24px 0;'>
                                        <picture>
                                            <source srcset="../../webp/blog/articles/nifty/1.webp" type="image/webp">
                                            <img src="../../images/blog/articles/nifty/1.png"
                                                alt="	Screenshot of *name of extension*" />
                                        </picture>

                                    </div>
                                    <p>“<i>Is this guy going to advise people about setting analytics on their
                                            website?</i> ” I’m not. There is plenty of material online to cover that
                                        part
                                        if anything is amiss. I will share two tips that could improve your <a
                                            href='https://analytics.google.com/' target='_blank'>Google
                                            analytics</a> game. If you already knew this, then good for you. If not,
                                        here we go.
                                    </p>

                                    <p>
                                    <ol style='margin-left:5%'>
                                        <li>
                                            <p> <strong>Using views properly.</strong><br />
                                                Views are basically ‘the data’ from your property analytics viewed in a
                                                particular way. Imagine your data as a raw photo, and the view is an
                                                Instagram post with or without any filter added. All views have all the
                                                data
                                                from your property by default (#nofilter ). But you could modify views
                                                in a
                                                way that helps you make sense of the said data. You could set up filters
                                                for
                                                certain data and filter out unnecessary information, or set up goals and
                                                see
                                                if you achieved them. So the question is how do you use these views to
                                                your
                                                benefit.
                                            </p>
                                            <p>
                                            <ul class='list-marker' style='margin-left:5%'>
                                                <p>
                                                    <li><strong>Use a master view </strong><br />


                                                        This will be the unadulterated version of your data. No filters,
                                                        no
                                                        modifications. Never touch this view as you will need to access
                                                        the
                                                        raw data from time to time. If you go ahead and play with all
                                                        the
                                                        incoming data without any backups, it will become a problem
                                                        later
                                                        on.

                                                    </li>
                                                </p>
                                                <p>
                                                    <li><strong>Use a test view</strong><br />


                                                        This will be your canary in the tunnel. Always test your
                                                        modifications, filters, and goals here first and see if it works
                                                        properly. The data will get mangled up here, but that is the
                                                        purpose. You will test your analytical tools like you test your
                                                        culinary skills on your family on a cursed Sunday (for them).
                                                        Google
                                                        Analytics doesn't let you undo the filters that have been run on
                                                        a
                                                        view. Filtered Out data is lost so, don't forget to do this.

                                                    </li>
                                                </p>
                                            </ul>
                                            </p>
                                        </li>
                                        <li>
                                            <p><strong>Filtering out your comrades.</strong><br />
                                                If you work with a group of people who are also working on the same
                                                product
                                                as you, it is better to filter out their activity from your data, or you
                                                will get confused with that ‘heavy conversion rate from a particular
                                                apartment complex in Bengaluru’. If you used to work in an office
                                                together,
                                                you could have filtered traffic out like <a
                                                    href='https://support.google.com/analytics/answer/1034840?hl=en'
                                                    target='_blank'>‘here’</a>.
                                                But filtering out
                                                internal
                                                traffic is not that easy (thanks COVID) since most of us are working
                                                remotely from random places. So another good way is to use a browser
                                                extension that does not record your analytics activity. You can download
                                                it
                                                from <a href='https://tools.google.com/dlpage/gaoptout'
                                                    target='_blank'>here</a>.
                                                It is available for Microsoft Internet Explorer 11, Google
                                                Chrome, Mozilla Firefox, Apple Safari, and Opera. If you use a browser,
                                                not
                                                on this list, maybe you should switch?
                                            </p>
                                        </li>

                                    </ol>
                                    </p>
                                    <h2
                                        style='font-weight:bold;padding: 0;font-size:24px;margin-top:25px;line-height:1.3;'>
                                        Setting Up Pixels
                                    </h2>

                                    <div class="postsingle__img" style='margin:24px 0;'>
                                        <picture>
                                            <source srcset="../../webp/blog/articles/nifty/2.webp" type="image/webp">
                                            <img src="../../images/blog/articles/nifty/2.png"
                                                alt="	Screenshot of *name of extension*" />
                                        </picture>

                                    </div>
                                    <p> <a href='https://tools.google.com/dlpage/gaoptout' target='_blank'>Social media
                                            pixels</a> are a
                                        powerful tool in your
                                        hand with your website. (If you are a person concerned about privacy and how it
                                        is used and all kinds of certain things, I'm with you. Maybe use this
                                        responsibly, or don't use it at all. I’m just writing a note on what's out there
                                        man. Leave me alone.) help you to track the visitors of your site in their
                                        respective social media sites, without revealing their details of course. So
                                        what is a pixel? Pixel is a line of code from a social media platform, (Just
                                        like the analytics code that you have installed), that goes into the header of
                                        your website and tracks the user and keeps tabs on them. It is very easy to
                                        install and it boosts your digital marketing efforts if you know how to use it
                                        properly. <strong> Here are a couple of examples of the use of pixels:</strong>
                                    </p>
                                    <p>
                                    <ol style='margin-left:5%'>
                                        <li>
                                            <p> <strong>Advertise to your visitors. </strong><br />
                                                If your visitors are left without making any meaningful conversion, how
                                                do you get them back? What if you could advertise to them the next day
                                                and show them why they are missing out or why you are awesome. Pixels
                                                let you do that.
                                            </p>
                                        </li>
                                        <li>
                                            <p><strong>Find similar audiences. </strong><br />
                                                Pixel lets you find users similar to the ones that visited your site.
                                                Say you sell handmade leather shoes. And a few people have visited and
                                                left. A Pixel could help you target users who have similar tastes to the
                                                people who visited. Or if you are a small website development company
                                                and your targets were other SMEs who would need websites, you could
                                                target meaningfully based on the existing visitors. How cool is that!
                                            </p>
                                        </li>

                                    </ol>
                                    </p>
                                    <p>If you are in the B2C domain, Facebook is your best bet to use the pixel to your
                                        advantage. If you are in the B2B sector, even if you use FB, use LinkedIn
                                        because you will be able to get a lot more information usually FB couldn’t
                                        provide. </p>
                                    <h2
                                        style='font-weight:bold;padding: 0;font-size:24px;margin-top:25px;line-height:1.3;'>
                                        Using Google Tag Manager
                                    </h2>

                                    <div class="postsingle__img" style='margin:24px 0;'>
                                        <picture>
                                            <source srcset="../../webp/blog/articles/nifty/3.webp" type="image/webp">
                                            <img src="../../images/blog/articles/nifty/3.png"
                                                alt="	Screenshot of *name of extension*" />
                                        </picture>

                                    </div>
                                    <p>If you are a product manager or a product owner who likes to experiment a lot
                                        with your site (as you should), one thing you would usually hate is asking your
                                        engineer/website guy to constantly make changes and publish it. Even if you are
                                        doing it yourself, it is a headache to do it all the time and keep track of all
                                        the snippets of codes you added and changed. <a
                                            href='https://marketingplatform.google.com/about/tag-manager/'
                                            target='_blank'>Google tag
                                            manager</a> is an
                                        agile tool
                                        that you could use to easily customize your
                                        site without getting your hand in the code. (Except for maybe when you have to
                                        install this tag manager)
                                    </p>
                                    <p>
                                        <strong>Here are a few useful ways: </strong><br />
                                    <ol style='margin-left:5%'>
                                        <li>
                                            <p> <strong>Adding tracking scripts without editing the code directly:
                                                </strong><br />
                                                Tag manager helps you set up codes in the header of your site in the
                                                easiest way possible. It already has set up almost all the major
                                                players' code ready on the go, so that you will only have to use the ID
                                                of your said code in the tag manager. Or if you are into pasting the
                                                whole code, you can do that as well.
                                            </p>
                                        </li>
                                        <li>
                                            <p><strong>Triggering events: </strong><br />
                                                If you are using Google analytics, then one of the meaningful ways of
                                                using the GA is by creating events on the site and tracking it. It is
                                                always a pain to get the event created if your engineer/website guy is
                                                loaded with other ‘important work’. Well, the tag manager helps you
                                                create events on the site and trigger it according to your criteria
                                                without directly messing with the website code. This will start
                                                appearing in your GA right away.
                                            </p>
                                        </li>
                                        <li>
                                            <p><strong> Creating Event Listener’s: </strong><br />
                                                Is another versatile way of using a Tag manager. It helps you trigger
                                                events based on the chosen criteria from a set of predefined types of
                                                criteria. You could create ‘button click’ events or ‘link click’
                                                listeners or form submit listener or javascript error listener to
                                                trigger events. You could define event parameters and then make it sent
                                                to your Google Analytics.
                                            </p>
                                        </li>

                                    </ol>
                                    </p>
                                    <p>The tag manager also lets you define any of these conditions specific to pages or
                                        other conditions you could define. You can even modify style elements (for short
                                        term quick results. Not a permanent solution) to your liking if you want to try
                                        something. Modifying your site and creating a complex tracking infrastructure
                                        without affecting your ‘website guy’s’ mental health? I’d say it is worth it.
                                    </p>
                                    <h2
                                        style='font-weight:bold;padding: 0;font-size:24px;margin-top:25px;line-height:1.3;'>
                                        Bonus: Getting an SSL/TLS certificate
                                    </h2>

                                    <div class="postsingle__img" style='margin:24px 0;'>
                                        <picture>
                                            <source srcset="../../webp/blog/articles/nifty/4.webp" type="image/webp">
                                            <img src="../../images/blog/articles/nifty/4.png"
                                                alt="	Screenshot of *name of extension*" />
                                        </picture>

                                    </div>
                                    <p> <a href='https://www.cloudflare.com/learning/ssl/how-does-ssl-work/'
                                            target='_blank'>SSL/TLS
                                            certificates</a> certify your sites on
                                        Making connections to your websites secure is more important than ever. It
                                        protects your user and yourself from all sorts of threats that get increasingly
                                        sophisticated every day. You can get an SSL certificate most usually from the
                                        service you bought your domain name from. Or any other service provider on the
                                        market. It doesn't cost much but the added advantage is that it could get your
                                        site increased preference since all the major search engines prefer sites with
                                        strong security and appropriate certification.
                                    </p>
                                    <p>If you are not inclined to pay for this, then you could always avail a
                                        certificate from Letsencrypt.org, a non-profit organization that provides TLS
                                        certificates (TLS is the more jacked nephew of uncle SSL) free of charge. The
                                        renewal period of these certificates are a bit shorter than the paid versions in
                                        the market, but I’m sure you can handle it if you are into free stuff. </p>
                                    <p>It is worth noting that Let’s encrypt lost a little bit of their mojo when a
                                        report came in about how phishing websites misused their service to get
                                        certificates for their paypal phishing operations. They rescinded those
                                        certificates later but that revitalised the debate on free certificates and free
                                        certificate authorities. Free CAs are a hot debate and choose your side wisely.
                                        Either side, make sure that you get your certificate because it could mean more
                                        traffic to your website. </p>
                                    <p>So that’s it. From this list you must have noticed one thing. All of these tools
                                        are for tracking, testing or modifying the website and it’s traffic. Like I said
                                        in the beginning, publishing a website is one of the easiest things in the
                                        world. But if you are to use it effectively, you will have to constantly test
                                        observe and modify it as you go. Even if you are not going to use any of these
                                        tools, which I highly recommend that you do, make sure that you do those three
                                        things. Test, observe, and modify. </p>




                                    <div class="social-icon-wrapper">
                                        <div class="social-caption">
                                            Share:
                                        </div>
                                        <ul class="social-icon">
                                            <a class="share_icons_margin">
                                                <img onclick='SocialLinks("fb","Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet")'
                                                    src="../../images/footer/facebook.png" alt="social media" />
                                            </a>
                                            <a class="share_icons_margin">
                                                <img onclick='SocialLinks("wapp","Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet")'
                                                    src="../../images/footer/whatsapp.svg" alt="social media" />
                                            </a>
                                            <a class="share_icons_margin">
                                                <img onclick='SocialLinks("tweet","Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet")'
                                                    src="../../images/footer/twitter.png" alt="social media" />
                                            </a>
                                            <a class="share_icons_margin">
                                                <img onclick='SocialLinks("li","Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet")'
                                                    src="../../images/footer/Linkedin.png" alt="social media" />
                                            </a>
                                        </ul>
                                    </div>



                                    <hr />

                                </div>

                            </div>


                        </div>
                        <?php require '../latest-blogs.php'; ?>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>