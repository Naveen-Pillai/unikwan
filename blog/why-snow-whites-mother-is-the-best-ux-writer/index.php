<!DOCTYPE html>
<html lang="en">

<head>
    <title>Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet</title>
    <meta name="description"
        content="Did you know your brand voice and tone can make or break a product while marketing your brand. Learn why it is important to map the emotional state of the users and how to keep your brand consistent. " />
    <meta name="keywords"
        content="marketing your brand, build your story brand, company's tone of voice, help with branding your business, emotional state of the users, key features of good UX writing, brand consistent">
    <meta property="og:image" content="../../images/blog/articles/snowwhite.jpg" />
    <?php require '../../elements/headerinnersub.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../../elements/navbarinnersub.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="blog_subpage-header__bg">
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section">
                <div style='height:40px'></div>
                <div class="container">
                    <div class=" ">
                        <div class=" ">
                            <div class="postsingle">
                                <div class='blog-margin-holler'>
                                    <div class="section-heading" style='padding-bottom:0'>
                                        <a href='../../blog' style='text-decoration:none'>
                                            <div class="description"><i></i>Blog</div>
                                        </a>
                                        <h1 class="postsingle__title">
                                            Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet

                                        </h1>
                                    </div>

                                    <p>The tone and voice are essential to build a brand and engage users. Let's examine
                                        how
                                        Snow White's Stepmother can help to define the brand's voice and tone.


                                    </p>
                                    <div class='blog_autohr_holder'>
                                        <div class='blog_avatar_holder'>
                                            <img class='blog_img_avatar' width='30px' src="../../images/favicon.png"
                                                alt="Importance of Branding" />
                                            <div>
                                                <article class='blog_author_text'><strong>Samrat Sharma</strong>
                                                </article>
                                                <article class='blog_author_text'>23rd Nov, 2020 · 3 min read</article>
                                            </div>
                                        </div>
                                        <div class="">
                                            <ul class="social-icon">
                                                <a class="share_icons_margin"
                                                    href="https://www.linkedin.com/in/samrat-sharma-86817944/"
                                                    target='_blank'>
                                                    <img src="../../images/footer/Linkedin.png" alt="social media" />
                                                </a>

                                            </ul>
                                        </div>
                                    </div>
                                </div>



                                <div class="postsingle__img">

                                    <img src="../../images/blog/articles/snowwhite.jpg" alt="Marketing Your Brand" />

                                </div>

                                <br>
                                <div class='blog-margin-holler'>
                                    <p>If I were to ask you to name few Disney fairytale protagonists; there is a good
                                        chance that one of the names will be Snow White. It shouldn’t surprise anyone
                                        that
                                        Snow White is a famous character because she is fierce, a survivor and she can
                                        ‘talk
                                        to animals’- the princess has major clout. However, she does lack the means to
                                        be an
                                        excellent UX writer. The key features of good UX writing such as ‘voice’ and
                                        ‘tone’
                                        is not understood by her. On the other hand, her evil stepmother is a woman of
                                        many
                                        talents.

                                    </p>
                                    <p>Let’s pretend that Snow White’s story does not have an ending- say we stop at
                                        Snow
                                        White taking the apple from the old woman (Stepmother in disguise). You will
                                        notice
                                        that Snow White is not able to understand nuance and context behind the old
                                        woman
                                        selling the apple. If voice and tone is not understood properly, you will not be
                                        able to sell anything, even the ‘Hope Diamond’. The question which comes up-
                                        what is
                                        voice and tone and why it is important to build your story brand?</p>
                                    <div class="blogquote_boxhold">
                                        <div class="blogquote_sidebar"></div>
                                        <article class="blog_quoite_texting">“Rhiannon Jones, a content designer, cites
                                            that every brand is a two way
                                            communication. It is easy to look at brands as a dialogue between the
                                            product
                                            and user. ” </article>
                                    </div>
                                    <p>Therefore, it is important for the brand to have a cohesive and uniform voice
                                        throughout its narrative. Voice can be defined as who you are which never
                                        changes.
                                        The only thing which changes is the tone. The changes in tone allows for the
                                        product
                                        to adapt to diﬀerent scenarios. Think of the stepmother- observe what she sells
                                        and
                                        how she sells.

                                    </p>
                                    <p>Unlike the Disney film, after the failed attempt of the huntsman, the evil
                                        stepmother
                                        tries to kill Snow White not once but three times. First, she ties a corset too
                                        tight making her pass out; second time she sells her a poisonous comb- both
                                        times
                                        the dwarfs are able to save Snow White. Despite repeated warnings from the
                                        dwarfs,
                                        Snow eats the red glossy poisonous apple and collapses. After a careful reading
                                        of
                                        the story, the reader sees how the stepmother transforms herself into an old
                                        woman
                                        with a bent back.

                                    </p>
                                    <p>It is interesting to see how vulnerability and empathy convinces Snow White to
                                        buy
                                        tools of her own murder. When selling any product it is important to map the
                                        emotional state of the users (angry, confused, neutral etc.) and create a
                                        corresponding tone (warm, empathetic, friendly, straight forward, pleasant
                                        etc.).
                                        The voice or identity of the stepmother never changes- which is sinister and
                                        evil.
                                        As herself the stepmother knows that Snow White will not meet her. </p>
                                    <p>Therefore, what she changes is the tone- through her appearance as an old woman,
                                        the
                                        stepmother comes across as an empathetic, defenseless and unfortunate
                                        individual.
                                        This allows Snow White to change her attitude towards the old woman, let down
                                        her
                                        guard and accepting tools which she doesn’t need.
                                    </p>
                                    <p>Voice and tone can make or break a product while marketing your brand. While the
                                        Stepmother never changes her brand of evil, she can change her tone to make it
                                        more
                                        client and user friendly. If you want to become a good UX writer, realize the
                                        voice
                                        and tone of the copy. You can learn much from villains of fairytales as they are
                                        charismatic, adaptable and witty. Better yet, hire Snow White’s stepmother for
                                        your
                                        product as she can sell anything.
                                    </p>


                                    <div class="social-icon-wrapper">
                                        <div class="social-caption">
                                            Share:
                                        </div>
                                        <ul class="social-icon">
                                            <a class="share_icons_margin">
                                                <img onclick='SocialLinks("fb","Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet")'
                                                    src="../../images/footer/facebook.png" alt="social media" />
                                            </a>
                                            <a class="share_icons_margin">
                                                <img onclick='SocialLinks("wapp","Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet")'
                                                    src="../../images/footer/whatsapp.svg" alt="social media" />
                                            </a>
                                            <a class="share_icons_margin">
                                                <img onclick='SocialLinks("tweet","Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet")'
                                                    src="../../images/footer/twitter.png" alt="social media" />
                                            </a>
                                            <a class="share_icons_margin">
                                                <img onclick='SocialLinks("li","Why Snow White’s Mother Is The Best UX Writer You Will Ever Meet")'
                                                    src="../../images/footer/Linkedin.png" alt="social media" />
                                            </a>
                                        </ul>
                                    </div>



                                    <hr />

                                </div>

                            </div>


                        </div>
                        <?php require '../latest-blogs.php'; ?>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <?php require '../../elements/footerinnersub.php'; ?>
        </div>
    </main>
    <?php require '../../elements/svgcodeinnersub.php'; ?>
</body>

</html>