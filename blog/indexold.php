<!DOCTYPE html>
<html lang="en">

<head>
    <title>Blog & Insights | Unikwan Innovations | Design Agency In Bangalore</title>
    <meta name="description"
        content="Get your daily dose of UX design, user research, user experience strategy, interaction design, and design thinking stories. " />

    <?php require '../elements/headerinner.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../elements/navbarinner.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="">
                    <div class="subpage-header__bg">
                        <div class="container">
                            <div class="subpage-header__block">
                                <h1 class="subpage-header__title">Blog</h1>
                                <div class="subpage-header__line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section section-default-top">
                <div class="container">
                    <div class="listing-post">
                        <div class="postaside-box__title">
                            <h2 style='font-weight:normal'>Featured Blogs</h2>
                        </div>
                        <div class="post">
                            <div>
                                <div class=""><a href="award-from-clutch-2020">
                                        <picture>
                                            <source srcset="../webp/blog/list/featured/clutch2020_2.webp"
                                                type="image/webp">
                                            <img src="../images/blog/list/featured/clutch2020_2.jpg" alt="unikwan_blog">
                                        </picture>

                                    </a></div>
                                <br>
                                <div class="post-col-description">
                                    <div class="post__description">
                                        <h2 class="post__title"><a href="award-from-clutch-2020">UniKwan Receives
                                                Prestigious UI/UX Design Award from…</a></h2>
                                        <p>
                                            We have exciting news to announce! UniKwan, one of Bengaluru’s leading
                                            creative design agencies, has been recognized by…
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="postaside-box__title" style='margin-top:60px'>
                            <h2 style='font-weight:normal'>Latest Blogs</h2>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="why-snow-whites-mother-is-the-best-ux-writer">

                                        <img src="../images/blog/list/snowwhite.jpg" alt="unikwan_blog">

                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="why-snow-whites-mother-is-the-best-ux-writer">Why
                                            Snow White’s Mother Is
                                            The Best UX Writer You Will Ever Meet </a></h2>
                                    <p>
                                        The tone and voice are essential to build a brand and engage users. Let's
                                        examine how Snow White's Stepmother can help to define the brand's voice and
                                        tone.

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="is-branding-important">
                                        <picture>
                                            <source srcset="../webp/blog/list/branding.webp" type="image/webp">
                                            <img src="../images/blog/list/branding.jpg" alt="unikwan_blog">
                                        </picture>

                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="is-branding-important">Why is Branding Important to
                                            your company?</a></h2>
                                    <p>
                                        When you think of Raymond what is the first image that strikes you? When you
                                        think of Amazon,…
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="5-best-ui-ux-trends">
                                        <picture>
                                            <source srcset="../webp/blog/list/uitrends.webp" type="image/webp">
                                            <img src="../images/blog/list/uitrends.jpg" alt="unikwan_blog">
                                        </picture>
                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="5-best-ui-ux-trends">5 UI/UX Trends to watch out in
                                            2020 </a></h2>
                                    <p>
                                        Here is a list of UI and UX trends that we feel designers at design
                                        consultancies and digital…
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="design-thinking">
                                        <picture>
                                            <source srcset="../webp/blog/list/Designthinking.webp" type="image/webp">
                                            <img src="../images/blog/list/Designthinking.jpg" alt="unikwan_blog">
                                        </picture>

                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="design-thinking">Using Design Thinking as a
                                            Strategy for Innovation </a></h2>
                                    <p>
                                        Using Design Thinking As A Strategy For Innovation There are clear distinctions
                                        between design thinking and design. If…
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="banglore-based-top-design-agency">
                                        <picture>
                                            <source srcset="../webp/blog/list/unikwan.webp" type="image/webp">
                                            <img src="../images/blog/list/unikwan.jpg" alt="unikwan_blog">
                                        </picture>

                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="banglore-based-top-design-agency">Bangalore based
                                            top design agency creating a…</a></h2>
                                    <p>
                                        A top Bangalore based design consultancy agency that is creating a better future
                                        using design thinking. “We anticipate…
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a
                                        href="5-reasons-why-you-need-to-hire-a-design-team-for-your-company">

                                        <picture>
                                            <source srcset="../webp/blog/list/hire.webp" type="image/webp">
                                            <img src="../images/blog/list/hire.jpg" alt="unikwan_blog">
                                        </picture>
                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a
                                            href="5-reasons-why-you-need-to-hire-a-design-team-for-your-company">5
                                            Reasons Why You Should Hire A Design Team...</a></h2>
                                    <p>
                                        Every company somewhere started off with a passion to bring a change in society
                                        with their profound ideas.…
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="award-from-clutch-2019">
                                        <picture>
                                            <source srcset="../webp/blog/list/award2019.webp" type="image/webp">
                                            <img src="../images/blog/list/award2019.jpg" alt="unikwan_blog">
                                        </picture>

                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="award-from-clutch-2019"> Top Creative & Design
                                            agency award from ...</a></h2>
                                    <p>
                                        UniKwan is a Top 2019 Design Agency on Clutch. UniKwan is excited to announce
                                        that we have been…
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="What-is-design-for-problem-solving">
                                        <picture>
                                            <source srcset="../webp/blog/list/what_is_design.webp" type="image/webp">
                                            <img src="../images/blog/list/what_is_design.jpg" alt="unikwan_blog">
                                        </picture>

                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="What-is-design-for-problem-solving">What is Design-
                                            design for problem solving</a></h2>
                                    <p>
                                        Understanding the common misunderstandings of what we know about design.
                                        Redefining how design thinking helps problem solving. In…
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="User-Research-and-the-benefits">
                                        <picture>
                                            <source srcset="../webp/blog/list/uxresearch.webp" type="image/webp">
                                            <img src="../images/blog/list/uxresearch.jpg" alt="unikwan_blog">
                                        </picture>

                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="User-Research-and-the-benefits">Benefits of User
                                            Research in Design </a></h2>
                                    <p>
                                        The term User Research and User Testing is not glamourised enough in the
                                        industry ecosystem to engage it…
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="chrome-plugins-to-accelerate-your-UX-design-research">

                                        <picture>
                                            <source srcset="../webp/blog/list/chrome.webp" type="image/webp">
                                            <img src="../images/blog/list/chrome.jpg" alt="unikwan_blog">
                                        </picture>
                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a
                                            href="chrome-plugins-to-accelerate-your-UX-design-research">5 Google Chrome
                                            Extension For Designers [2020 Update]
                                        </a></h2>
                                    <p>
                                        It is the bane of any UX designer to plow hours at a time through articles,
                                        references, competitors, to create clarity on any project .
                                        While there is no replacement for good legwork and research,
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="post">
                            <div class="post-col-description">
                                <div class="post__img"><a href="How-to-make-your-UI-design-CRAP">

                                        <picture>
                                            <source srcset="../webp/blog/list/crap.webp" type="image/webp">
                                            <img src="../images/blog/list/crap.jpg" alt="unikwan_blog">
                                        </picture>
                                    </a></div>
                                <div class="post__description">
                                    <h2 class="post__title"><a href="How-to-make-your-UI-design-CRAP">How to make your
                                            UI design CRAP

                                        </a></h2>
                                    <p>
                                        Maybe you have already heard of it, maybe you are new to this game. But one more
                                        repetition doesn’t hurt, does it?....
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="page-nav">
					<a href="#" class="page-nav__btn page-nav__left">
						<i class="btn__icon">
							<svg><use xlink:href="#arrow_left"></use></svg>
						</i>
						<span class="btn__text">PREV</span>
					</a>
					<ul class="page-nav__list">
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
					</ul>
					<a href="#" class="page-nav__btn page-nav__right">
						<span class="btn__text">NEXT</span>
						<i class="btn__icon">
							<svg><use xlink:href="#arrow_right"></use></svg>
						</i>
					</a>
				</div> -->
                </div>
            </section>
            <!-- end section -->
            <?php require '../elements/footerinner.php'; ?>
        </div>
    </main>
    <?php require '../elements/svgcodeinner.php'; ?>
</body>

</html>