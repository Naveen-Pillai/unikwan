<!DOCTYPE html>
<html lang="en">

<head>
    <title>Contact Us | Unikwan Innovations | Design Agency In Bangalore
    </title>
    <meta name="description"
        content="Discover and Design experiences to transform business enterprises. Create something with us! Get in touch." />

    <?php require '../elements/headerinner.php'; ?>
</head>

<body>
    <!-- start Header -->
    <?php require '../elements/navbarinner.php'; ?>
    <!-- end Header -->
    <main id="mainContent">
        <div class="section--bg-wrapper-02">
            <!-- start section -->
            <section class="section--no-padding section">
                <div class="subpage-header__bg">
                    <div class="container">
                        <div class="subpage-header__block">
                            <h1 class="subpage-header__title">Contact Us</h1>
                            <div class="subpage-header__line"></div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end section -->

            <!-- start section -->
            <section class="section section-default-top">
                <div class="container">
                    <div class="section-heading section-heading_indentg02">
                        <div class="description"><i></i>Contact Form</div>
                        <h2 class="title">Please use the Form</h2>
                    </div>
                    <form class="ukcontact-form ukform-default" id='unikwanContact'>
                        <div class="row">
                            <div class="col-md-5 col-lg-4">
                                <div class="notes d-md-none d-lg-none d-xl-none">
                                    Fill details to submit
                                </div>
                                <div class="form-group">
                                    <!-- <label class="placeholder-label">Your Name</label> -->
                                    <input type="text" placeholder='Your Name' id="name" name="name"
                                        class="form-control" />
                                </div>
                                <div class="form-group">
                                    <!-- <label class="placeholder-label">Company Name</label> -->
                                    <input placeholder='Company Name' type="text" id="name2" name="name2"
                                        class="form-control" />
                                </div>
                                <div class="form-group">
                                    <!-- <label class="placeholder-label">Email Address</label> -->
                                    <input type="text" placeholder='Email Address' id="email" name="email"
                                        class="form-control" />
                                </div>
                                <div class="form-group">
                                    <!-- <label class="placeholder-label">Email Address</label> -->
                                    <input type="text" placeholder='Phone No (Optional)' id="phone" name="phone"
                                        class="form-control" />
                                </div>
                                <div class="form-group">
                                    <div class="wrapper-select-for-title">
                                        <!-- <label class="placeholder-label">Purpose </label> -->
                                        <select id="industry" name="industry" class="js-init-select select-custom-02">
                                            <option>Business</option>
                                            <option>Jobs</option>
                                            <option>Media &Workshop</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="notes d-none d-md-block d-lg-block d-xl-block">
                                    <article class='sent-notification contact-error-text'>Fill details to submit
                                    </article>
                                </div>

                            </div>
                            <div class="divider divider__md notes d-md-none d-lg-none d-xl-none"></div>
                            <div class="col-md-7 col-lg-8" style='display: flex;flex-direction: column;'>
                                <div class="form-group">
                                    <!-- <label class="placeholder-label">Message</label> -->
                                    <textarea placeholder='Message' id="subject" name="subject"
                                        class="form-control"></textarea>
                                </div>
                                <div class="navbar-uk-mobilehid">
                                    <article class='sent-notification contact-error-text'>Fill details to submit
                                    </article>
                                </div>
                                <div class='recaptcha_holder'>
                                    <div class="g-recaptcha" data-sitekey="6LcL5bsZAAAAAL_o5ccyGGgPi-2H_I6qwOMmk1ip"
                                        data-callback='captcha_validation'></div>
                                    <div onclick='sendjsmail()' class="btn recaptcha_holder_margin">SUBMIT NOW</div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </section>
            <!-- end section -->
            <!-- start section -->
            <section class="section section-default-top">
                <div class="container">
                    <div class='locations_main_hold'>
                        <div class='locations_single_hold flex-prop-flex1'>
                            <article class='locations_header_txt'>
                                Office Locations
                            </article>
                            <div class='location_single_sub'>
                                <div class='locations_logo_hold flex-prop-flex1'>
                                    <div>
                                        <img src="../images/contact/pin.svg" alt="">
                                    </div>
                                    <div class='locations_details_txt'>
                                        <article class='locations_details_txt1'>
                                            New Delhi
                                        </article>
                                        <article class='locations_details_txt2'>
                                            N-239, First floor, GK-1,<br /> New Delhi-110048
                                        </article>
                                    </div>
                                </div>
                                <div class='locations_logo_hold flex-prop-flex1 flex-prop-flex1'>
                                    <div>
                                        <img src="../images/contact/pin.svg" alt="">
                                    </div>
                                    <div class='locations_details_txt'>
                                        <article class='locations_details_txt1'>
                                            Bengaluru
                                        </article>
                                        <article class='locations_details_txt2'>
                                            Bengaluru 1115, 2nd floor,<br /> 23rd Main Road, Sector 2,<br /> HSR Layout,
                                            Bengaluru,<br /> Karnataka 560102
                                        </article>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class='locations_single_hold flex-prop-flex1 spl-margin-leftadd'>
                            <article class='locations_header_txt'>
                                Contact Us
                            </article>
                            <div class='location_single_sub'>
                                <div class='locations_logo_hold flex-prop-flex1'>
                                    <div>
                                        <img src="../images/contact/phone-call.svg" alt="">
                                    </div>
                                    <div class='locations_details_txt'>
                                        <article class='locations_details_txt1'>
                                            Phone Number
                                        </article>
                                        <article class='locations_details_txt2'>
                                            +91-9686568578 <br />+91-9891303913
                                        </article>
                                    </div>
                                </div>
                                <div class='locations_logo_hold flex-prop-flex1'>
                                    <div>
                                        <img src="../images/contact/email.svg" alt="">
                                    </div>
                                    <div class='locations_details_txt'>
                                        <article class='locations_details_txt1'>
                                            Email
                                        </article>
                                        <article class='locations_details_txt2'>
                                            hello@unikwan.com
                                        </article>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- <section class="section section-default-top">
                <div class="contact-info">
                    <div class="contact-info__item">
                        <div class="contact-info__img">
                            <div class="mapouter">
                                <div class="gmap_canvas"><iframe width="100%" height="500" id="gmap_canvas"
                                        src="https://maps.google.com/maps?q=unikwan&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                                        href="https://www.embedgooglemap.net/blog/nordvpn-coupon-code/"></a></div>
                                <style>
                                .mapouter {
                                    position: relative;
                                    text-align: right;
                                    height: 500px;
                                    width: 100%;
                                }

                                .gmap_canvas {
                                    overflow: hidden;
                                    background: none !important;
                                    height: 500px;
                                    width: 100%;
                                }
                                </style>
                            </div>
                        </div>
                        <div class="contact-info__description">
                            <address>
                                <p>
                                    <strong>Address</strong>
                                    1115, 2nd floor,<br />23rd Main Road, Sector 2,
                                    <br />HSR Layout, Bengaluru,
                                    <br />Karnataka 560102
                                </p>
                                <p>
                                    <strong>Phone Number</strong>
                                    +91-9686568578,<br />+91-9891303913
                                </p>
                                <p>
                                    <strong>Email</strong>
                                    <a href="mailto:hello@unikwan.com">hello@unikwan.com</a>
                                </p>
                            </address>
                        </div>
                    </div>
                </div>
            </section> -->
            <!-- end section -->
            <?php require '../elements/footerinner.php'; ?>
        </div>
    </main>
    <?php require '../elements/svgcodeinner.php'; ?>
</body>

</html>